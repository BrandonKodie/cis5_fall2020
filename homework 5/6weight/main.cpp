/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: brand
 *
 * Created on October 5, 2020, 1:09 PM
 */
#include <iostream>
#include <cstdlib>

using namespace std;

const double  poundPerKilo = 2.2046; // 2.2046 feet in a kilogram
// const double gramsPerKilo = 0.001; // 1000 grams in a kilogram
//const double ouncesPerPound = 0.0666; // 15 ounces in a pound
// const double gramsPerPound = 453.5970; // 1000 grams / 2.2046
const double gramsPerOunce = 30.2398; // 453.5970 / 15
    
/**
 * 
 * 
 * @param pound converted to kilogram 
 * @param ounce 
 * @return  grams from ounces and int remainder of kilograms
 */
int convert(int pound, double ounce);
/**
 * 
 * @param pound int division for whole number of kilograms 
 * @pint weightInput ();aram ounce check to see if ounce >= 1000 grams , if so + 1 kilogram
 * @return 
 */
int convertkilo(int pound, double ounce);
/**
 * enter weight for applied value
 * @return weight to the variable weightInput is applied to ex. int pound{ weightInput() }
 */
int weightInput ();
/**
 * 
 * @param display the amount of grams
 * @param display the amount of kilograms
 * @return 
 */
int output(int gram,int kilo);

int main(int argc, char** argv)
{
    string menuInput;
    do
    {
        
    
    cout << "Enter Pounds " << endl;
 
    int pound{ weightInput() }; 
    cout << " Enter Ounces " << endl;
    double ounce{ weightInput()};
    
    int gram{convert(pound, ounce)};
    int kilo{convertkilo(pound, ounce)};
    
    output(gram, kilo);
    
    

    cout<< "Would you like to input again? y/n" << endl;
    cin >> menuInput;
 
    }while(tolower(menuInput[0]) == 'y');
    
    return 0;
}
int weightInput ()
{
    cout << "Please enter the number " << endl;
    int weight{};
    cin >> weight;
    return weight;
}
int output(int gram,int kilo)
{
    cout << "Kilo: " << kilo << endl;
    cout << "Grams: " << gram << endl;
}
int convertkilo(int pound, double ounce)
{
    
    int grams;
    double kilogram; 
    int kilo;
    double remainder;
    
    kilogram = pound / poundPerKilo;
    kilo = kilogram / 1;
    //kilogramIntRemainder = pound % poundsPerKilo
    //
    remainder = (kilogram - kilo) * 1000;
    
    grams = (ounce * gramsPerOunce) + remainder;
        while( grams > 1000)
    {
        kilo = kilo + 1;
        grams = grams - 1000;
    }
   
    return (kilo);
}
int convert(int pound, double ounce)
{
    
    int grams;
    double kilogram; 
    int kilo;
    double remainder;
    
    kilogram = pound / poundPerKilo;
    kilo = kilogram / 1;
    //kilogramIntRemainder = pound % poundsPerKilo
    remainder = (kilogram - kilo) * 1000;
    
    grams = (ounce * gramsPerOunce) + remainder;
           while( grams > 1000)
    {
        kilo = kilo + 1;
        grams = grams - 1000;
    }
    
    return (grams);
}