#include<iostream>

#include <cstdlib>

using namespace std;

/*
 * 
 */
void swapIntegers(int& a, int& b);
int main(int argc, char** argv)
{
    int a = 5;
    int b = 1;
    cout << "a = " << a << " " << "b = " << b << endl;
    swapIntegers(a, b); 
    cout << "a = " << a << " " << "b = " << b << endl;
    return 0;
}
void swapIntegers(int& a, int& b)
{
    int temp = a;
    a = b;
    b = temp;
    
    
}
