#include<iostream>
#include <cstdlib>

using namespace std;

const double centimetersInInch = 2.54;
const double feetInMeters = 3.2808;
const double metersInFoot = 0.3048;

int remainderFromMeter(int foot, int meter, int& meterRemainder);

int metersFromFeet(int foot, int& meter, double metersInFoot);
double centimeterFromInch(int inch,double& centimeter, int meterRemainder);
void centimetersOver(double& centimeter, int& meter);

int main(int argc, char** argv)
{
    int foot, inch, meter, meterRemainder;
    double meters, centimeter;
    

    cout << "Enter feet: " << endl;
    cin >> foot;
    cout << "Enter inches: " << endl;
    cin >> inch;
    metersFromFeet(foot, meter, metersInFoot);
    remainderFromMeter(foot, meter, meterRemainder);
    centimeterFromInch(inch, centimeter, meterRemainder);
    centimetersOver(centimeter, meter);
    
    cout << "Meters: " << meter << endl;
    
    cout << "Centimeters: " << centimeter << endl;
            
    
    return 0;
}

void centimetersOver(double& centimeter, int& meter)
{
    while(centimeter > 99)
    {    
        if (centimeter > 99)
        {
        meter = meter + 1;
        centimeter = centimeter - 100;
        }
    }
}
double centimeterFromInch(int inch,double& centimeter, int meterRemainder)
{
    centimeter = (inch * centimetersInInch) + meterRemainder;
    return centimeter;
}
int metersFromFeet(int foot, int& meter, double metersInFoot)
{
    meter = foot / feetInMeters;
    return meter;
}
        int remainderFromMeter(int foot, int meter, int& meterRemainder)
        {
        meterRemainder = ((foot / feetInMeters) - meter) * 100;
        return meterRemainder;

        }