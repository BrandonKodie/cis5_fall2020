
#include<iostream>

#include <cstdlib>

using namespace std;

/*
 * 
 */
void swapStrings(string& firstString, string& secondString);
int main(int argc, char** argv)
{
    string firstString = "Hello";
    string secondString = "Good-Bye";
    cout << "firstString = " << firstString << " " << "secondString = " << secondString << endl;
    swapStrings(firstString, secondString); 
    cout << "firstString = " << firstString << " " << "secondString = " << secondString << endl;
    return 0;
}
void swapStrings(string& firstString, string& secondString)
{
    string temp = firstString;
    firstString = secondString;
    secondString = temp;
    
    
}
