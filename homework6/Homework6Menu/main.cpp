#include<iostream>
#include <cstdlib>

using namespace std;

const int quarterValue = 25;
const int dimeValue = 10;
const int pennyValue = 1;
const double centimetersInInch = 2.54;
const double feetInMeters = 3.2808;
const double metersInFoot = 0.3048;
int max(double val1, double val2, double val3);
double max(double val1, double val2);
void swapStrings(string& a, string& b);
void swapIntegers(int& a, int& b);
int remainderFromMeter(int foot, int meter, int& meterRemainder);
void swapInString(char swap1, char swap2, string&);
void swapInString(char, char,string&, string&);
void swapChar(char& swap1, char& swap2);
int metersFromFeet(int foot, int& meter, double metersInFoot);
double centimeterFromInch(int inch,double& centimeter, int meterRemainder);
void centimetersOver(double& centimeter, int& meter);
void compute_coins1(int coin_Value, int& num, int& amount_Left);
void compute_coins2(int coin_Value, int& num, int& amount_Left);
void compute_coins3(int coin_Value, int& num, int& amount_Left);
int main(int argc, char** argv)
{
    string string1, string2;
    char swap1;
    char swap2;
    string firstString = "Hello";
    string secondString = "Good-Bye";
    double num1, num2, num3;
    string menuInput, menuInput2;
    int option;
    int a = 5;
    int b = 1;
    int foot, inch, meter, meterRemainder;
    double meters, centimeter;
    int num = 0, coin_Value = 0, amount_Left = 0;
    int quarters = 0, dimes = 0, pennies = 0;
    
 do
 {
    cout << "HomeWork 6 Problems:" << endl;
    cout << "1) Overload" << endl;
    cout << "2) Swap Integers" << endl;
    cout << "3) Swap Strings" << endl;
    cout << "4) Swap Chars" << endl;
    cout << "5) Length Conversion" << endl;
    cout << "6) Coin Conversion" << endl;
    cin >> option;
    switch(option)
        {
        case 1:
        
        cout << " Enter two numbers: ";
        cin >> num1;
        cin >> num2;
        max(num1, num2);
        cout << " Enter three numbers: " << endl;
        cout << " First number: ";
        cin >> num1;
        cout << " Second number: ";
        cin >> num2;
        cout << " Third number: ";
        cin >> num3;
        max(num1, num2, num3);
        break;
        case 2:
            
        cout << "a = " << a << " " << "b = " << b << endl;
        swapIntegers(a, b); 
        cout << "a = " << a << " " << "b = " << b << endl;
        break;
        case 3:
            
        cout << "firstString = " << firstString << " " << "secondString = " << secondString << endl;
        swapStrings(firstString, secondString); 
        cout << "firstString = " << firstString << " " << "secondString = " << secondString << endl;
        break;
        case 4:
    
        cout << " Swap two chars " << endl;
        cout << " Please enter two chars" << endl;
        cin >> swap1 >> swap2;
        
        cout << "swap1 = " << swap1 << " " << "swap2 = " << swap2 << endl;
        swapChar(swap1, swap2); 
        cout << "swap1 = " << swap1 << " " << "swap2 = " << swap2 << endl;
       
        cout << "Swap chars in a string" << endl;
        cout << "Enter Char to swap: " << endl;
        cin >> swap1;
        cout << "Enter Char to swap with: " << endl;
        cin >> swap2;
        cout << " Enter  first string: " << endl;
        cin >> string1;
        swapInString(swap1, swap2, string1);
        cout << "string1 is now: " << string1 << endl;
        cout << " Swap chars in two strings" << endl;
        cout << "Enter Char to swap: " << endl;
        cin >> swap1;
        cout << "Enter Char to swap with: " << endl;
        cin >> swap2;
        cout << " Enter  first string: " << endl;
        cin >> string1;
        cout << " Enter your second string: " << endl;
        cin >> string2;
        cout << "Swap " << swap1 << " with " << swap2 << " for "
             << string1 << " and " << string2 << endl;

        swapInString(swap1, swap2, string1, string2);
        cout << "string1 is now: " << string1 << endl;
         
        cout << "string2 is now: " << string2 << endl;
          
        break;
            
        case 5:
        cout << "Enter feet: " << endl;
        cin >> foot;
        cout << "Enter inches: " << endl;
        cin >> inch;
        metersFromFeet(foot, meter, metersInFoot);
        remainderFromMeter(foot, meter, meterRemainder);
        centimeterFromInch(inch, centimeter, meterRemainder);
        centimetersOver(centimeter, meter);

        cout << "Meters: " << meter << endl;

        cout << "Centimeters: " << centimeter << endl;
            
    
        break;
        case 6:
         do
        {
        cout << "Enter number of cents: " << endl;
        cin >> amount_Left;
        compute_coins1(quarterValue,quarters,amount_Left);
        compute_coins2(dimeValue,dimes,amount_Left);
        compute_coins3(pennyValue,pennies,amount_Left);   
        cout << "Quarters " << quarters << " Dimes " << dimes << " Pennies: " << pennies << endl;
        cout << "convert again?" << endl;
        cin >> menuInput;
    
        } while(tolower(menuInput[0]) == 'y');
         break;
            
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    cout << " Return home? y/n";
    cin >> menuInput2;
    }while (tolower(menuInput2[0]) == 'y');
    
    

    return 0;
}

void compute_coins1(int coin_Value, int& num, int& amount_Left)
{
    
    num = amount_Left / coin_Value;
         
    amount_Left = amount_Left - (coin_Value * num);
    
}
  
void compute_coins2(int coin_Value, int& num, int& amount_Left)
{          
    num = amount_Left/ coin_Value;

    amount_Left = amount_Left - (coin_Value * num);      
}   
     
   
void compute_coins3(int coin_Value, int& num, int& amount_Left)
     {
        num = amount_Left / pennyValue;    
     }
void centimetersOver(double& centimeter, int& meter)
{
    while(centimeter > 99)
    {    
        if (centimeter > 99)
        {
        meter = meter + 1;
        centimeter = centimeter - 100;
        }
    }
}
double centimeterFromInch(int inch,double& centimeter, int meterRemainder)
{
    centimeter = (inch * centimetersInInch) + meterRemainder;
    return centimeter;
}
int metersFromFeet(int foot, int& meter, double metersInFoot)
{
    meter = foot / feetInMeters;
    return meter;
}
        int remainderFromMeter(int foot, int meter, int& meterRemainder)
        {
        meterRemainder = ((foot / feetInMeters) - meter) * 100;
        return meterRemainder;

        }
        void swapChar(char& swap1, char& swap2)
{
    char temp = swap1;
    swap1 = swap2;
    swap2 = temp;
    
    
        
}
void swapInString(char swap1, char swap2, string& string1)
{
    char temp = swap1;
    swap1 = swap2;
    swap2 = temp;
    
    for (int i = 0; i < string1.size(); i++)
    {
    
        if (string1[i] == swap1)
        {
        string1[i] = swap2;
        }
        else if (string1[i] == swap2)
        {
            string1[i] = swap1;
        }
        else
        {

        } 
    
    }
}

void swapInString(char swap1, char swap2, string& string1, string& string2)
{
    char temp = swap1;
    swap1 = swap2;
    swap2 = temp;
    
    for (int i = 0; i < string1.size(); i++)
    {
    
        if (string1[i] == swap1)
        {
            string1[i] = swap2;
        }
        else if (string1[i] == swap2)
        {
            string1[i] = swap1;
        }
        else
        {

        }
        }
    for (int i = 0; i < string2.size(); i++)
    {
    
        if (string2[i] == swap1)
        {
            string2[i] = swap2;
        }
        else if (string2[i] == swap2)
        {
            string2[i] = swap1;
        }
        else
        {

        }
    
}
}

void swapIntegers(int& a, int& b)
{
    int temp = a;
    a = b;
    b = temp;
    
    
}
void swapStrings(string& firstString, string& secondString)
{
    string temp = firstString;
    firstString = secondString;
    secondString = temp;
    
    
}
int max(double val1, double val2, double val3)
{
    if(val1 > val2 && val1 > val3)
    {
        cout << "The first number: " << val1 << " is the biggest " << endl;
        
        return val1;
       
    }
    else if( val2 > val1 && val2 > val3 )
    {
        cout << " The second number: " << val2 << " is the biggest " << endl;
        return val2;
    }
    else if ( val3 > val1 && val3 > val2)
    {
        cout << " The third number: " << val3 << " is the biggest " << endl;
        return val3;
    }
    else
    {
        cout << "Numbers provided are invalid" << endl;
    }
}
double max(double val1, double val2)
{
    if(val1 > val2)
    {
        cout << val1 << " is bigger than " << val2 << endl;
        
        return val1;
       
    }
    else if(val2 > val1)
    {
        cout << val2 << " is bigger than " << val1 << endl;
        return val2;
    }
    else
    {
        cout << val1 << " Equals " << val2 << endl;
    }
}