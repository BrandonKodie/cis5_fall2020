#include <iostream>


#include <cstdlib>

using namespace std;

const int quarterValue = 25;
const int dimeValue = 10;
const int pennyValue = 1;
void compute_coins1(int coin_Value, int& num, int& amount_Left);
void compute_coins2(int coin_Value, int& num, int& amount_Left);
void compute_coins3(int coin_Value, int& num, int& amount_Left);
int main(int argc, char** argv)
{
    int num = 0, coin_Value = 0, amount_Left = 0;
    int quarters = 0, dimes = 0, pennies = 0;
    
    string menuInput;
    do
    {
        cout << "Enter number of cents: " << endl;
        cin >> amount_Left;
        compute_coins1(quarterValue,quarters,amount_Left);
        compute_coins2(dimeValue,dimes,amount_Left);
        compute_coins3(pennyValue,pennies,amount_Left);   
        cout << "Quarters " << quarters << " Dimes " << dimes << " Pennies: " << pennies << endl;
        cout << "convert again?" << endl;
        cin >> menuInput;
    
    } while(tolower(menuInput[0]) == 'y');
    return 0;
}

void compute_coins1(int coin_Value, int& num, int& amount_Left)
{
    
       num = amount_Left / coin_Value;
         
        amount_Left = amount_Left - (coin_Value * num);
    
}
  
void compute_coins2(int coin_Value, int& num, int& amount_Left)
{      
  
        
         num = amount_Left/ coin_Value;
         
        amount_Left = amount_Left - (coin_Value * num);
        
       
}   
     
   
void compute_coins3(int coin_Value, int& num, int& amount_Left)
     {
         
    
        num = amount_Left / pennyValue;
        
     }
    
    
    