#include <vector>
#include<iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
int linearSearch(const vector<int> &, int val);
void selectionSort(vector<int>& v);
int binarySearch(const vector<int> &, int val);
void output(const vector<int> &v);
void storeRandomNumbers(vector<int>& v, int low, int high);
int main(int argc, char** argv)
{
    srand(time(0));
    int size = 20;
    vector<int> v(size);
    storeRandomNumbers(v, 1, 100);
    selectionSort(v);
    output(v);
    
    cout << "Numbers to flip:";
    int num;
    cin >> num;
    
    int location = linearSearch(v,num);
    
    if (location == -1)
    {
        cout << "That number was not found!" << endl;
    }
    else
    {
        cout << "The number is located at:" << location << endl;
    }
    
    cout << " Using binary search..." << endl;
    // make sure sorted
    selectionSort(v);
    output(v);
    
    cout << "Enter another number to find: ";
    cin >> num;
    
    
    location = binarySearch(v, num);
    
    if (location == -1)
    {
        cout << "That number was not found!" << endl;
    }
    else
    {
        cout << "The number is located at:" << location << endl;
    }
    
    return 0;
}
void storeRandomNumbers(vector<int>& v, int low, int high)
{
    for(int i = 0; i < v.size(); i++)
    {
        v[i] = rand() % (high - low + 1) + low;
    }
}
int linearSearch(const vector<int> &v, int val)
{
    // loop to search for the number
    for ( int i = 0; i< v.size(); i++)
    {// search every element and compare
        if (v[i] == val)
        {
            return i;
        }
    }
    // If I reach here i never found the code
    return -1;
}
void selectionSort(vector<int>& v)
{

    for(int i = 0; i < v.size() - 1; i++)
    {
        int min = i;
        for(int j = i; j < v.size(); j++)
        {
                if (v[j] < v[min])
                {
                    min = j;
                }
        }
            swap(v[i], v[min]);
           
    }
}
void output(const vector<int> &v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}
/**
 * Performs binary search on a vector
 * @param v
 * @param val
 * @return 
 */
int binarySearch(const vector<int>& v, int val)
{
    if(v.size() == 0)
    {
        return -1;
    }
    int low = 0;
    int high = v.size() - 1;
    int middle = (low + high) / 2;
    int guess = v[middle];
    
    // Keep splitting the search space in half
    // Search only what you need to 
    while(guess != val && low <= high) 
    {
       // cout << "low: " << low << endl;
       // cout << "high: " << high << endl;
      //  cout << "middle: " << middle << endl << endl;;
        // Get the new middle
            if (guess < val) // guessed to low
            { 
               // cout << " too low" << endl;
                // Go search to the right of it
                low = middle + 1;
                middle = (low + high) / 2;
                guess = v[middle];
            }
            else// guess is too high
            {
               // cout << "too high" << endl;
                // move high val down
                high = middle - 1;
                middle = (low + high) / 2;
                guess = v[middle];
        }
     
    }
        if(low > high)
        {
            return -1;
        }
        else
        {
        return middle;
    }
}