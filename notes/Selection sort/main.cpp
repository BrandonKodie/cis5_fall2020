#include<iostream>
#include <cstdlib>
#include <vector>
#include <ctime>
using namespace std;

/*
 * 
 */
void storeRandomNumbers(vector<int>& v, int low, int high);
void output(const vector<int> &v);
void selectionSort(vector<int>& v);
int main(int argc, char** argv)
{
    srand(time(0));
    // Selection sort
            // Have 2 regions, unsorted and sorted region
    // Find the smallest in the unsorted region
    // Place at the front of the unsorted region
   
    vector<int> v(10);
     storeRandomNumbers(v, 1, 50);
    output(v);
    selectionSort(v);
    output(v);
    return 0;
}
void storeRandomNumbers(vector<int>& v, int low, int high)
{
    for(int i = 0; i < v.size(); i++)
    {
        v[i] = rand() % (high - low + 1) + low;
    }
}

void output(const vector<int> &v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}
/**
 * Sorts the vector using selection sort, in ascending order
 * @param v
 */
void selectionSort(vector<int>& v)
{
    // Number of iterations
    for(int i = 0; i < v.size() - 1; i++)
    {
        cout << "Iteration: " << i + 1 << endl;
        
        //find min
        // minimum is going to be the minimum location
        int min = i;
        
        // Selecting the smallest value in the unsorted region
        for(int j = i; j < v.size(); j++)
        {
                if (v[j] < v[min])
                {
                    min = j;
                }
        }
            // swap
            swap(v[i], v[min]);
            output(v);
        
    }
}
void selectionSort(vector<int>& v)
{

    for(int i = 0; i < v.size() - 1; i++)
    {
        int min = i;
        for(int j = i; j < v.size(); j++)
        {
                if (v[j] < v[min])
                {
                    min = j;
                }
        }
            swap(v[i], v[min]);
           
    }
}