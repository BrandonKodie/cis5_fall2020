// https://www.youtube.com/watch?v=mwUV70E6qtc&feature=youtu.be
#include <cstdlib>
#include<iostream>
#include<vector>
#include<ctime>
using namespace std;

void storeRandomNumbers(vector<int>& v, int low, int high);
void output(const vector<int> &v);
void insertionSort(vector<int> &v);
/*
 * 
 */
int main(int argc, char** argv)
{   srand(time(0));

    vector<int> v(10);
    
    storeRandomNumbers(v, 1, 50);
    
    output(v);
    
    insertionSort(v);
    
    output(v);
    return 0;
}

void storeRandomNumbers(vector<int>& v, int low, int high)
{
    for(int i = 0; i < v.size(); i++)
    {
        v[i] = rand() % (high - low + 1) + low;
    }
}

void output(const vector<int> &v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}
/**insertion sort
 * sorts in ascending order
 * @param v
 */
void insertionSort(vector<int> &v)
{
    for (int i = 1; i < v.size(); i++)
    {
        cout << "Iteration: " << i << endl;
        int cur = v[i];
        
        int j = i;
        
        // j > 0 handles the case of inserting at the beginning 
        while(j > 0 && cur < v[j - 1])
        {
            // We need to shift the value over 
            v[j] = v [j-1];
            j--;
        }
        v[j] = cur;
        
        output(v);
    }
}