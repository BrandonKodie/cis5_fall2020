#include<iostream>
#include <cstdlib>

using namespace std;
//Global constants
const int COLUMNS = 3;
/*
 * 
 * 
 */
// Function assign values to array
void assignValues(int a[][COLUMNS], int rows)
{    
    // Nested for loops
    // For every single row
    for (int i = 0; i < rows; i++)
    {
        // For every single column in within the row
        for (int j = 0; j < COLUMNS;j++)
        {
            a[i][j] = i * rows + j + 1;
        }
    }
    
}
int main(int argc, char** argv)
{
    int rows = 3;
   
    
    int a[rows][COLUMNS];
    assignValues(a, rows);
  
    
    // output the array
        for (int i = 0; i < rows; i++)
    {
        // For every single column in within the row
        for (int j = 0; j < COLUMNS;j++)
        {
            cout << a[i][j] << " ";
        }
        cout << endl;
    }
    
    
    
    return 0;
}

