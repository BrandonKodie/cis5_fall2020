

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    
    string userInput;
    
    cout << "1) Loops with strings" << endl;
    cout << "2) Babylonian Algorithm" << endl;
    
    cout << "Please enter an option: ";
    int option;
    cin >> option;
    
    string name;
    
    switch(option)
    {
        case 1:
            cout << "Loops" << endl;
            
            name = "Matthew";
            
            cout << name.size() << endl;
            
            for(int i = 0; i < name.size(); i++)
            {
                for (int j = 0; j < name.size(); j++)
                    
                {
                    cout << name[i] << " ";
                }
                // name[i] = 'x';
                // USe the subscript operator []
                // to output individual characters
               
                cout << endl;
            }
            cout << endl << name << endl;
            break;
            
            
        case 2:
            cout << "Babylonian" << endl;
            // Problem 5, Week 4 Lab
            /*
             The Babylonian algorithm  to compute the square root n is as follows:
             * a. Make a guess at the answer(you can pick n / 2 as your initial guess)
             * b. Compute r = n / guess
             * c. set guess = (guess +r) / 2
             * d. Go back to step 2 for as many iterations as necessart
             * Guess will be come closer to squar root as the steps are repeated.
             * write a program that inputs an integer for n
             * iterates through the babylonian algorithm until guess
             * is within 1 % of the previous guess. Output the answer as a double 
             * with 5 decimals.
             */
            cout << "What do you want to find the square root of: ";
            double input;
            cin >> input;
            
            double guess, r, newGuess;
            guess = input / 2; // initial guess. a
            
            r = input / guess; // part b
            
            newGuess = (guess + r) / 2; // part c
            
            cout << "guess: " << guess << endl;
            cout << " New Guess: " << newGuess << endl;
            
            while (guess / newGuess < .9999 || guess / newGuess > 1.0001)
            {
              guess = newGuess;
              r = input / guess;
              newGuess = (guess + r) / 2;
              
              cout << "Guess: " << guess << endl;
              cout << "New Guess:" << newGuess << endl;
            }
                   
            break;
            
        case 3:
            
        default:
            cout << "No valid input... exiting... " << endl;
    }
    
    return 0;
}

