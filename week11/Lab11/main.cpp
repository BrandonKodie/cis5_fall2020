#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

/*
 * 
 */
void selectionSort(vector<int>& v);
void output(const vector<int> &v);
int main(int argc, char** argv) {

    int *pointer1 = new int;
    int *pointer2 = new int;
    
    cout << "Pointer 1 address : " << *pointer1 << "Pointer1 value: " << pointer1;
    cout << "Pointer 1 address : " << *pointer2 << "Pointer1 value: " << pointer2;
    int num;
    vector<int> pie;
    while(num != -999)
    {
        cout << "Enter a num or -999 to stop";
        cin >> num;
        pie.push_back(num);
        selectionSort(pie);
        output(pie);
    }
    
    return 0;
}

void selectionSort(vector<int>& v)
{
    // Number of iterations
    for(int i = 0; i < v.size() - 1; i++)
    {
        cout << "Iteration: " << i + 1 << endl;
        
        //find min
        // minimum is going to be the minimum location
        int min = i;
        
        // Selecting the smallest value in the unsorted region
        for(int j = i; j < v.size(); j++)
        {
                if (v[j] < v[min])
                {
                    min = j;
                }
        }
            // swap
            swap(v[i], v[min]);
            output(v);
        
    }
}
void output(const vector<int> &v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}