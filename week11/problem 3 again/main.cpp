#include<iostream>
#include <cstdlib>
#include<string>
#include<fstream>
#include<vector>
#include <iomanip>
using namespace std;

/*
 * 
 */
void output(const vector<string> name1, const vector<string> name2);
int main(int argc, char** argv) {
    string firstName;
    string lastName;
    vector<string> fName; // first name vector
    vector<string> lName; // last name vector
    ifstream broken("broken.dat");
    
    while (broken >> firstName >> lastName)
    {

        if(firstName[0] == ',' )
        {
            firstName[0] = ' ';
        }
        if(lastName[0] == ',')
            {
            lastName[0] = ' ';
        }

        fName.push_back(firstName);
        lName.push_back(lastName);
    } 
    output(fName,lName);
    return 0;
}
void output(const vector<string> name1, const vector<string> name2)
{
    for(int i = 0; i < name1.size();i++)
    {
        cout << "First name: " << left << setw(5) << name1[i] << "  " << "Last name: " 
                << name2[i] << endl;
    }
}
