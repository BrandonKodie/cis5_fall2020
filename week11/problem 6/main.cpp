#include<iostream>
#include <cstdlib>
#include <vector>
using namespace std;

/*
 * 
 */
int binarySearch(const vector<int>& v, int val);
void output(const vector<int> &v);
void selectionSort(vector<int>& v);
int main(int argc, char** argv) {
    srand(time(0));
    vector <int> bigVector;
    int num, location;
    int size = 100000;
    
    for(int i = 0; i  < size; i++)
    {
        int x = rand() % 100;
        bigVector.push_back(x);
    }
    selectionSort(bigVector);

        cout << "Enter a number to find: ";
    cin >> num;
    
    
    location = binarySearch(bigVector, num);
    
    if (location == -1)
    {
        cout << "That number was not found!" << endl;
    }
    else
    {
        cout << "The number is located at:" << location << endl;
    }
    return 0;
}

void selectionSort(vector<int>& v)
{

    for(int i = 0; i < v.size() - 1; i++)
    {
        int min = i;
        for(int j = i; j < v.size(); j++)
        {
                if (v[j] < v[min])
                {
                    min = j;
                }
        }
            swap(v[i], v[min]);
            
    }
}
void output(const vector<int> &v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << endl;
    }
    cout << endl;
}
int binarySearch(const vector<int>& v, int val)
{
    if(v.size() == 0)
    {
        return -1;
    }
    int low = 0;
    int high = v.size() - 1;
    int middle = (low + high) / 2;
    int guess = v[middle];
    
    // Keep splitting the search space in half
    // Search only what you need to 
    while(guess != val && low <= high) 
    {
       // cout << "low: " << low << endl;
       // cout << "high: " << high << endl;
      //  cout << "middle: " << middle << endl << endl;;
        // Get the new middle
            if (guess < val) // guessed to low
            { 
               // cout << " too low" << endl;
                // Go search to the right of it
                low = middle + 1;
                middle = (low + high) / 2;
                guess = v[middle];
            }
            else// guess is too high
            {
               // cout << "too high" << endl;
                // move high val down
                high = middle - 1;
                middle = (low + high) / 2;
                guess = v[middle];
        }
     
    }
        if(low > high)
        {
            return -1;
        }
        else
        {
        return middle;
    }
}