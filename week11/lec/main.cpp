#include<iostream>
#include <cstdlib>

using namespace std;


struct Student // <- PascalCase
{
    string name;
    int id;
    int age;
    string phoneNumber;
    
}; // <- ending curly brace
typedef Student* Zebras; 
void output(Student student);
void output(Student* students, int size);
void addElement(Student* &students, int &size, Student student);
int main(int argc, char** argv)
{
    Student student1;
    
    student1.name = "Dave";
    student1.id = 3;
    student1.age = 31;
    student1.phoneNumber = "664-3296";
    
    output(student1);
    int size = 0;
    Student* students = new Student[size];
    
    addElement(students,size,student1);
    output(student1);
    
    return 0;
}
void output(Student student)
{
    cout << "Name: " << student.name << endl;
    cout << "Id: " << student.id << endl;
    cout << "Age " << student.age << endl;
    cout << "Phone Number: " << student.phoneNumber << endl;
}
void addElement(Student* &students, int&size, Student student)
{
    // create new one   
    //copy over 
    // get rid of old
    //Step
    Student* temp = new Student[size + 1];
    // step 2 copy
    for(int i = 0; i < size;i ++)
    {
        temp[i] = students[i];  
    }
    
    temp[size] = student;
    
    delete[] students;
    
    students = temp;
    size++;
}
void output(Student* students, int size)
{
    for(int i = 0; i < size; i++)
    {
        output(students, size);
        
    }
}