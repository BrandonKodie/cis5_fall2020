#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    // string data type
    string s = "Hello World"; // string literal
    
    // primitive data types
    int i;
    double d;
    bool b;
    char c;
    
    // String is known as a Class
    // s is a variable that holds a string object
    // an object is an instance of a class
    // a class is a user defined data type
    string s2 = "Dave";
    
    cout << i << " " << s << endl;
    
    // Objects contain properties
    // String is a set of characters
    
    // Objects contain member function
    // A function is a specific task e.g. srand(),rand(), getline (cin, s)
    // A Member is the object which the function acts upon
    // A member function only acts on the member
    // A member function is used with the dot(.) operator
    
    // String member functions
    // Length of a stream
    // Use .Length9() or .size()
    // Returns the number of characters in the string
    
    int length = s2.length();
    cout << "The length of s2 is: " << length << endl;
    
    cout << "The length of s is:" << s.length() << endl;
    
    cout << "The size of s is" << s.size() << endl;
    // Not a function
    // int x = (2 + 3) / 5;
    
    // 2nd member function
    // .substr(int location) // starts at location reads to end
    // .substr(int location, int count) count # of character
    // ex.  s.substr(0,1);
    // Returns the sub-string of a string
    
    // "Hello World"
    //Start location at 0 <- C++ is a zero-based index language
    // Count location starts at 1
    // H -> Location 0
    // E -> Location 1
    // L -> Location 2
    // L -> Location 3
    // O -> Location 4
    
    // E.X. Get the sub-string "LLO"
    // start at location 2 get 3 characters
    
    cout << s.substr(2, 3) << endl;
    
    //e.x. Get the substring "ORL"
    cout << s.substr(7, 3) << endl;
    // Get " World"
    cout << s.substr(5) << endl;
    
    // get just the d 
    cout << s.substr(s.size() -1) << endl;
    
    // Find member function
    // Find any given sub-string from a string
    // Returns the location ( index) of that string
    // .find(string)
    
    //ex find O
    int location = s.find("O");
    cout << location << endl;
    //find the o
    location = s.find("o");
    cout << "Location of :" << location << endl;
    
    //Find the "orl" 
    cout << "Location of orl; " << s.find("orl") << endl;
    
    
    return 0;
}

