
#include <iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    
    //random numbers
    //seed a random number
    srand(time(0));
    
    cout << " How many times would you like to play " << endl;
    int numGames;
    cin >> numGames;
    
    // counter
    int totalPlayed = 0;
    
    // variables to keep track of plays
    int totalWins = 0;
    int totalLosses = 0;
    
    // The craps game itself
    while(totalPlayed < numGames)
    {
        cout << "Current Game: " << totalPlayed + 1 << endl;
        // continuation A
        //round 1
        int dice1 = rand() % 6 +1;
        int dice2 = rand() % 6 +1;
        
        int roll1 = dice1 + dice2;
        
        cout << "Roll 1 :" << roll1 << endl;
            // Flag for winning
        bool won = true;
            
        // check if won / lose
        if (roll1 ==7 || roll1 == 11)
        {
            won = true;
            totalWins++;
        }
        else if ( roll1 == 2 || roll1 == 3 || roll1 == 12 )
        { 
            won = false;
            totalLosses++;
        }
        else
        {
            // Continuation D
            // Round 2
            dice1 = rand() % 6 +1;
            dice2 = rand() % 6 +1;
            int roll2 = dice1 + dice2;
            
            cout << "Roll 2 : " << roll2 << endl;
            
            while (roll2 != 7 && roll2 != roll1)
            {
                cout << "Rolling Again!" << endl;
                
                dice1 = rand() % 6 +1;
                dice2 = rand() % 6 +1;
                roll2 = dice1 + dice2;
                
            }
            if (roll2 == 7)
            {
                won = false;
                totalLosses++;
            }
            
            else
            {
                won = true;
                totalWins++;
            }
        }
        //after the game has played
        // end in continuation c
        cout << "Current result: " << endl;
        
        if (won)
        {
            cout << "Won" << endl;
            
        }
        else
        {
            cout << "Lose" << endl;
        }
                totalPlayed++;
    }
    
    
    
    //display the final result
    cout << "You have won: " << totalWins << endl;
    cout << "You have lost :" << totalLosses << endl;
    
    
    return 0;
}

