#include <iostream>
#include <cstdlib>

using namespace std;

// return_data_type name (parameters)
// Purpose of the function is to determine/return
// if the number given is even or odd
// Return true if num is even
bool isEven(int num)
{
    //bool isEven= true;
    return num % 2 == 0;
    /*if(num % 2 == 0)
    {
       return true; //isEven = true;
    }
    else
    {
      return false;  //isEven = false;
    }*/
    //return isEven;
    
}
//Function Prototypes
// Similar to a variable declaration, but with function
//Function is defined somewhere else
// To do a function prototype, just provide the header with a semi-collon
bool isEven(int num);
/*
 * 
 */
int main(int argc, char** argv)
{
    // Get a number for the user
    int num;
    cout << "Please enter an integer: ";
    cin >> num;
    
    // If we mod value by 2, we can determine if odd or even
    //if(num % 2 == 0)
    if (isEven(num))
    {
        cout << "You have entered an even number!" << endl;
    }
    else
    {
        cout << "You entered an odd number" << endl;
    }
    
    
    return 0;
}

