#include<iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
// string objects
    string name = "Dave Matthews";
            
    // Location and Index
    // NAme has 13 characters
        cout << "Num characters: " << name.length() << endl;
    
    // The largest index (location is 12
    // Because  index starts at 0
     
        // Use sub-script operator []
        // A location is inside the [] e.x. [0]
        // Real example: name[0] -> D
        // Return the character at that location
        cout << "Character at location 0:" << name[0] << endl;
        
        cout << "2nd location:" << name[1] << endl;
        cout << " The value at location 2: " << name[2] << endl;
        
        // Get character at location 20
        // Logic Error -> Bounds error
        cout << "Location 20" << name[20] << endl;
        
    return 0;
}

