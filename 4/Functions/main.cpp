#include <iostream>
#include <cstdlib>

using namespace std;
// Function definitions  up here
// return_data_type function_name(parameters)
int square(int value)// function header
{
    value = value * value;
    
    //Return statement. The function will return the value to the 
    // invoker/caller
    return value;
}
// Create a function that compares 2 values to one another
// and outputs the largest one
// void function <- function that does not return a value
// Used when just output is needed
void compareTwoValues(int num1, int num2)
{
    if (num1 > num2)
    {
        cout << "the first value is larger: " << num1 << endl; 
    }
    else if (num1 < num2)
    {
        cout << "The second value is larger: " << num2 << endl;
    }
    else 
    {
        cout << "Both numbers are equal" << num1 << endl;
    }          
}

//Compare two values. Return the largest of the two
int getLargest(int num1, int num2)
{
    int largest;

    if (num1 > num2)
    {
      return num1; 
    }
    
    else 
    {
        return num2;
    }  
       
}
/*
 * 
 */
int main(int argc, char** argv)
{
    // Functions/Methods we have seen
    // strand() <- seeds a random number
    // rand() <- Creats a single random number
    // Member functions
    string s = "Hello";
    cout << "Substring member function: " << s.substr(2, 2) << endl;
    cout << "Length member function: " << s.length() << endl;
    
    srand(time(0));
    
   
    cout << "Random value function: " <<rand() << endl;
    
    // IO functions 
    // Input a sentence from the user
    cout << "Please enter a sentence: " << endl;
    getline(cin, s);
    cout << "The sentence from the getline function is: " << s << endl;
    
    // syntax of a function
    // Function definition
    // return_data_type function_name(parameters)
    
    // When i use a function, we call it invoking
    // When I use/invoke a function, I pass in arguments
    int num = 2;
    //num = num * num;
    num = square(num); // num is 4
    cout << "Num: " << num << endl;
            
    num = square(num); // num is 16
    cout << "Num: " << num << endl;
    
    // Store 10 numbers and compare which one is larger
    int num1, num2, num3, num4, num5, num6;
    
    cout << "Enter 6 integer values; ";
    cin >> num1 >> num2 >> num3 >> num4 >> num5 >> num6;
    /*
    if (num1 > num2)
    {
        cout << "Num1 is larger: " << num1 << endl;
       
    }
    else if(num1 < num2)
    {
        cout << "Num2 is larger:" << num2 << endl;
        
    }
    else 
    {
        cout << "Values are equal" << num1 << endl;
    }
    if (num3 > num4)
    {
        cout << "Num3 is larger: " << num3 << endl;
    }
    else if (num3 < num4)
    {
        cout << "num4 is larger: " << num4 << endl;
    }
    else 
    {
        cout << "Both are equal!" << endl;
    }
    if (num5 < num6)
    {
        cout << " Num6 is larger" << endl;
    }
    else if(num5 > num6)
    {
        cout << "Num5" << endl;
    }
    else
    {
        cout << "same" << endl;
    }
    */
    compareTwoValues(num1, num2);
    compareTwoValues(num3, num4);
    compareTwoValues(num5, num6);
    
    
         // Get the largest of the 6
    int largest = getLargest(getLargest(getLargest(getLargest(getLargest(
    num1, num2)
    , num3)
    , num4)
    , num5)
    , num6);
    cout << "Largest : " << largest << endl;
    return 0;
}

