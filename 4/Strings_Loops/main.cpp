#include <iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
// User enters string "Hello"
//    User sees: 
//H
//E
//L
//L
//O
    
    // Prompt the user
    cout << "Please enter a string:" << endl;
    string input;
    getline(cin, input);
    
    // We have the entire string
    // We have to iterate across the entire string
    // Use the for loop and the subscript operator
    // I know the size/length of string, hence for loop
    
    // Start at index 0. Loop "size" times
    for (int i = 0; i < input.size(); i++)
    {
        // Use the subscript operator [] square
        
        cout << input[i] << endl;
    }
    
    // output every the string in reverse
    for (int i = input.size() - 1; i >= 0; i--)
    {
        cout << input[i];
    }
    cout << endl;
    // Output all characters in caps
    for(int i = 0; i < input.length(); i++)
    {
        //char c = toupper(input[i]); //implicit conversion
          
        // Explicit conversion
        cout << static_cast<char>(toupper(input[i]));
        
        
    }
    return 0;
}

