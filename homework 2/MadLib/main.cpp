#include <iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    string AFOOD, Name, ANOTHERNAME, ADJ, COLOR, ANIMAL;
    int num;
    
    cout << "Please enter a name" << endl;
    cin >> Name;
    cout << "Please enter Another name " << endl;
    cin >> ANOTHERNAME;
    cout << "Please enter a food" << endl;
    cin >> AFOOD;
    cout << "Please enter a number between 100-200" << endl;
    cin >> num;
    cout << "Please enter an Adjective" << endl;
    cin >> ADJ;
    cout << "Please enter a color" << endl;
    cin >> COLOR;
    cout << "Please enter an animal" << endl;
    cin >> ANIMAL;
    
    
    cout << "Dear " << Name << "," << endl;
    
    cout << "I am sorry that I am unable to turn in my homework at this time. First, I ate a rotten " << AFOOD << "," << endl;
    cout << "which made me turn " << COLOR << " and extremely ill. I came down with a fever of " << num << "." << endl;
    cout << "Next, my pet " << ANIMAL << "must have smelled the remains of the " << AFOOD << "on my" << endl;
    cout << "homework because he ate it. I am currently rewriting my homework and hope you will accept " << endl;
    cout << "it late." << endl;
    cout <<" " << endl;
    cout << "Sincerely," << endl;
    cout << Name << endl;
            
    
    
    return 0;
    
    
}

