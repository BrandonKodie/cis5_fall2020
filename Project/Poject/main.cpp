#include<iostream>
#include <cstdlib>
#include<string>
#include<iomanip>
using namespace std;

const int CardsInHand = 5;
/**
 * passes cardValue[] through  convert_string_bac gives int values to string  
 * @param cardValue contain the string values for the cards
 * @param cardConverted the string values converted to int
 */
void convert_array_bac(string cardValue[], int cardConverted[]);
/**
 * provides the int values for string values input
 * @param card
 * @return 
 */
int convert_string_bac(string card);
  /**
   * Determines the winner by finding which number is closest to 9(9 is the highest possible value)
   * Takes in two ints and compares to see if they are the same or which one is greater
   * adds 1 to the bet counter of the outcome
   * @param playerHand
   * @param dealerHand
   * @param playerBet +1 if playerHand is closest to 9
   * @param dealerBet +1 if dealerHand is closer to 9
   * @param tieBet + if playerHand = dealerHand
   * @return 
   */                                   
int max(int playerHand, int dealerHand, int& playerBet,  int& dealerBet, int& tieBet);
/**
 * Takes in a string array, converts to int using convert_string then places in an int array
 * @param cardValue array is passed through convert_string 
 * @param cardConverted receives converted cardValue 
 */
void convert_array(string cardValue[], int cardConverted[]);
/**
 * Takes in a card string and returns an int value 
 * @param card string values are given int returns
 * @return int value for string card
 */
int convert_string(string card);
/**
 * Determines if the array has two matching values
 * @param cardConverted is the array that holds the cards in a players hand
 * @return true if hand has one pair of matching cardConverted[]
 */
bool onePair(int cardConverted[]);
/**
 * Determines if the array has two pairs of different matching values
 * @param cardConverted is the array that holds the cards in a players hand
 * @return true if hand has two different pairs  of cardConverted[]
 */
bool twoPair(int cardConverted[]);
/**
 * Determines if the carConverted array has three matching values
 * @param cardConverted is the array that holds the cards in a players hand
 * @return true if hand has three cardConverted[] that match
 */
bool threeOfAKind(int cardConverted[]);
/**
 * Determines if the carConverted array has four matching values
 * @param cardConverted is the array that holds the cards in a players hand
 * @return true if hand has 4 cardConverted[] that match
 */
bool fourOfAKind(int cardConverted[]);
/**
 * Determines if the cardConverted array contains a pair and three of a kind 
 *  with values that are different from the pair
 * 
 * @param cardConverted is the array that holds the cards in a players hand
 * @return true if hand contains two matching cardConverted[] as well as three matching
 * cardConverted values that are different from the pair
 */
bool fullHouse(int cardConverted[]);
/**
 * takes in chars for slot and applies an int value to each one
 * @param slot
 * @return 
 */
int convert_char(char slot);
/**
 * Takes a char array and converts it to integer values through convert_char function
 * then applies those int values to and int array
 * @param letters = the different chars on the slot
 * @param lettersConverted is the int array theat the converted char array is stored as
 */
void convert_array2(char letters[], int lettersConverted[]);
/**
 * Takes in a the three values payline row added together, determines if it equals
 *  a winning combination, adds bet * (multiplier of a winning combo) if any
 * @param row1  three converted values that make a payline added together;
 * @param score altered if row has a winning combo
 * @param bet holds the amount of player bet which is applied to a 
 * multiplier if there is a winning line;
 * @return 
 */
int row(int row1, int& score, int bet);
/*
 * 
 */
int main(int argc, char** argv)
{
int score = 100, bet = 1;
string card[CardsInHand];
string menuInput, menuInput2, menu1;
string cardValue[] = {"2", "3", "4", "5", "6", "7", "8", "9", "10", 
"Jack", "Queen", "King", "Ace"};
int cardConverted[CardsInHand];
string userInput;
int option;       
srand(time(0));
int match = 0;
int lettersConverted[9];
int i = 1;
char letters[] = "KKKKQQQQQJJJJJWWAAAC";
char slot[9];
string x, betChoice;
string userCards[3];
string dealerCards[3];
int userConverted[3];
int dealerConverted[3];

do
{
        cout << "Chips: " << score 
        << "    Welcome to the casino floor please choose a game!" << endl;
        cout << endl << "1) Poker" << endl;
        cout << endl << "2) Slot" << endl;
        cout << endl << "3) Baccarat" << endl;
    
        
        
        cin >> option;
        switch(option)
        {
        case 1:
            
            
       do
    {
        system("clear");
        cout << "5 card poker" << endl;
        
        cout << "Chips: " << score << "     Place your bet: " << endl;
        cin >> bet;
        cout << endl;
        
         for(int i = 0; i < 5; i++)
        {
        string x = cardValue[rand() % 13]; 
        cout << " " <<  x << " ";
        card[i] = x;
        
        } 
        cout << endl << endl;

            for(int i = 0; i < 5; i++)
            {
        
            cout << "Card " << i + 1 << ": " << card[i] << " Keep or discard? k/d " ;
            cin >> userInput;
                if ((tolower(userInput[0])) == 'd')
                    {
                    
                    string x = cardValue[rand() % 13];
                    card[i] = x ;
                    }
                else if((tolower(userInput[0])) == 'k')
                    {
                        card[i] = card[i];
                    }
                else
                    {
                        cout << "Invalid input";
                    }

            }
        cout << endl;
        convert_array(card, cardConverted);
            for(int i = 0; i < 5; i++)
             {
             cout << " " << card[i];
             }
        cout << endl << endl;
        
            if (fourOfAKind(cardConverted) == true)
            {
                cout << "Winner! Four of a Kind" << endl;
                score = score + (bet * 12);
            }  
            else if(fullHouse(cardConverted) == true)
            {
                cout << "Winner! Full House!" << endl;
                score = score + (bet * 20);

            }
            else if(threeOfAKind(cardConverted) == true)
            {
                cout << "Winner! Three of a Kind!" << endl;
                score = score + (bet * 9);
            }      
            else if(twoPair(cardConverted) == true)
            {
                cout << "Winner! Two Pair!" << endl;
                score = score + (bet * 6);
            }
            else if(onePair(cardConverted) == true)
            {
                cout << "Winner! One Pair!" << endl;
                score = score + (bet * 3);
            }   
            else
            {
                cout << "You lose  " << endl;
                score = score - bet;
            }

        cout << "Chips: " << score;
        cout << endl << "Would you like to play another hand? y/n" << endl;
        cin >> menu1;
        system("clear");
    }while(menu1 == "y" );
    cout << endl;
        break;
        
            
            
        case 2:
          
    

        
        do
        {
            system("clear");

        slot[0] = letters[rand() % 20]; 
        slot[1] = letters[rand() % 20]; 
        slot[2] = letters[rand() % 20]; 
        slot[3] = letters[rand() % 20]; 
        slot[4] = letters[rand() % 20]; 
        slot[5] = letters[rand() % 20]; 
        slot[6] = letters[rand() % 20]; 
        slot[7] = letters[rand() % 20]; 
        slot[8] = letters[rand() % 20];
        cout << "       Slots" << endl;
        cout << "---------------------" << endl;
        cout << setw(5) << "X" << setw(5) << "X" << setw(5) << "X" << endl;
        cout << setw(5) << "X" << setw(5) << "X" << setw(5) << "X" << endl;
        cout << setw(5) << "X" << setw(5) << "X" << setw(5) << "X" << endl;
        cout << "---------------------" << endl;

        cout << "Chips: " << score << " Please place your bet" << endl;
        cin >> bet;
             if (bet > score || bet <= 0 || bet == score || bet == 0)    
            {
             cout << "Invalid Bet";
             break;
            }	

             else 
            {
                 system("clear");
             score = score - bet;
             cout << "       Slots" << endl;
             cout << "---------------------" << endl;
             cout << setw(5) << slot[0] << setw(5) << slot[1] << setw(5) << slot[2] << endl;
             cout << setw(5) << slot[3] << setw(5) << slot[4] << setw(5) << slot[5] << endl;
             cout << setw(5) << slot[6] << setw(5) << slot[7] << setw(5) << slot[8] << endl;
             cout << "---------------------" << endl;
            }

        convert_array2(slot, lettersConverted);


        int row1 = lettersConverted[0] + lettersConverted[1] + lettersConverted[2];
        int row2 = lettersConverted[3] + lettersConverted[4] + lettersConverted[5];
        int row3 = lettersConverted[6] + lettersConverted[7] + lettersConverted[8];
        int row4 = lettersConverted[0] + lettersConverted[4] + lettersConverted[8];
        int row5 = lettersConverted[6] + lettersConverted[4] + lettersConverted[2];

        cout << "Pay Line 1: " ; row(row1, score, bet);
        cout << "Pay Line 2: " ; row(row2, score, bet);
        cout << "Pay Line 3: " ; row(row3, score, bet);
        cout << "Pay Top/Bottom: " ; row(row4, score, bet);
        cout << "Pay Bottom/Top: " ; row(row5, score, bet);
        cout << endl;


        cout << "Chips: " << score;

        cout  << "     Would you like to play again? Y/N";
        cin >> menuInput;

        system("clear");
        }while (tolower(menuInput[0]) == 'y');

        break;
            case 3:
                
    do
    {
        system("clear");
        int dealerBet = 0, playerBet = 0, tieBet = 0;   
        cout << "Chips: " << score << "      Baccarat" << endl;
       for(int i = 0; i < 3;i++)
       {
           x = cardValue[rand() % 13];
           userCards[i] = x;

       }
       for(int i = 0; i < 3;i++)
       {
           x = cardValue[rand() % 13];
           dealerCards[i] = x;

       }
       cout << "Would you like to bet on the player, dealer, or tie" << endl;
       cin >> betChoice;
       system("clear");
       cout << "Chips: " << score << "      ";
       cout << "How much would you like to bet? "<< endl;
       cin >> bet;
       score = score - bet;
       system("clear");    
       cout << "Chips: " << score << "      Baccarat" << endl;

       convert_array_bac(userCards, userConverted);
       convert_array_bac(dealerCards, dealerConverted);
       cout << "Players cards: ";
       cout << userCards[0] << " " << userCards[1] << endl;
       cout << "Dealers cards: ";
       cout << dealerCards[0] << " " << dealerCards[1] << endl;
       int playerHand = userConverted[0] + userConverted[1];
       int dealerHand = dealerConverted[0] + dealerConverted[1];

            if (playerHand >= 10)
            {
                playerHand = playerHand - 10;
            }
            else
            {

            }
            if (dealerHand >= 10)
            {
                dealerHand = dealerHand - 10;
            }
            else 
            {

            }
            cout << "Players hand value: " << playerHand << endl;
            cout << "Dealers hand value: " << dealerHand << endl;
            if (playerHand == 8 && dealerHand == 8)
            {
                cout << "Natural Tie " << endl;
                tieBet++;
            }
            else if (playerHand == 8 && dealerHand == 9)
            {
                cout << "Natural Tie " << endl;
                tieBet++;
            }
            else if (playerHand == 9 && dealerHand == 8)
            {
                cout << "Natural Tie " << endl;
                tieBet++;
            }
            else if (playerHand == 9 && dealerHand == 9)
            {
                cout << "Natural Tie " << endl;
                tieBet++;
            }
            else if (playerHand == 8 || playerHand == 9 )
            {
                cout << "Player wins natural" << endl;
                playerBet++;
            }
            else if (dealerHand == 8 || dealerHand == 9)
            {
                cout << "Dealer wins natural" << endl;
                dealerBet++;
            }
            else if (playerHand == 7 || playerHand == 6) 
            {
                cout << "Player Stays "  << endl;
                cout << endl;
                if (dealerHand <= 5)
                {
                    cout << "Dealer draws " << dealerCards[2] << endl;
                    cout << endl;
                    dealerHand = dealerHand + dealerConverted[2];
                    if (dealerHand >= 10)
                    {
                        dealerHand = dealerHand - 10;
                    }
                    else
                    {

                    }
                }

                else
                {

                }
                
            if (playerHand > dealerHand)
            {
                cout << "Players hand value: " << playerHand << endl;
                cout << "Dealers hand value: " << dealerHand << endl;
                cout << "Player wins " << endl;
                playerBet++;

            }
            else if (dealerHand > playerHand)
            {
                cout << "Players hand value: " << playerHand << endl; 
                cout << "Dealers hand value: " << dealerHand << endl;
                cout << "Dealer wins " << endl;
                dealerBet++;
            }
            else 
            { 
                cout << "Players hand value: " << playerHand << " Dealers hand value: " << dealerHand << endl;
                cout << "Tie" << endl;
                tieBet++;
            }
                
            }

            else if (playerHand <= 5)
            {
            cout << "Player draws a card " << userCards[2] << endl;
            playerHand = playerHand + userConverted[2];
                if (playerHand >= 10)
                {
                    playerHand = playerHand - 10;
                }
                else
                {

                }
            cout << "Players hand: " << playerHand << endl;
            
                if (userConverted[2] == 8 && dealerHand <= 2)
                {
                    cout << "Dealer draws a card " << dealerCards[2] << endl;
                    dealerHand = dealerHand + dealerConverted[2];
                }
                else if (userConverted[2] == 6 || userConverted[2] == 7 && dealerHand <= 6)
                {
                    cout << "Dealer draws a card " << dealerCards[2] << endl;
                    dealerHand = dealerHand + dealerConverted[2];
                }
                else if (userConverted[2] == 5 || userConverted[2] == 4 && dealerHand <= 5)
                {
                    cout << "Dealer draws a card " << dealerCards[2] << endl;
                    dealerHand = dealerHand + dealerConverted[2];
                }
                else if (userConverted[2] == 2 || userConverted[2] == 3 && dealerHand <= 4)
                {
                    cout << "Dealer draws a card " << dealerCards[2] << endl;
                    dealerHand = dealerHand + dealerConverted[2];
                }
                else if (userConverted[2] == 1 || userConverted[2] == 9 || userConverted[2] == 0 && dealerHand <= 3)
                {
                    cout << "Dealer draws a card " << dealerCards[2] << endl;
                    dealerHand = dealerHand + dealerConverted[2];
                }
                else
                {

                }
            
                if (dealerHand >= 10)
                {
                    dealerHand = dealerHand - 10;
                }
                else
                {

                }
            cout << "Dealers hand " << dealerHand << endl;
            cout << "Players hand " << playerHand << endl;
            max(playerHand, dealerHand, playerBet, dealerBet, tieBet);
            }
            else
            {

            }
            
            
            if (tolower(betChoice[0] == 'p' )&& playerBet == 1)
            {
                cout << "You Win!" <<  bet * 2 << endl;
                score = score + (bet * 2);
            }
            else if (tolower(betChoice[0] == 'p' )&& playerBet == 0)
            {
                cout << "You lose" << endl;
            }
            else
            {

            }
            
            
            if(tolower(betChoice[0] == 't' )&& tieBet == 1)
            {
                cout << "You Win!" << endl;
                score = score + (bet * 8);
            }
            else if(tolower(betChoice[0] == 't' )&& tieBet == 0)
            {
                cout << "You lose" << endl;
            }
            else
            {

            }

            
            if(tolower(betChoice[0] == 'd' )&& dealerBet == 1)
            {
                cout << "You Win!" << endl;
                score = score + (bet * 2);
            }
            else if(tolower(betChoice[0] == 'd' )&& dealerBet == 0)
            {
                cout << "You lose" << endl;
            }
            else
            {

            }
        cout << " Try again? " << endl;
        cin >> menuInput;
        system("clear");
        }while(tolower(menuInput[0] == 'y'));

        }
        
        
        

        

    } while (score >= 1);
    
    return 0;
}

bool onePair(int cardConverted[])
{
    for(int i = 0; i < CardsInHand; i++)
    {
        for(int p = i + 1; p < CardsInHand; p++)
        {
            if (cardConverted[i] == cardConverted[p])
            {
                return true;
            }
        }
    }return false;
    
}
bool twoPair(int cardConverted[])
{
    int pairCounter = 0;
    
        for(int i = 0; i < CardsInHand; i++)
    {
        for(int p = i + 1; p < CardsInHand; p++)
        {
            if (cardConverted[i] == cardConverted[p])
            {
                pairCounter++;
            }
        }
    }
    if(pairCounter == 2)
    {
        return true;
    }
    else
    { 
        return false;
    }
}
bool threeOfAKind(int cardConverted[])
{
    for (int i = 0; i < CardsInHand; i++)
    {
       int match = 0;
       for(int p = i + 1; p < CardsInHand; p++)
       {
       if (cardConverted[i] == cardConverted[p])
          {
                match++;
          }
            if(match == 2)
             {
        return true;
             }      
       }

    } return false;

}
bool fourOfAKind(int cardConverted[])
{
    for (int i = 0; i < CardsInHand; i++)
    {
       int match = 0;
       for(int p = i + 1; p < CardsInHand; p++)
       {
       if (cardConverted[i] == cardConverted[p])
          {
                match++;
          }
            if(match == 3)
             {
        return true;
             }      
       }

    } return false;

}
bool fullHouse(int cardConverted[])
{
    int pair = 0;
    int trip = 0;
    int TripleCard = 0;
    for (int i = 0; i < CardsInHand; i++)
    {
       
       
       int match = 0;
       for(int p = i + 1; p < CardsInHand; p++)
       {
       if (cardConverted[i] == cardConverted[p])
          {
                match++;
          }
            if(match == 2)
             {
                TripleCard = cardConverted[i];
                trip++;
             }      
       }
     }      
    if (TripleCard == 0)
    {
        
    }
    for(int i = 0; i < CardsInHand; i++)
             {
                for(int p = i + 1; p < CardsInHand; p++)
                {
                    if (cardConverted[i] == cardConverted[p] && cardConverted[i] 
                            != TripleCard)
                    {
                        pair++;
                    }
                }
            }
    if (pair == 1 && trip == 1)
    {
        return true;
    }
    else
    {
        return false;
    }
    
}

int convert_string(string card)
{
	if(card == "2")
	{
		return 2;
	}
	else if(card == "3")
	{
		return 3;
	}
	else if(card == "4")
	{
		return 4;
	}
	else if(card == "5")
	{
		return 5;
	}
	else if(card == "6")
	{
		return 6;
	}
	else if(card == "7")
	{
		return 7;
	}
	else if(card == "8")
	{
		return 8;
	}
	else if(card == "9")
	{
		return 9;
	}
	else if(card == "10")
	{
		return 10;
	}
	else if(card == "Jack")
	{
		return 11;
	}
	else if(card == "Queen")
	{
		return 12;
	}
	else if(card == "King")
	{
		return 13;
	}
	else
	{
		return 14;
	}
}
void convert_array(string cardValue[], int cardConverted[])
{
	for(short i = 0; i < CardsInHand; i++)
	{
		cardConverted[i] = convert_string(cardValue[i]);
	}
	return;
}

int row(int row1, int& score, int bet)
{
       if (row1 == 1 || row1 == 2 || row1 == 3)
    {
        cout << "Winner Jacks!   You Win: " << bet * 2 << " Chips" << endl;
        score = score + (bet * 1);
    }
    else if (row1 == 4 || row1 == 8 || row1 == 12)
    {
        cout << "Winner Queens!   You Win: " << bet * 3 << " Chips" << endl;
        score = score + (bet * 2);
    }   
    else if (row1 == 20 || row1 == 60 || row1 == 80)
    {
        cout << "Winner Kings!   You Win: "  << bet * 4 << " Chips" << endl;
        score = score + (bet * 3);
    }
    else if (row1 == 50 || row1 == 100 || row1 == 150)
    {
        cout << "Winner Aces!   You Win:  "  << bet * 5 << " Chips" << endl;
        score = score + (bet * 4);
    }
    else if(row1 == 1000)
    {
        cout << " One cherry!   You Win: " << bet * 1 << " Chips" << endl;
        score = score + (bet * 1);
    }
    else if(row1 == 2000)
    {
        cout << " 2 cherries!   You Win: " <<  bet * 12 << " Chips" << endl;
        score = score + (bet * 12);
    }
    else if(row1 == 3000)
    {
        cout << " 3 cherries!   You Win: " <<  bet * 30 << " Chips" << endl;
        score = score + (bet * 30);
    } 
    else if(row1 == 0)
    {
        cout << "Wild JackPot!   You Win: " <<  bet * 20 << "Chips" << endl;
        score = score + (bet * 20);
    }
    else
    {
        cout << "no winning lines" << endl;
    }
}

int convert_char(char slot)
{
	if(slot == 'J')
	{
		return 1;
	}
	else if(slot == 'Q')
	{
		return 4;
	}
	else if(slot == 'K')
	{
		return 20;
	}
	else if(slot == 'A')
	{
		return 50;
	}
        else if (slot == 'W')
        {
            return 0;
        }
        else
        {
            return 1000;
        }


}
void convert_array2(char letters[], int lettersConverted[])
{
	for(short i = 0; i < 9; i++)
	{
		lettersConverted[i] = convert_char(letters[i]);
	}
	return;
}
int convert_string_bac(string card)
{
	if(card == "2")
	{
		return 2;
	}
	else if(card == "3")
	{
		return 3;
	}
	else if(card == "4")
	{
		return 4;
	}
	else if(card == "5")
	{
		return 5;
	}
	else if(card == "6")
	{
		return 6;
	}
	else if(card == "7")
	{
		return 7;
	}
	else if(card == "8")
	{
		return 8;
	}
	else if(card == "9")
	{
		return 9;
	}
	else if(card == "10")
	{
		return 0;
	}
	else if(card == "Jack")
	{
		return 0;
	}
	else if(card == "Queen")
	{
		return 0;
	}
	else if(card == "King")
	{
		return 0;
	}
	else
	{
		return 1;
	}
}
void convert_array_bac(string cardValue[], int cardConverted[])
{
	for(short i = 0; i < 3; i++)
	{
		cardConverted[i] = convert_string_bac(cardValue[i]);
	}
	return;
}
int max(int playerHand, int dealerHand, int& playerBet,  int& dealerBet, int& tieBet)
{
    if(playerHand > dealerHand)
    {
        cout << "Player Hand " << playerHand << " beats Dealer hand " << dealerHand << endl;
        
        return playerBet++;
       
    }
    else if(dealerHand > playerHand)
    {
        cout << "Dealer Hand " << dealerHand << " beats " << " Player Hand " <<  playerHand << endl;
        return dealerBet++;
    }
    else
    {
        cout << playerHand << " tie " << dealerHand << endl;
        return tieBet++;
    }
}