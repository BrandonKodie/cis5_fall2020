#include<iostream>
#include <cstdlib>
#include<string>

using namespace std;

/*
 * 
 */
const int CardsInHand = 5;
void convert_array(string cardValue[], int cardConverted[]);
int convert_string(string cardConverted);
bool onePair(int cardConverted[]);
bool twoPair(int cardConverted[]);
bool threeOfAKind(int cardConverted[]);
bool fourOfAKind(int cardConverted[]);
bool fullHouse(int cardConverted[]);
int main(int argc, char** argv)
{   
    
    cout << " Welcome to the 5 card poker table!" << endl;
    string menu;
    do
  {
    int cardConverted[CardsInHand];
    srand (time(0));

    
    cout << endl;
        string card[CardsInHand];
    for(int i = 0; i < 5; i++)
    {
        string cardValue[] = {"2", "3", "4", "5", "6", "7", "8", "9", "10",
        "Jack", "Queen", "King", "Ace"};
        string x = cardValue[rand() % 13]; 
        cout << " " <<  x << " ";
        card[i] = x;
    } 
        cout << endl << endl;
        /*cout << " Card 1: " << card[0] << endl;
        cout << " Card 2: " << card[1] << endl;
        cout << " Card 3: " << card[2] << endl;
        cout << " Card 4: " << card[3] << endl;
        cout << " Card 5: " << card[4] << endl;
       */ 
        for(int i = 0; i < 5; i++)
        {
            string userInput;
            cout << "Card " << i + 1 << ": " << card[i] << " Keep or discard? k/d " ;
            cin >> userInput;
            if ((tolower(userInput[0])) == 'd')
            {
        string cardValue[] = {"2", "3", "4", "5", "6", "7", "8", "9", "10",
        "Jack", "Queen", "King", "Ace"};
        string x = cardValue[rand() % 13];
                card[i] = x ;
            }
            else if((tolower(userInput[0])) == 'k')
            {
             card[i] = card[i];
            }
            else
            {
                cout << "Invalid input";
            }
        
        }
        cout << endl;
         convert_array(card, cardConverted);
         for(int i = 0; i < 5; i++)
         {
             cout << " " << card[i];
         }
         cout << endl << endl;
         


        if (fourOfAKind(cardConverted) == true)
        {
            cout << "Winner! Four of a Kind" << endl;
        }  
        else if(fullHouse(cardConverted) == true)
        {
            cout << "Winner! Full House!" << endl;
        }
        else if(threeOfAKind(cardConverted) == true)
        {
            cout << "Winner! Three of a Kind!" << endl;
        }      
        else if(twoPair(cardConverted) == true)
        {
            cout << "Winner! Two Pair!" << endl;
        }
        else if(onePair(cardConverted) == true)
        {
            cout << "Winner! One Pair!" << endl;
        }   
        else
        {
            cout << "You lose  " << endl;
        }
        
        
            cout << endl << "Would you like to play another hand?" << endl;
            cin >> menu;
    }while(menu == "y" );
    return 0;
    
}

bool onePair(int cardConverted[])
{
    for(int i = 0; i < CardsInHand; i++)
    {
        for(int p = i + 1; p < CardsInHand; p++)
        {
            if (cardConverted[i] == cardConverted[p])
            {
                return true;
            }
        }
    }return false;
    
}
bool twoPair(int cardConverted[])
{
    int pairCounter = 0;
    
        for(int i = 0; i < CardsInHand; i++)
    {
        for(int p = i + 1; p < CardsInHand; p++)
        {
            if (cardConverted[i] == cardConverted[p])
            {
                pairCounter++;
            }
        }
    }
    if(pairCounter == 2)
    {
        return true;
    }
    else
    { 
        return false;
    }
}
bool threeOfAKind(int cardConverted[])
{
    for (int i = 0; i < CardsInHand; i++)
    {
       int match = 0;
       for(int p = i + 1; p < CardsInHand; p++)
       {
       if (cardConverted[i] == cardConverted[p])
          {
                match++;
          }
            if(match == 2)
             {
        return true;
             }      
       }

    } return false;

}
bool fourOfAKind(int cardConverted[])
{
    for (int i = 0; i < CardsInHand; i++)
    {
       int match = 0;
       for(int p = i + 1; p < CardsInHand; p++)
       {
       if (cardConverted[i] == cardConverted[p])
          {
                match++;
          }
            if(match == 3)
             {
        return true;
             }      
       }

    } return false;

}
bool fullHouse(int cardConverted[])
{
    int pair = 0;
    int trip = 0;
    int TripleCard = 0;
    for (int i = 0; i < CardsInHand; i++)
    {
       
       
       int match = 0;
       for(int p = i + 1; p < CardsInHand; p++)
       {
       if (cardConverted[i] == cardConverted[p])
          {
                match++;
          }
            if(match == 2)
             {
                TripleCard = cardConverted[i];
                trip++;
             }      
       }
     }      
    if (TripleCard == 0)
    {
        
    }
    for(int i = 0; i < CardsInHand; i++)
             {
                for(int p = i + 1; p < CardsInHand; p++)
                {
                    if (cardConverted[i] == cardConverted[p] && cardConverted[i] 
                            != TripleCard)
                    {
                        pair++;
                    }
                }
            }
    if (pair == 1 && trip == 1)
    {
        return true;
    }
    else
    {
        return false;
    }
    
}

int convert_string(string card)
{
	if(card == "2")
	{
		return 2;
	}
	else if(card == "3")
	{
		return 3;
	}
	else if(card == "4")
	{
		return 4;
	}
	else if(card == "5")
	{
		return 5;
	}
	else if(card == "6")
	{
		return 6;
	}
	else if(card == "7")
	{
		return 7;
	}
	else if(card == "8")
	{
		return 8;
	}
	else if(card == "9")
	{
		return 9;
	}
	else if(card == "10")
	{
		return 10;
	}
	else if(card == "Jack")
	{
		return 11;
	}
	else if(card == "Queen")
	{
		return 12;
	}
	else if(card == "King")
	{
		return 13;
	}
	else
	{
		return 14;
	}
}
void convert_array(string cardValue[], int cardConverted[])
{
	for(short i = 0; i < CardsInHand; i++)
	{
		cardConverted[i] = convert_string(cardValue[i]);
	}
	return;
}
