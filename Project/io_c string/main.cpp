#include<iostream>
#include <cstdlib>
#include<fstream>
using namespace std;

// Write function that reads from an input
// And writes that input to the console
// Write a void function
void readFromInput(istream& input);

/*
 * 
 */
int main(int argc, char** argv)
{
    ofstream out;
    out.open("test.txt", ios::app);
    out << "test file 1";
    out.close();

    // C-Strings
    // C-Strings are strings that end in a '\0' (null terminator)
    // escaped character '\n'
    string x = "Hello World";
    
    // Syntax c-string
    // A set of characters
    // A set is also known as an array
    // Also as a collection for container
    // ex: file contains 20 possible characters
    // In reality, can only store 19 because of null terminator
    char file[20];
    
    // Get an input from the user
    cout << " Please enter a word" << endl;
    cin >> file;
    cout << endl;
    cout << " You entered: " << file << endl;
    
    cout << "Enter another value";
    readFromInput(cin);
    
   // read from the file writen in the begining 
    //
    ifstream fin;
    fin.open("test.txt");
     
    cout << endl << endl;
    //Error check the file
    if (fin.fail())
    {
        cout << "The reading of the file failed. It does not exisit ";
    }
    else
    {
        // Read from the file
        // Use a while loop to read from the while
        while (!fin.eof())
        {
            readFromInput(fin);
        }
    }
    fin.close();
    
    // Open the file with flags
    // Use the fstream datatype
    fstream fileStream;
    
    // ios::in allows input. ios::out allows output
    // ios::app allows appending
    fileStream.open("test.txt", ios::out | ios::app );
    fileStream << "Hello I am another line";
    fileStream.close();
    return 0;
}
// Write function that reads from an input
// And writes that input to the console
// Write a void function
//iStream is a dataType that support input from both files and user (consol)
// Input is like cin
void readFromInput(istream& input)
{
    string value;
    input >> value;
    
    cout << "The value is: " << value << endl;
}