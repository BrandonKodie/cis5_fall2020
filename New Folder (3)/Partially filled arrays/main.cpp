#include<iostream>
#include <cstdlib>

using namespace std;

void output(int a[], int size);
void addValue(int a[], int& size, int capacity, int value);
void removeValue(int a[], int &size);


int main(int argc, char** argv)
{
    
    //Partially filled array
    // NEED:
    //Array
    //Size - Number of elements in array
    // Capacity - Total possible number of elements
    int capacity = 6;
    int size = 0;
    int array[capacity];
    
    output(array, size);
    // Ask users for numbers    
    //Output the array after every input    
    //Notify the user if the array is full
    //Give the user an option to remove the last one
    
    cout << " Would you like to modify the array y/n" << endl;
    string input;
    cin >> input;
    
    while(tolower(input[0]) == 'y')
    {
        // Ask the user whether they want to add or remove a value
        //output the initial array
        cout << "Current array: ";
        output(array, size);
        
        cout << "Would you like to add or remove a value? " << endl;
        cout << "1) Adding a value" << endl;
        cout << "2) Removing a value" << endl;
        
        // Get input from user!
        int option;
        cin >> option;
        
        switch(option)
        {
            case 1: // Adding a value
                cout << "Please enter a value";
                int value;
                cin >> value;
                
                addValue(array, size, capacity, value);
                break;
            case 2:
                               
                //Validate that there are elements to remove
                removeValue(array, size);
                break;
            default:
                cout << "Not an option!" << endl;
        }
        
        cout << "After manipulation: ";
        output(array, size);
        
        cout << " Would you like to modify the array again y/n?";
        cin >> input;
    }
    
    
    
    return 0;
}
/**
 * This function outputs any array,
 * @param a the array
 * @param size
 */
void output(int a[], int size)
{
    for (int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}
/**
 * Adds a value to the very end of the array(appending)
 * @param a array
 * @param size the current array size
 * @param capacity total number of possible elements
 */
void addValue(int a[], int& size, int capacity, int value)
{
    if (size < capacity)
        {
            a[size] = value;
            size++;
        }
        else
        {
            cout << "The array is full" << endl;
        }
}
/**
 * Removes  a single value from te end of the partially filled array
 * @param a the given array
 * @param size the current size of the array
 */
void removeValue(int a[], int &size)
{
    if (size > 0)
    {
        size--;
    }
    else
    {
        cout << "Array is empty. Nothing to delete" << endl;
    }
}