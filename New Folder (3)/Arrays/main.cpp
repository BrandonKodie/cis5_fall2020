#include <cstdlib>
#include<iostream>
#include<vector>
using namespace std;

// Output function
void output(int a[], int size);
void output(double a[], int size);
void getValuesFromUser(double a[], int size);


/*
 * 
 */
int main(int argc, char** argv)
{

    
    //Arrays
    //syntax:
    //data_type name[numberOfElements AKA size]
    
    //ex. 
    char file[20]; // character array, c-strings
    
    // ex 2
    int size = 5;
    int a[size]; // int array size 5
    
    //Once array is defined, the size can not change
    //Always need to know the size of the array
    // Still use the subscript operator
    a[0] = 2;
    a[3] = 7;
    
    // To do array manipulation, use a for loop

    output(a, size);
    
    // Unable to return arrays from functions
    
    // Create another array
    int userValues = 3;
    double values[userValues];
    getValuesFromUser(values, userValues);
    
    output(values, userValues);
    
    // Other ways to initialize arrays
    int test[2] = {1, 2};
    output(test, size);
    
    int test1[] = {1, 2, 3, 4, 5};
    output(test1, 5);
    
    test1[0 + 4] = 20; // test1[4] = 20;
    
    return 0;
}
void output(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}
void output(double a[], int size)
{
        for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}
void getValuesFromUser(double a[], int size)
{
        for (int i = 0; i < size; i++)
    {
        cout << "Please enter a number: ";
        double value;
        cin >> value;
        
        a[i] = value;
    }
}