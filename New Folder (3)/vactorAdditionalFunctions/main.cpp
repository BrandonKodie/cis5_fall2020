#include<iostream>
#include <cstdlib>
#include<vector>
using namespace std;

// Functional PROTOTYPES
int maxValue(const vector<int>& v);
int minValue(const vector<int>& v);
double averageValue(const vector<int>& v);
void output(const vector<int> values);
/*
 * 
 */
int main(int argc, char** argv)
{
    vector<int> values;
    values.push_back(3);
    values.push_back(44);
    values.push_back(111);
    values.push_back(11);
    values.push_back(25);
    output(values);
    cout << "The max is: " << maxValue(values) << endl;
    output(values);
    cout << "The min is: " << minValue(values) << endl;
    
    cout << "The average value is:" << averageValue(values) << endl;
    return 0;
}
/**
 * the function figures out the largest number within the vector
 * There is no current error checking on the vector
 * @param v the vector
 * @return  the highest value
 */
int maxValue(const vector<int>& v)
{
    // Create a temporary max value 
    int max = -99999999;
    
    // Check every single element
    // if the value is larger, it is the new max
    for(int i = 0; i < v.size(); i++)
    {
        // Compare value with the current max
        if(v[i] > max)
        {
            // We have a new maximum 
            max = v[i];
        }
    }
    return max;
}
void output(const vector<int> values)
{
    //loop
    for(int i = 0; i < values.size(); i++)
    {
        cout << values[i] << " ";
    }
    cout << endl;
}
int minValue(const vector<int>& v)
{
        // Create a temporary max value 
    int min = 99999999;
    
    // Check every single element
    // if the value is larger, it is the new max
    for(int i = 0; i < v.size(); i++)
    {
        // Compare value with the current max
        if(v[i] < min)
        {
            // We have a new maximum 
            min = v[i];
        }
    }
    return min;
}
double averageValue(const vector<int>& v)
{
    // Need to keep track of the total value
    int total = 0;
    
    // loop across all elements and add to total
    for (int i = 0; i < v.size(); i++)
    {
        total += v[i];
    }
    // Don't forget to static cast
    return total / static_cast<double>(v.size());
}