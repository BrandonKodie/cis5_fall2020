#include<iostream>
#include<vector>
#include <cstdlib>

using namespace std;
//void deleteVectorLocation(vector<int> &v, int location);
void deleteOrdered(vector<int>& v, int loc);
int main(int argc, char** argv)
{   
    vector<int> v1 = {3, 4, 5, 6, 7,};
    int location;
    for(int i = 0; i < v1.size(); i++)
    {
    cout << v1[i] << endl;
    }
    cout << "Please enter a vector location to remove: " << endl;
    cin >> location;
    cout << endl;
    //deleteVectorLocation(v, remove);
    deleteOrdered(v1, location);

    cout << endl;
    for(int i : v1)
        cout << i << endl;
    return 0;
}
/*void deleteVectorLocation(vector<int> &v, int location)
{
    v.erase(v.begin()+location);
}
 * */
void deleteOrdered(vector<int>& v, int loc)
{
     if(loc > 0 && loc < v.size())
    {
         for(int i = loc; i < v.size() - 1; i++)
         {
             v[i] = v[i + 1];
         }
         v.pop_back();
    }
    else
    {
        cout << "Invalid location provided" << endl;
    }
}