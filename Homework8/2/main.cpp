#include<iostream>
#include <cstdlib>

using namespace std;

void deleteOrdered(int a[], int loc, int size);
void removeValue(int a[], int &size);
void output(int a[], int size);

int main(int argc, char** argv)
{
    int size2 = 5;
    int location2;
    int v2[size2] = {1, 2, 3, 4, 5};
    
    output(v2, size2);
    
    cout << "Select a location to delete: " << endl;
    cin >> location2;

    deleteOrdered(v2, location2, size2);
    
    removeValue(v2, size2);
    
    output(v2, size2);

    return 0;
}
void deleteOrdered(int a[], int loc, int size)
{
     if(loc > 0 && loc < size)
    {
         //Shift
         for(int i = loc; i < size - 1; i++)
         {
             a[i] = a[i + 1];
         }
        
    }
    else
    {
        cout << "Invalid location provided" << endl;
    }
}
void removeValue(int a[], int &size)
{
    if (size > 0)
    {
        size--;
    }
    else
    {
        cout << "Array is empty. Nothing to delete" << endl;
    }
}
void output(int a[], int size)
{
    for (int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}