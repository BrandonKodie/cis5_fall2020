#include<iostream>
#include <cstdlib>
#include<vector>

using namespace std;
/**
 * deletes a given location by shifting all following values down a location
 * then pops the end
 * 
 * @param v the vector to be altered
 * @param loc the location to start
 */
void deleteOrdered(vector<int>& v, int loc);
/**
 * outputs vector
 * @param v the vector
 */
void output(const vector<int>& v);
/**
 * 
 * @param v the vector to be looped through
 * @param number the user given number to search for
 * @param location the vector location containing the given number
 * @return location
 */
int findLocation(const vector<int> v, int number, int& location);
int main(int argc, char** argv)
{
    vector<int> values;
    values.push_back(1);
    values.push_back(3);
    values.push_back(6);
    values.push_back(5);
    values.push_back(7);
    values.push_back(4);
    
    output(values);
    cout << "Please enter a number :" << endl;
    int number, location;
    cin >> number;

    output(values);
    
    findLocation(values, number, location);
    
    deleteOrdered(values, location);
    
    output(values);

    return 0;
}

void deleteOrdered(vector<int>& v, int loc)
{
     if(loc > 0 && loc < v.size())
    {
         for(int i = loc; i < v.size() - 1; i++)
         {
             v[i] = v[i + 1];
         }
         v.pop_back();
    }
    else
    {
        cout << "Invalid location provided" << endl;
    }
}
void output(const vector<int>& v)
{
    for (int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}
int findLocation(const vector<int> v, int number, int& location)
{
    location = 0;
        for (int i = 0; i < v.size(); i++)
    {
        if(v[i] == number)
        {
            cout << "Vector location " << i << " matches the number given " << endl;
            location = i;
        }
        else
        {
            cout << "Vector location " << i << " does not match the number given " << endl;
        }
    } 
        return location;
}