#include<iostream>
#include <cstdlib>

using namespace std;

void deleteOrdered(int a[], int loc, int size);
void removeValue(int a[], int &size);
void output(int a[], int size);
int findLocation(int a[], int size, int number, int& location);

int main(int argc, char** argv)
{
    int size5 = 5;
    int location5;
    int a5[size5] = {1, 2, 3, 4, 5};
    
    output(a5, size5);
    
    cout << "Select a location to delete: " << endl;
    cin >> location5;

    deleteOrdered(a5, location5, size5);
    
    removeValue(a5, size5);
    
    output(a5, size5);

    return 0;
}
void deleteOrdered(int a[], int loc, int size)
{
     if(loc > 0 && loc < size)
    {

         for(int i = loc; i < size - 1; i++)
         {
             a[i] = a[i + 1];
         }
        
    }
    else
    {
        cout << "Invalid location provided" << endl;
    }
}
void removeValue(int a[], int &size)
{
    if (size > 0)
    {
        size--;
    }
    else
    {
        cout << "Array is empty. Nothing to delete" << endl;
    }
}
void output(int a[], int size)
{
    for (int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}
int findLocation(int a[], int size, int number, int& location)
{
    location = 0;
        for (int i = 0; i < size; i++)
    {
        if(a[i] == number)
        {
            cout << "Vector location " << i << " matches the number given " << endl;
            location = i;
        }
        else
        {
            cout << "Vector location " << i << " does not match the number given " << endl;
        }
    } 
        return location;
}