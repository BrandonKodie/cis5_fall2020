#include<iostream>
#include <cstdlib>
#include<vector>

using namespace std;
void output(const vector<int>& v);
void output(int a[], int size);
void output(char a[],int capacity);
void deleteOrdered(vector<int>& v, int loc);
void deleteOrdered(int a[], int loc, int size);
void checkVector(vector<int> v, int number);
bool vectorCheck(vector<int> v, int number);
void removeValue(int a[], int &size);
void shiftArray(char a[], int location, int &size);
void delete_repeats(char a[], int &size);
int returnLocation(const vector<int> v, int number);
int main(int argc, char** argv)
{   
    int size2 = 5;
    int size5 = 5;
    int capacity8 = 10;
    int size8 = 8;
    char a8[capacity8];
    
    vector<int> v6 = {1, 3, 6, 5, 7, 4};
    vector<int> v7 = {1, 3, 6, 5, 7, 4 };
    vector<int> values3 = {1, 3, 6, 5, 7, 4 };
    vector<int> values4 = {1, 3, 6, 5, 7, 4 };
    int a2[size2] = {1, 2, 3, 4, 5};
    vector<int> v1 = {3, 4, 5, 6, 7,};
    int a5[size5] = {1, 2, 3, 4, 5};
    
    int number5, number6, number7, number4, location4, number3, location3;
    int location2, option, location;
    
    string menuInput;
    
    do
    {

        cout << "Homework 8 Menu" << endl;
        cout << "Problem 1" << endl;
        cout << "Problem 2" << endl;
        cout << "Problem 3" << endl;
        cout << "Problem 4" << endl;
        cout << "Problem 5" << endl;
        cout << "Problem 6" << endl;
        cout << "Problem 7" << endl;
        cout << "Problem 8" << endl;
        cout << endl << "Please select a problem to review" << endl;
        cin >> option;

        switch(option)
            {


            case 1:

                output(v1); // Displays the vector

                cout << "Please enter a vector location to remove: " << endl;
                cin >> location; // Location of number to delete v1[location]

                cout << endl;

                deleteOrdered(v1, location);
                // Deletes number at the location then shifts remaining numbers
                //down

                output(v1);

                cout << endl;

                break;

            case 2:

                output(a2, size2); // display array

                cout << "Select a location to delete: " << endl;
                cin >> location2; // user inputs location to delete

                deleteOrdered(a2, location2, size2);
                //Location is deleted and other numbers are shifted down 
                removeValue(a2, size2);
                // takes one away from size to since we removed a number
                output(a2, size2); // displays the array with the number removed


                break;

            case 3:

                cout << "Please enter a number to remove:" << endl;
                output(values3); // output values3 vector

                cin >> number3; // gets number from the user

                location3 = returnLocation(values3, number3);
                // Finds the location of the given number and returns it

                deleteOrdered(values3, location3);

                output(values3);

                break;

            case 4:

                output(values4); // outputting values4 vector

                cout << "Please enter a number to remove:" << endl;
                cin >> number4;
                // Gets number from user

                location4 = returnLocation(values4, number4);
                //Returns the location of the number given by user

                deleteOrdered(values4, location4);
                // Deletes the number at the returned location the shifts
                // all remaining numbers down to keep the order

                cout << "Output after deletion " << endl;

                output(values4);

                break;

            case 5:

                output(a5, size5); // output array

                cout << "Select a location to delete: " << endl;
                cin >> number5; // user gives a number to delete

                deleteOrdered(a5, number5, size5);
                // Removes the given number and shifts the remaining numbers down
                // to maintain the order

                removeValue(a5, size5);
                //Decreases the size to make up for number that was deleted

                output(a5, size5);
                // outputs the array with the number removed and size decreased

                break;

            case 6:

                output(v6); // Displays the vector v6

                cout << "Enter a number to check the vector for " << endl;
                cin >> number6; // Gets the number to look for from user

                checkVector(v6, number6);
                // checks to see if the given number is contained in the vector

                break;

            case 7:

                output(v7); // Outputs vector v7
                while(number7 != -999) // Loops until user enters -999
                {

                cout << "Enter a number to add to vector, enter -999 to stop "
                        << endl;
                cin >> number7; // Gets a number from the user
                        // if bool vectorCheck if true
                    if(vectorCheck(v7, number7) == true)
                    {//Add the given number to the vector
                        v7.push_back(number7); 
                    }
                    else // If bool is false tell the user that the given number is invalid
                    {
                        cout << number7 << " is already contained in the vector"
                                " and will not be recorded" << endl;
                    }
                output(v7);  // Outputs the vector          
                }
                break;

            case 8:

                a8[0] = 'a';
                a8[1] = 'b';
                a8[2] = 'a';
                a8[3] = 'c'; // Sets some values fot the partially filled array a8
                a8[4] = 'c';
                a8[5] = 'b';
                a8[6] = 'd';
                a8[7] = 'e';

                output(a8, size8); //outputs a8[] with the current array size

                delete_repeats(a8, size8); // Checks array for matching chars
                // if found replaces the duplicate char by shifting the array down         
                output(a8, size8);

                break;

            default:

                cout << "Invalid input. Please enter 1-8" << endl;
                // displays if input is not 1-8
            }
        cout << "Return to main menu? y/n" << endl;
        cin >> menuInput;
    }while (tolower(menuInput[0]) == 'y'); 

    return 0;
}
/**
 * Deletes location by shifting the following vector elements down to replace loc
 * @param v name of the vector
 * @param loc is the vector location
 */
void deleteOrdered(vector<int>& v, int loc)
{
     if(loc > 0 && loc < v.size())
    {
         for(int i = loc; i < v.size() - 1; i++)
         {
             v[i] = v[i + 1];
         }
         v.pop_back();
    }
    else
    {
        cout << "Invalid location provided" << endl;
    }
}
/**
 * Deletes location by shifting the following array elements down to replace loc
 * @param a is the name of the array
 * @param loc is the element location
 * @param size is the size of array
 */
void deleteOrdered(int a[], int loc, int size)
{
     if(loc > 0 && loc < size)
    {
         //Shift
         for(int i = loc; i < size - 1; i++)
         {
             a[i] = a[i + 1];
         }        
    }
    else
    {
        cout << "Invalid location provided" << endl;
    }
}
/**
 * Decreases the size of a partially filled array
 * @param a is the array name
 * @param size is the current size of the partially filled array
 */
void removeValue(int a[], int &size)
{
    if (size > 0)
    {
        size--;
    }
    else
    {
        cout << "Array is empty. Nothing to delete" << endl;
    }
}
/**
 * Prints array to screen
 * @param a is the name of the array
 * @param size is the current size 
 */
void output(int a[], int size)
{
    for (int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}
/**
 * Outputs vector
 * @param v is the name of the vector
 */
void output(const vector<int>& v)
{
    for (int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}
/**
 * 
 * @param v the vector to be looped through
 * @param number the user given number to search for
 * @param location the vector location containing the given number
 * @return location
 */
int returnLocation(const vector<int> v, int number)
{
    int location = -1;
    for (int i = 0; i < v.size(); i++)
    {
        if(v[i] == number)
        {
            cout << "Vector location " << i << " matches the number given " 
                    << endl;
            location = i;
            return location;
        }
        else
        {
            cout << "Vector location " << i << " does not match the number "
                    "given " << endl;
        }
    } 
    return location;
}
/**
 * 
 * @param v
 * @param number
 */
void checkVector(vector<int> v, int number)
{
    int count = 0;

    for(int i = 0; i < v.size(); i++)
    {
        if(v[i] == number)
        {
            count++;
        }       
    }
        if(count > 0)
        {
            cout << "The number " << number << " is already contained in this"
                    " vector" << endl;
        }
        else
        {
            cout << "The number " << number << " is not yet contained in this "
                    "vector" << endl;
        }   
}
/**
 * Checks vector to see in a given number is contained inside
 * @param v is name of the vector
 * @param number is the value that we are checking the vector for
 * @return false if the number is contained in the vector, true if not
 */
bool vectorCheck(vector<int> v, int number)
{
    for(int i = 0; i < v.size(); i++)
    {
        while(v[i] == number)
        {
            return false;
        }
    }
    return true;
}
/**
 * Removes the char at the location given by shifting the rest of the 
 * array down to the location of the removed char
 *
 * @param a the array given
 * @param location is the location of the element to remove
 * @param size is the used size of the partially filled array
 */
void shiftArray(char a[], int location, int &size)
{
     if(location > 0 && location < size)
    {
         for(int i = location; i < size - 1; i++)
         {
             a[i] = a[i + 1];
         }
         size--;
    }
    else
    {
        
    }
}
/**
 * Searches array given for repeats then uses shiftArray function to delete 
 * repeats
 * @param a the array given
 * @param size the current size of the partially filled array
 */
void delete_repeats(char a[], int &size)
{
    for(int i = 0; i < size; i++)
    {
        for(int k = i + 1;k < size;k++)
        {
            if(a[i] == a[k])
            {
                shiftArray(a, k, size);

            }
        }
    }
    
}
/**
 * Prints array to screed
 * @param a array given
 * @param capacity size of array
 */
void output(char a[],int capacity)
{
    for(int i = 0; i < capacity; i++ )
    {
        cout << a[i] << " ";
    }
    cout << endl;
   
}