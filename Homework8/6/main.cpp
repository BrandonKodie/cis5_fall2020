#include <vector>
#include <cstdlib>
#include <iostream>
using namespace std;



void checkVector(vector<int> v, int number);
void output(const vector<int>& v);
int main(int argc, char** argv)
{
    vector<int> v6 = {1, 3, 6, 5, 7, 4};
    int number6;

    
    output(v6);
    cout << "Enter a number to check the vector for " << endl;
    cin >> number6;
    
    checkVector(v6, number6);
    
    return 0;
}

void checkVector(vector<int> v, int number)
{
    int count = 0;

    for(int i = 0; i < v.size(); i++)
    {
        while(v[i] == number)
        {
            count++;
        }       
    }
        if(count > 0)
        {
            cout << "The number " << number << " is already contained in this vector"
                    << endl;
        }
        else
        {
            cout << "The number " << number << " is not yet contained in this vector"
                    << endl;
        }   
}
void output(const vector<int>& v)
{
    for (int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}