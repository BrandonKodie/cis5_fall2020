#include <iostream>
#include <cstdlib>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
        double score, scoresum, possiblesum;
        double percentage;
        int input;
        int pointspossible;
        
        cout << "How many exercises to input?";
        cin >> input;

        for (int i = 0; i < input; i++)
        {
            cout << "Score received for exercise   " << i + 1 << endl;
            cin >> score;
            cout << "Total points possible for exercise " << i + 1 << endl;
            cin >> pointspossible;
            scoresum = scoresum + score;
            possiblesum = possiblesum + pointspossible;
            percentage = (scoresum / possiblesum) * 100;
        }
       
        
       
        
        cout << "Your total is " << scoresum << " out of " << possiblesum << ", or " 
                 << setprecision(4) << percentage << "%" << endl;
        
        
    return 0;
}

