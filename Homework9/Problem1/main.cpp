#include<vector>
#include <cstdlib>
#include<iostream>

using namespace std;


void output(const vector<int> ticket);
void output(const vector<string> names);
void output(const vector<int> &ticket, const vector<string>& names);

int main(int argc, char** argv)
{
    srand(time(0));
    
    vector<int> ticket;
    vector<string> names;
    int high = 40;
    int low= 1;
    cout << "Would you like to add a name? y/n" ;
    string input;
    cin >> input;
    
    while(tolower(input[0]) == 'y')
    {
        cout << "Please enter a name: ";
        string name;
        cin >> name;
        
        names.push_back(name);
        ticket.push_back(rand() % (high - low + 1) + low);
        output(ticket, names);
        
        cout << "Would you like to add another student? ";
        cin >> input;
    }

    
    
    
    return 0;
}

void output(const vector<int> ticket)
{
    for (int i = 0; i < ticket.size(); i++)
    {
        cout << ticket[i] << " ";
    }
}
void output(const vector<string> names)
{
        for (int i = 0; i < names.size(); i++)
    {
        cout << names[i] << " ";
    }
}
void output(const vector<int> &ticket, const vector<string>& names)
{
     for (int i = 0; i < names.size(); i++)
    {
        cout << names[i] << " " << ticket[i] << endl;
    }
}