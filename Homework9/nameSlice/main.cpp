#include<iostream>
#include <cstdlib>
#include<vector>
#include<fstream>

using namespace std;

/*
 * 
 */
void readFromInput(istream& input);

int main(int argc, char** argv)
{
    ofstream out;
    out.open("test.txt");
    out << "test file 1";
    out.close();
    
    const int size = 100;
    char name[size];
    char file[size];
    cout << "Please enter a word:" << endl;
    cin >> file;
    cout << endl;
    cout << "You entered: " << file << endl;
    cout << "Enter another value";
    readFromInput(cin);
    
    // read from the file created above
    ifstream fin;
    fin.open("test.txt");
    //error check
    if(fin.fail())
    {
        cout << "The reading of the file failed.";
    }
    else
    {
        //read from the file
        //use while loop
        while(!fin.eof())
        {
            readFromInput(fin);
        }
    }
    
    fin.close();
    
    return 0;
}
//Input is like cin
void readFromInput(istream& input)
{
    
    string value;
    input >> value;
    
    cout << "The value is: " << value << endl;
}