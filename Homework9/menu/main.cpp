#include<vector>
#include<iostream>
#include <cstdlib>
#include<fstream>
#include<iomanip>

using namespace std;

/*
 * 
 */
void output(const vector<string> name1, const vector<string> name2);
void selectionSort(vector<int>& v);
void selectionSort(int a[], int size);
void output(const vector<int> ticket);
void output(const vector<string> names);
void output(const vector<int> &ticket, const vector<string>& names);
void bubbleSort(int a[], int size);
void storeRandomNumbers(int a[], int low, int high, int size);
void output(int a[], int size);
int main(int argc, char** argv)
{
    srand(time(0));
    int studentNum; // number of students
    int testNum; // number of tests
    int scores[studentNum][testNum]; //scores for students
    int temp6;// temp for problem 6
    int size = 10; // array size
    int a5[size];// Problem 5 array
    int option;
    string name1, menuInput,input1;    
    vector<int> ticket;
    vector<string> names1;
    int high = 40;
    int low= 1;
    ifstream fin;
    ifstream fin6;
    
    int a4[size];
    string input2;
    vector<string> firstName2;
    vector<string> lastName2;
    vector<string> name2;
    string input7;
    vector<int> v7;
    int number7;
    string firstName;
    string lastName;
    vector<string> fName; // first name vector problem 3
    vector<string> lName; // last name vector problem 3
    ifstream broken;
    
    do
    {

        cout << "Homework 9 Menu" << endl;
        cout << "Problem 1" << endl;
        cout << "Problem 2" << endl;
        cout << "Problem 3" << endl;
        cout << "Problem 4" << endl;
        cout << "Problem 5" << endl;
        cout << "Problem 6" << endl;
        cout << "Problem 7" << endl;
        
        cout << endl << "Please select a problem to review" << endl;
        cin >> option;

        switch(option)
            {


            case 1:

        
        cout << "Would you like to add a name? y/n" ;
        
        cin >> input1;

        while(tolower(input1[0]) == 'y')
        {
            cout << "Please enter a name: ";
            
            cin >> name1;

            names1.push_back(name1);// Adds name1 to the names1 vector
            ticket.push_back(rand() % (high - low + 1) + low);// Adds random number to ticket vector
            output(ticket, names1); // Outputs 

            cout << "Would you like to add another student? "; // asks to repeat loop
            cin >> input1;
        }

                break;

            case 2:
                  
        fin.open("names.dat"); // opens names.dat
      
        // reads in a first and last name each loop
        while (fin >> firstName >> lastName)
        {    
            firstName2.push_back(firstName);//stores the first item read from file
            lastName2.push_back(lastName);//stores the second item read from file
        }
        fin.close();// close names.dat
        output(firstName2, lastName2);
     
                break;

            case 3:
        
        broken.open("broken.dat");// opens broken.dat
        while (broken >> firstName >> lastName) // Reads first and last name each loop
        {

            if(firstName[0] == ',' )// If firstName[0] = , the first name is missing
            {
                firstName[0] = ' ';// Changes the , after the missing first name into a space
            }
            if(lastName[0] == ',')// If lastName[0] = , the last name is missing
                {
                lastName[0] = ' ';// Changes the , before the missing last name into a space
            }

            fName.push_back(firstName);// Adds first name to vector
            lName.push_back(lastName);//Adds last name to the vector
        } 
        output(fName,lName);// outputs first and last names parallel 
        broken.close();
        
                break;

            case 4:
              
        storeRandomNumbers(a4, low, high, size);// Stores  random num between low and high in array a4
        output(a4, size);// Outputs before sorting
        cout << endl;
        bubbleSort(a4, size);// Bubble sorts array a4
        output(a4, size);// outputs the array after sorting

                break;

            case 5:
                
        storeRandomNumbers(a5, low, high, size);// Stores random num between low and high in array a5
        output(a5, size);// Outputs before sort
        cout << endl;
        selectionSort(a5, size); // Uses selection sort
        output(a5, size); // Outputs sorted array

                break;

            case 6:

        fin6.open("tests6.dat");
       // keep looping while end is not reached
        fin6 >> temp6;// Reads in first item in file 
        studentNum = temp6;// Stores first item as studentNum which will be a row/colum
        fin6 >> temp6;// Reads in the second item from file
        testNum = temp6;// Stores the second item as testnum

        while (!fin6.eof())
        {    
            for(int i = 0; i < studentNum; i++)//loops through rows
            {
                for(int j = 0;j < testNum;j++)//loops through colum 
                {
                    fin6 >> temp6; // Reads in a score from file
                    scores[i][j] = temp6; // Stores score in array
                    cout << " " << scores[i][j] << " ";// outputs added score
                }
                cout << endl;
            }
        }

        fin.close();//closes score file

                break;

            case 7:
        do
        {

            cout << "Give a number to add to the vector: " << endl;// asks for number to add
            cin >> number7; // user gives number
            v7.push_back(number7);// number is added to the vector
            selectionSort(v7); // vector is sorted
            cout << "Would you like to add a number to the vector? y/n ";// ask to loop again
            cin >> input7;// user answers y or n to loop again or stop
        }while (tolower(input7[0]) != 'n');

        output(v7);
                
                break;

    
            default:

                cout << "Invalid input. Please enter 1-7" << endl;
                // displays if input is not 1-7
            }
        cout << "Return to main menu? y/n" << endl;
        cin >> menuInput;
    }while (tolower(menuInput[0]) == 'y'); 
    return 0;
}
/**
 * Outputs vector of ints
 * @param ticket
 */
void output(const vector<int> ticket)
{
    for (int i = 0; i < ticket.size(); i++)
    {
        cout << ticket[i] << " ";
    }
}
/**
 * outputs vector of strings
 * @param names
 */
void output(const vector<string> names)
{
        for (int i = 0; i < names.size(); i++)
    {
        cout << names[i] << " ";
    }
}
/**
 * outputs a int vector containing ids along side a string vector
 * of names
 * @param ticket
 * @param names
 */
void output(const vector<int> &ticket, const vector<string>& names)
{
     for (int i = 0; i < names.size(); i++)
    {
        cout << names[i] << " " << ticket[i] << endl;
    }
}
/**
 * Uses bubble sort to compare elements and sort
 * @param a
 * @param size
 */
void bubbleSort(int a[], int size)
{
    // Loop N number of iterations
    for(int i = 0; i < size; i++)
    {
        //cout << "Iteration: " << i << endl;
        // Bubble two values at a time, and swap if needed.
        for(int j = 0; j < size - 1; j++)
        {
            if (a[j] > a[j + 1]) // Verify bounds
            {
                swap(a[j], a[j + 1]);
            }
        }
        //output(a, size);
    }
}
/**
 * Stores size amount of random numbers in the given array 
 * @param a the array
 * @param low lowest possible random number
 * @param high highest possible random number
 * @param size size of the array
 */
void storeRandomNumbers(int a[], int low, int high, int size)
{
    for(int i = 0; i < size; i++)
    {
        a[i] = rand() % (high - low + 1) + low;
    }
}
/**
 * Outputs an array
 * @param a
 * @param size
 */
void output(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}
/**
 * Uses selection sort on a given array & size
 * @param a array given
 * @param size size of the array
 */
void selectionSort(int a[], int size)
{
    // Number of iterations
    for(int i = 0; i < size - 1; i++)
    {
        //cout << "Iteration: " << i + 1 << endl;
        
        //find min
        // minimum is going to be the minimum location
        int min = i;
        
        // Selecting the smallest value in the unsorted region
        for(int j = i; j < size; j++)
        {
            if (a[j] < a[min])
            {
                min = j;
            }
        }
            // swap
            swap(a[i], a[min]);
  
    }
}
/**
 * uses selection sort on a vector
 * @param v
 */
void selectionSort(vector<int>& v)
{
    for(int i = 0; i < v.size() - 1; i++)
    {

        int min = i;
        
        for(int j = i; j < v.size(); j++)
        {
                if (v[j] < v[min])
                {
                    min = j;
                }
        }
            swap(v[i], v[min]);    
    }
}
/**
 * Outputs first and last name vectors side by side
 * @param name1 first name vector
 * @param name2 last name vector
 */
void output(const vector<string> name1, const vector<string> name2)
{
    for(int i = 0; i < name1.size();i++)
    {
        cout << "First name: " << left << setw(5) << name1[i] << "  " << "Last name: " 
                << name2[i] << endl;
    }
}