#include<iostream>
#include <cstdlib>



using namespace std;

/*
 * 
 */
void bubbleSort(int a[], int size);
void storeRandomNumbers(int a[], int low, int high, int size);
void output(int a[], int size);
int main(int argc, char** argv)
{
    srand(time(0));
    int size4 = 10;
    int a4[size4];
    int low = 1;
    int high = 50;
    
    storeRandomNumbers(a4, low, high, size4);
    
    output(a4, size4);
    cout << endl;
    bubbleSort(a4, size4);
    output(a4, size4);
    
    
    
    return 0;
}

void bubbleSort(int a[], int size)
{
    // Loop N number of iterations
    for(int i = 0; i < size; i++)
    {
        //cout << "Iteration: " << i << endl;
        // Bubble two values at a time, and swap if needed.
        for(int j = 0; j < size - 1; j++)
        {
            if (a[j] > a[j + 1]) // Verify bounds
            {
                swap(a[j], a[j + 1]);
            }
        }
        //output(a, size);
    }
}

void storeRandomNumbers(int a[], int low, int high, int size)
{
    for(int i = 0; i < size; i++)
    {
        a[i] = rand() % (high - low + 1) + low;
    }
}
void output(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}