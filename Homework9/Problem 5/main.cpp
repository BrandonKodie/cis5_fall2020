#include<iostream>
#include <cstdlib>
#include <time.h>
using namespace std;

/*
 * 
 */
void storeRandomNumbers(int a[], int low, int high, int size);
void output(int a[], int size);
void selectionSort(int a[], int size);
int main(int argc, char** argv)
{
    srand(time(0));
    int size5 = 10;
    int a5[size5];
    int low = 1;
    int high = 55;
    storeRandomNumbers(a5, low, high, size5);
    output(a5, size5);
    cout << endl;
    selectionSort(a5, size5);
    output(a5, size5);
    
    

    return 0;
}

void storeRandomNumbers(int a[], int low, int high, int size)
{
    for(int i = 0; i < size; i++)
    {
        a[i] = rand() % (high - low + 1) + low;
    }
}
void output(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}
void selectionSort(int a[], int size)
{
    // Number of iterations
    for(int i = 0; i < size - 1; i++)
    {
        //cout << "Iteration: " << i + 1 << endl;
        
        //find min
        // minimum is going to be the minimum location
        int min = i;
        
        // Selecting the smallest value in the unsorted region
        for(int j = i; j < size; j++)
        {
            if (a[j] < a[min])
            {
                min = j;
            }
        }
            // swap
            swap(a[i], a[min]);
  
    }
}