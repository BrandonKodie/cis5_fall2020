#include<vector>
#include <cstdlib>
#include<iostream>

using namespace std;

void output(const vector<int> &v);
void selectionSort(vector<int>& v);


int main(int argc, char** argv)
{
    string input7;
    vector<int> v7;
    int number7;
    
    do
    {

        cout << "Give a number to add to the vector: " << endl;
        cin >> number7;
        v7.push_back(number7);
        selectionSort(v7);
        cout << "Would you like to add a number to the vector? ";
        cin >> input7;
    }while (tolower(input7[0]) != 'n');
    
    output(v7);
    

    return 0;
}
void output(const vector<int> &v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}
/**
 * Sorts the vector using selection sort, in ascending order
 * @param v
 */
void selectionSort(vector<int>& v)
{
    for(int i = 0; i < v.size() - 1; i++)
    {

        int min = i;
        
        for(int j = i; j < v.size(); j++)
        {
                if (v[j] < v[min])
                {
                    min = j;
                }
        }
            swap(v[i], v[min]);    
    }
}
