
#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 * 
 */
int main(int argc, char** argv)
{
//Loops
// 3 kinds of loops
// while loop, do-while, for loop
    
    // FOR LOOP
    // syntax:
    // for (iterator; conditional expression; iterator change)
    // ex. iterator -> int i =1
    // ex. condition -> i < 10
    // ex. change -> i < 10 (loop continues while condition is true)
    // ex. change -> i++ 
    // for ( int i =1; i < 10; i++)
    
    // i++ -> Increments value by 1
    // Step 1 happens only once
    // After step 4, go to step 2
    
    //     (1)     (2)    (4)
    for (int i=1; i < 10; i++)
    {
        // (3)
        cout << i << endl;
    }
    // <- Go to the iterator change
    
    // This is how a typical for loop is
    for (int i = 0; i < 10; i++)
    {
        cout << i << endl;
    }
    
    // Output number 1 to 1000
    /*
    for (int i = 0; i < 1000; i++)
    {
        cout << i + 1 << endl;
    }*/
     //While loop
    // syntax: 
    // While (condition)
    // {
    // code
    // }
    
    cout <<" Outputting a while loop" << endl;
    int i = 10; // Still need a iterator for a while loop
    while (i < 10 )
    {
        i++;
        cout << i << endl;
        
    }
    
    // Do-While Loop
    // syntax: do
    // {
    // code
    //}
    // while (conditional expression);
    // Do while loops guarantee a single execution
    
    cout << "Outputting the Do-While Loop" << endl;
    i = 10;
    do 
    { 
        cout <<i << endl;
        i++;
    }
    while (i < 10);
    
    // When to use loops
    // for loop: used for counting/ when we know the number of loop
    // while loop: use for unknown # of loops (user input)
    // do-while loop: use for user input (unknown # of loops) ex. games
    
    
    return 0;
}

