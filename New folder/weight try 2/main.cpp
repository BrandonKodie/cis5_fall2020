/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: brand
 *
 * Created on October 7, 2020, 11:37 AM
 */
#include <iostream>
#include <cstdlib>

using namespace std;
const double  poundPerKilo = 2.2046;
const double gramsPerKilo = 0.001;
const double ouncesPerPound = 0.0666;
const double gramsPerPound = 453.5970;
const double gramsPerOunce = 30.2398;
    

int convert(int pound, double ounce)
{
    
    int grams;
    double kilogram; 
    int kilo;
    double remainder;
    
    kilogram = pound / poundPerKilo;
    kilo = kilogram / 1;
    
    remainder = (kilogram - kilo) * 1000;
    
    grams = (ounce * gramsPerOunce) + remainder;
           while( grams > 1000)
    {
        kilo = kilo + 1;
        grams = grams - 1000;
    }
    
    return (grams);
}
int convertkilo(int pound, double ounce)
{
    
    int grams;
    double kilogram; 
    int kilo;
    double remainder;
    
    kilogram = pound / poundPerKilo;
    kilo = kilogram / 1;
    
    remainder = (kilogram - kilo) * 1000;
    
    grams = (ounce * gramsPerOunce) + remainder;
        while( grams > 1000)
    {
        kilo = kilo + 1;
        grams = grams - 1000;
    }
   
    return (kilo);
}

int weightInput ()
{
    cout << "Please enter the number " << endl;
    int weight{};
    cin >> weight;
    return weight;
}

int output(int gram,int kilo)
{
    cout << "Kilo: " << kilo << endl;
    cout << "Grams: " << gram << endl;
}

int main(int argc, char** argv)
{
    string menuInput;
    do
    {
        
    
    cout << "Enter Pounds " << endl;
 
    int pound{ weightInput() }; 
    cout << " Enter Ounces " << endl;
    double ounce{ weightInput()};
    
    int gram{convert(pound, ounce)};
    int kilo{convertkilo(pound, ounce)};
    
    output(gram, kilo);
    
    

    cout<< "Would you like to input again? y/n" << endl;
    cin >> menuInput;
 
    }while(tolower(menuInput[0]) == 'y');
    
    return 0;
}

double convert (int weight)
{
    double kilo;
    double b = 0.453592;
    kilo = weight * b;
    return (kilo);
}