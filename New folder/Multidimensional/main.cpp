

/* 
 * File:   main.cpp
 * Author: brand
 * 
 * multidimensional arrays
 * 
 * Created on October 7, 2020, 10:06 PM
 */
#include<iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    int array[2][3] = {{2,3,4}, {8,9,10}};
    
    // 2,3,4
    //8,9,10
        
    cout << array[0][2] << endl;
    cout << array[1][0] << endl;
    
    int bigun[2][3] = {{1,2,3},{5,6,8}};
    
    for(int row = 0;row < 2;row++){
        for(int column = 0; column < 3; column++){
            cout << bigun[row][column] << " ";
        }
        cout << endl;
      
    }
    
    
    return 0;
}

