#include<iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
void add5(int &num)
{
    num += 5;
}
void swapDoubles(double &val1, double &val2)
{
    double temp = val1;
    val1 = val2;
    val2 = temp;
    
}
int recursiveOutput(int val)
{
    cout << val << endl;
    if(val < 1)
    {
        return 0;
    }
    recursiveOutput(val - 1);
    return val;
}
int main(int argc, char** argv)
{
    int num = 10;
    
    cout << " Before: " << num << endl;
    add5(num);
    cout << "After: " << num << endl;
    recursiveOutput(num);
    double x = 5.5;
    double y = 2.3;
    
    // strings
    string input;
    cout << "Enter a name";
    cin >> input;
    
    for(int i = 0; i < input.size(); i++)
    {
        cout << input[i] << " ";
    }
    
    return 0;
}

/*
 Recursion 
 * - A function that calls itself
 * 
 * Functions
 * - Reference Parameters
 * 
 * know how to write a function
 * know how to use a function
 
 know file io basics
 * 1 open file. 2) MAnipulate the file 3) close
 * 
 * know variable rules
 * 
 * know math division
 * 
 */