#include<iostream>
#include <cstdlib>
#include<vector>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    
    // Vector
    // A vector is a container
    // A vector is a set of items
    // A vector is a new datatype
    // Need to specify what kind of container the vector is
    
    //SYNTAX:
    // vector<int> v;
    vector<int> v;
    cout << "Vector push_back 20, 10, 100" << endl;
    //Inserting a value into the vector
    // Use the member function push_back(val)
    // Insert a single value at the end of the vector
    //---------------
    //begin      end
    //[] [] [] [] [] [] [] [] 
    // Insert a few values
    v.push_back(20);
    v.push_back(10);
    v.push_back(100);
    
    //output the value contained within the vector
    // Access elements of a vector just like accessing characters of a string
    // Use the subscript operator
    // e.x. string s = "Hello; s[2] == '1'
    cout << v[0] << endl;
    cout << v[2] << endl;
    
    // Use a forloop, and output ALL the contents of a vector
    // Use the .size() member function
    cout << "Output using the for loop! " << endl;
    for (int i = 0; i < v.size(); i++)
    {
        cout << v[i] << endl;
    }
    // Remove a value from a vector
    // Use .pop_back() to remove a single value from a vector
    // Removes te last value
    v.pop_back();
    cout << "After pop" << endl;
    for (int i = 0; i < v.size(); i++)
    {
        cout << v[i] << endl;
    }
    

    return 0;
}

