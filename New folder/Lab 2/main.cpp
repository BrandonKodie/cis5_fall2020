/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: brand
 *
 * Created on September 5, 2020, 8:34 AM
 */

#include <iostream> // Libraries

using namespace std; // Source of the library

/*
 * 
 */
int main(int argc, char** argv)
{
    
 //  All source code goes here for now
    
    // Variables
    //syntax: data_type name;
    int num1; //int is the data type => integer
    int num2;
    
    // = is called the assignment operator.
    num2 = 3.5; // .5 is truncated. Only the 3 is stored.
    
    
     
     // ask the user for input
    cout << "please enter two numbers:" << endl;
    cin >> num1 >> num2;        
    
    //Make sure use 2. to use double division
    // int division will turncate
    // 3 /2 == 1
    
    double avg = (num1 + num2) / 2.0;
    
    cout << "The average is: " << avg;
    
    /* 
     Get two numbers from the user
     output the number to the user
     swap the two numbers
     output the numbers again to the user
     */
    cout <<  "please enter two numbers: ";
    
            cin >> num1 >> num2;
            
            cout << "Num1: " << num1 << " Num2: " << num2 << endl;
            
            // swap
            int temp = num1; // create temporary to store value
            num1 = num2; // replace value
            num2 = temp; //use temp to replace other value
            
            cout << "Num1: " << num1 << " Num2: " << num2 << endl;
            
    return 0;
}

