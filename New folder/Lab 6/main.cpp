/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: brand
 *
 * Created on October 3, 2020, 7:36 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;
// ALL CAPS for constants. Use underscores for
const double PI = 3.14;
// Modularity
// return_data_type name( parameters)
// {
// code
// }
int square(int value)// function header
{                         //   
    return value * value; // function definition and declaration
}
/**
 * Calculates whether or not an int is even
 * @param int value provided by something
 * @return returns true if the value is even
 */
bool isEven(int value)
{
    return  value % 2 == 0; 
}
/**
 * Calculate the area of a circle 
 * @param radius - radius of circle
 * @return  the area of a circle
 */
double calculateArea(double radius)
{
   
    return radius * radius * PI;
}

void problem1(int input)
{
      if (isEven(input))
    {
        cout << "Number is even" << endl;
    }
    else
    {
        cout << " Number is odd! " << endl;
    }  
}
void problem2(int input)
{
    
    // invoke (call) the square function
    //input is passed as the argument
    cout << " the square of the number is : "
            << square(input) << endl;
    
    cout << " Please enter another number" ;
    cin >> input;
    
    cout << " the square of the second number is : "
            << square(input) << endl;         
}

int main(int argc, char** argv)
{
   
    
    int input;
    cout << "Please enter a number " ;
    cin >> input;
    // invoke the function 
    //nothing goes before the function call,
    // because nothing is returned
    problem1(input);
 
    cout << "Please enter a radius: ";
    double radius;
    cin >> radius;
    
    cout << "The area is: " << calculateArea(radius) << endl;
    
    return 0;
}

