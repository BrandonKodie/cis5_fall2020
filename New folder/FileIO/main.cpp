/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: brand
 *
 * Created on September 18, 2020, 4:46 PM
 */
#include <iostream>
#include <cstdlib>
#include <fstream> // allows reading/writing to files

using namespace std;
int input;
/*
 * 
 */
int main(int argc, char** argv)
{
// 3 MAIN PROCESSES
// 1) Open the file
// 2) Manipulate/access the file
// 3) close the file   (needed to release locks)
    
    // Outputting a file(writing to a file)
    
    // step 1: Opening the file
    //ofstream allows the writing to a file
    ofstream fout;
    ifstream fin;
    //use .open(fileName) to open a file
   fout.open("test.txt"); //always creates new file
   // Step 2: MAnipulate file
   // Use same operators as cout does e.x <<
   fout << "I am writing to a file" << endl;
    
    // Step 3: Close the file
    fout.close();
    //Example
    //write numbers 1-10 in the file
    // Step 1: Open
    fout.open("numbers.txt");
    
    // step 2: Output
    for(int i = 0; i < 10; i++)
    {
        fout << i << " ";
        
    }
    fout << endl;
    
    //step 3: Close
    fout.close();
    //xxxxxxxxxxxxxxxxxxxx
    
    fin.open("data.dat");
    string input;
    //step 2: read
    
    while (fin >> input)
    {
        cout << input << endl;
    }
        
    
            fin.close();
    
    
    fin.open("data.dat");
   // keep looping while end is not reached
    fin >> input;
    while (!fin.eof())
    {
        
        cout << input << " ";
        fin >> input;
    }
    
            fin.close();
            
            // error on input
            // Get a file name from the user
            // Attempt to reed
            
            cout << endl << "Please enter a file" << endl;
            string filename;
            cin >> filename;
            // Attempt open the file
            fin.open(filename);
            int input1, input2, input3, gas;
            if (fin.fail())
            {
                cout << "The file does not exist!" << endl;
            }
            else 
            {
                cout << "The file exists!"  << endl;
                fin >> input1;
                fin >> input2;
                fin >> input3;
                cout << input2 + input3 << endl;
            }
            // close file <<
            fin.close();
            
            
    
    
    
    
    return 0;
    
    
    
}

