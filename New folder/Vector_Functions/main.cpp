#include <vector>
#include <cstdlib>
#include<iostream>

using namespace std;
// function prototypes
void output(const vector<int> &v);
void add5(vector<int> &v);

// Create a function that returns a vector with random values
vector<int> createRandomVector(int size);
void storeRandomValues(vector<int> &v);

int main(int argc, char** argv)
{
    srand(time(0));
    
    // Create a vector with 3 random values 
    // Use the second way to declare a vector
    vector<int> v; // Creats an empty vector
    //vector<int> v2(3); //Creates a vector with the size of three (all zeros)
    
    //Use the function
    vector<int> v2 = createRandomVector(100);
    
   // v2[0] = 10;
    //v2[1] = 3;     // instead of push_back
   // v2[2] = 19;
    cout << "Printing out vector before altering" << endl;
    output(v2);
    
    
    // Increment every value in v2 by 5
    // Use a for loop to iterate through vector
    add5(v2);
     cout << endl << "Printing out vector after altering" << endl;
     output(v2);
    
    // Change the values to another set
     storeRandomValues(v2);
     
     cout << "New random values: " << endl;
     output(v2);
    

    return 0;
}
void output(const vector<int> &v)
{
          for (int i = 0; i < v.size(); i++)
    {
          cout << v[i] << " " << endl;
    }
          cout << endl;
}
/**
 * Adds 5 to every value in the vector
 * @param v - A vector of ints
 */
void add5(vector<int> &v)
{
        for (int i = 0; i < v.size() ; i++)
    {
       // int temp = v[i];
        //temp += 5;
       // v[i] = temp;
        v[i] += 5;
    }
}
/**
 * Creates and returns a random vector
 * with random values between 1-100
 * @param size
 * @return a vector with random values
 */
vector<int> createRandomVector(int size)
{
    vector<int> temp; //Empty vector
    // Generate a random value
    int high = 100;
    int low = 20;
   
    
    for (int i = 0; i < size; i++)
    { 
        int randValue = rand() % (high - low + 1) + low;
        temp.push_back(randValue);
    }
    return temp;
}
/**
 * Stores random values in a given vector
 * @param v
 */
void storeRandomValues(vector<int> &v)
{
    int high = 100;
    int low = 20;
    for (int i = 0; i < v.size(); i ++)
    { 
        int randValue = rand() % (high - low + 1) + low;
        v[i] = randValue;
        
    }
    
}