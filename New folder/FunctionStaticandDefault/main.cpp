#include<iostream>

#include <cstdlib>

using namespace std;

void swapValues(int& num1, int& num2);
void swapValues(double& num1,double& num2);
/*
 * An overloaded function has the same name, but
 * different parameters 
 */

// void function that outputs
// give a default to val
void outputValue(int val = 0);

// We can have as many defaults as we want
// Defaults start and the end of the header
// can not have a gap
void outputValues(int val1 = 1, int val2 = 2,
        int val3 = 3);

// Static values
// static values exist for the length of the program (in function)
void counter();
int main(int argc, char** argv)
{
    int val1 = 5, val2 = 10;

    // Output values
    cout << "Before: " << val1 << " " << val2 << endl;
    swapValues(val1, val2);
    
    cout << "After: " << val1 << " " << val2 << endl;
    
    // swap double
    double d1= 3.14, d2 = 1.29;
    cout << "Before: " << d1 << " " << d2 << endl;
    swapValues(d1, d2);
    cout << "After: " << d1 << " " << d2 << endl;
    
    outputValue();
    
    // Run the counter function 10 times
    for(int i = 0; i < 10; i++)
    {
        counter();
    }
    return 0;
}
void swapValues(int& num1, int& num2)
{
    int temp = num1;
    num1 = num2;
    num2 = temp;
}
void swapValues(double& num1,double& num2)
{
    double temp = num1;
    num1 = num2;
    num2 = temp;
}
void outputValue(int val){
    cout << "This is the value: " << val << endl;
}
void outputValues(int val1, int val2,
        int val3){
    
}
void counter()
{
    static int num = 0;
    cout << "number of times ran: " << num << endl;
    num++;
}