/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: brand
 *
 * Created on October 6, 2020, 10:35 PM
 */
#include <iostream>
#include <cstdlib>

using namespace std;

// Function prototypes
double getCircumference(double radius);
double getCircleAreaFromRadius( double radius);

//Global Constants        Good
const double PI = 3.14; //ALL CAPS

// avoid non-global variable   Bad
// double pi2 = 3.14;

int main(int argc, char** argv)
{
    
    //Get a radius from the user
    // output a circumference of a circle
    cout << " Please enter any value " << endl;
    double radius;
    cin >> radius;
    
    double circumference = getCircumference(radius);
    
    cout << "circumference of the circle is: " << circumference;
    //double area = pi * radius * radius;
    cout << "the area of the circle is: " << getCircleAreaFromRadius(radius);
    return 0;
}

// Function Definitions
/**
 * 
 * @param radius
 * @return returns the circumference
 */
double getCircumference(double radius)
{
    double circum = 3.14 * 2 * radius;
    return circum;
}
/**
 * Gets the area of a circle from a radius
 * @param radius any value for a radius of a circle
 * @return  returns the area as a double
 */
double getCircleAreaFromRadius(double radius)
{
    return PI * radius * radius;
}
