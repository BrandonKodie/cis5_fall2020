/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: brand
 *
 * Created on October 8, 2020, 5:01 PM
 */
#include<iostream>
#include <cstdlib>

using namespace std;
// 2 Rules of recursion
// 1) Base case (stop the recursion)
//2 ) recursive call


// Fibonacci Sequence
// 1 1 result of the previous 2. 1 1 2 3 5 8 13 21
// f(1) = 1 f (2) = 1, 
// f(n) = f(n - 1) + f(n - 2)
// f(5) = f(4) + f(3)
//      = f(3) + f(2) + f(2) +f(1)
//      = f(2) + f(1) + f(2) + f(2) + f(1)
//      = 1 + 1 + 1 + 1 +1
//      = 5


void output();
// Fibonacci function
// 1) f(1) or f(2) == 1
// 2) f(n) = f(n - 1) + f(n - 2)
int fibonacci(int val);
// function that outputs a string with spaces between every character
void output(string s, int location)
{
    // 1 ) Base case
    // 2) Recursive call
    if (location == s.size() - 1)
    {
        cout << s[location];
        return;
    }
    else
    {
        cout << s[location] << " ";
        output(s, location+ 1);
     // reverse   cout << s[location] << " ";
    }
    
}


int main(int argc, char** argv)
{
    // Ge<t a number from a user
   // cout << "Please enter a number: ";
   // int num;
    //cin >> num;
   // int fib = fibonacci(num);
   // cout << "The fibonacci of " << num << " is " << fib << endl;
    
    cout << "Please enter a word ";
    string word;
    cin >> word;
    output(word, 0);
    
    return 0;
}

int fibonacci(int val)
{
    // 1 Base case
    // 2) Recursive call
    cout << "f" << val << ") ";
    if (val == 1)
    {
        return 1;
    }
    else if (val == 2)
    {
        return 1;
    }
    else 
    {
        // Recursive call
        return fibonacci(val - 1)+ fibonacci(val -2);
    }
}