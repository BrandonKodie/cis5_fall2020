#include <iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    // wrap the entire menu in a do while
    
    string menuinput;
    do
        {

        // ? Create a menu based program
        cout << "PLease enter one of these options!" << endl;
        cout << "1) the for loop" << endl;
        cout << "2) While loop" << endl;
        cout << "3) Do-while loop" << endl;
        cout << "4) The guessing game" <<endl;

        int option;
        cin >> option;
        // All user-input variables
        string dowhileinput;
        string userinput;
        int low, high, randNum;

        switch(option)
        {
            case 1:     // Sentinel controlled loop
        cout << "How many times do you want to run the loop?";
        int input;
        cin >> input;

        for (int i = 0; i < input; i++)
        {
            cout << "Executing loop number: " << i + 1 << endl;
        }
        break;
            case 2:
                //user-controlled loop
            cout << "Would you like to run the code? yes/no ";

            cin >> userinput;

        // ways to enter yes
        // y, Y, yes, YES
        // only loop at first character
        //Make the first character lower case -> tolower(char)
        while (tolower(userinput[0]) == 'y')
        {
            cout << "Awesome! Would you like to enter the loop again? yes/no: " << endl;
            cin >> userinput;
        }
        break;
     case 3:
         // Do the same thing, but with a do-while loop

                    do {
                    cout << "Would you like to execute the loop again? yes/no";
                    cin >> dowhileinput;
                }
                while (tolower(dowhileinput[0]) == 'y');
    break;
        case 4:
            // Create a guessing game
        // Generate random number, and have the user guess the number
        // Need a range of numbers
        // Need some feedback on the number itself (too large or too small)
        // SEED A RANDOM NUMBER
        srand(time(0)); // Seed the time from jan 1, 1970
        randNum = rand(); // rand() is part of cstdlib

        // Make the number within a specific range
        // Number between 0-50
        //randNum = randNum % 51; // <- Gets a number between 0-50

        // What if I wanted a random number between a low and high?
        // 100-200
        low = 100;
        high = 300;

        // 300 - 100  - 200
        // num % 200 = 0-199
        // 0-199 + 100 =100-299
        //100-299 + 1 = 101-300
        randNum = (randNum % (high - low  + 1)) + low;

        // ex 1-1000
        // num % (1000 - 1 +1) = num % 1000 = 0 - 999
        // 0-999 +1 - 1-1000
        int num;
        do
        {
            cout << "Please enter a number between "
            << low << "-" << high << ": ";

            cin >> num;

            if (num > randNum)
            {
                cout << "The number is too large!" << endl;
            } else if (num < randNum)
            {
                cout << "The number is too small!" << endl;
            } else
            {
                cout << "You have guessed the number!" << endl;
            }
        }while (num != randNum);
        break;
    default:
            cout << "Not a valid input!";


                }

// END OF THE MENU
        cout << "Would you like to run again? Y/N";
        cin >> menuinput;
    } while (tolower(menuinput[0]) == 'y');
    return 0;
}

