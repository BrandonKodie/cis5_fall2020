
/* 
 * File:   main.cpp
 * Author: brand
 *
 * Created on October 8, 2020, 1:32 PM
 */

#include <cstdlib>
#include<iostream>
using namespace std;
// proto
int square(int& num);
void swapValues( int& val1, int& val2);


int main(int argc, char** argv)
{

    int num = 20;
    int val = square(num);
    cout <<"val = " << val << endl;
    
    cout << "Please enter 2 values: ";
    int num1, num2;
    cin >> num1 >> num2;
    //swap
    cout << "Before: Num1: " << num1 << "Num2: " << num2 << endl;
    // Create temp values:
    int temp = num1;
    num1 = num2;
    num2 = temp;
    
    cout << "After: Num1: " << num1 << " Num2: " << num2 << endl;
    
    cout << endl << "Using function instead" << endl;
    // use the function
        cout << "Before: Num1: " << num1 << "Num2: " << num2 << endl;

        swapValues(num1, num2);
    
    cout << "After: Num1: " << num1 << " Num2: " << num2 << endl;
    return 0;
}

int square(int& num){
    num = 6;
    return num * num;
}
/**
 * 
 * @param val1 an int first value
 * @param val2 an int second value
 */
void swapValues( int& val1, int& val2)
{
    int temp = val1;
    val1 = val2;
    val2 = temp;
    
}