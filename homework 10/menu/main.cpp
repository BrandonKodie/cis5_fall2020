#include<vector>
#include <cstdlib>
#include<iostream>
using namespace std;

/*
 * 
 */
int binarySearch(const vector<int>& v, int val);
void output(const vector<int> &v);
void selectionSort(vector<int>& v);
void output(int[], int size);
void storeRandomValues(int a[], int size);
int* createArray(int size);
void addToSize(int* &a, int &size, int num);
int main(int argc, char** argv) {

    
    srand(time(0));
    vector <int> bigVector;
    int num, location;
    int size = 100000;
    string menuInput;
    int variable1 = 42;
    int variable2 = variable1;
    int size2;
    int *pnt1;
    int *pnt2;
    pnt1 = &variable1;
    pnt2 = &variable1;
    int option;
    int size3;
    int size4 = 7;
    int staticArray[size4]; 
    int size5 = 5;
    int num5;
    int* a = new int[size5];
    a[size5];
    
    int count = 0;
    
    do
    {
        cout << "Homework 10 Menu" << endl;
        cout << "Problem 1" << endl;
        cout << "Problem 2" << endl;
        cout << "Problem 3" << endl;
        cout << "Problem 4" << endl;
        cout << "Problem 5" << endl;
        cout << "Problem 6" << endl;
        
        
        cout << endl << "Please select a problem to review" << endl;
        cin >> option;
        
        switch(option)
            {


            case 1:
                
        cout << "variable 1 = " <<  variable1 << endl << "variable 2 = "
                << variable2 << endl;
        variable1 = 12;
        cout << "variable 1 = " << variable1 << endl << "variable 2 = "
                << variable2 << endl;
        *pnt1 = 25; 
        cout << "variable 1 = " << variable1 << endl << "variable 2 = "
                << variable2 << endl;
        *pnt2 = 60;
        cout << "variable 1 = " << variable1 << endl << "variable 2 = "
                << variable2 << endl;
      
                break;

            case 2:
                    
            {cout << "PLease enter the size for the array: " << endl;
    cin >> size2;
    
    int* da = new int[size2];
    storeRandomValues(da, size2);
    output(da, size2);
    delete[] da;}
    
                break;

            case 3:
            {
    cout << "PLease enter the size for the array: " << endl;
    cin >> size3;
    
    int* dynamicArray = createArray(size3);
    storeRandomValues(dynamicArray, size3);
    output(dynamicArray, size3);
    delete[] dynamicArray;}

                break;

            case 4:
            {
    storeRandomValues(staticArray, size4);
    int *pnt4 = staticArray;

    cout << "Printing static Array" << endl;
    for(int i : staticArray)
    cout << i << " ";
        
    cout << endl;
    cout << "Printing point targeting static Array" << endl;
    for(int i = 0; i < size4; i++)
        cout << pnt4[i] << " "; }
                break;

            case 5:
            {
                    do {
        
        cout << "Enter a number or -999 to end:" << endl;
        cin >> num5;  
        if(count < size5)
        {
            a[count] = num5;
         count++;   
        }
        else
        {
            addToSize(a, size5, num5 );
            count++;
        }
        
    } while(num5 != -999);
    
        output(a,size5 - 1);
        cout << "size: " << size5;
           delete[] a;}
                break;

            case 6:
                
for(int i = 0; i  < size; i++)
    {
        int x = rand() % 100;
        bigVector.push_back(x);
    }
    selectionSort(bigVector);

        cout << "Enter a number to find: ";
    cin >> num;
    
    
    location = binarySearch(bigVector, num);
    
    if (location == -1)
    {
        cout << "That number was not found!" << endl;
    }
    else
    {
        cout << "The number is located at:" << location << endl;
    }
       
                break;

            default:

                cout << "Invalid input. Please enter 1-6" << endl;
                // displays if input is not 1-6
            }
        cout << "Return to main menu? y/n" << endl;
        cin >> menuInput;
    }while (tolower(menuInput[0]) == 'y'); 
    
    
    
    return 0;
}

void selectionSort(vector<int>& v)
{

    for(int i = 0; i < v.size() - 1; i++)
    {
        int min = i;
        for(int j = i; j < v.size(); j++)
        {
                if (v[j] < v[min])
                {
                    min = j;
                }
        }
            swap(v[i], v[min]);
            
    }
}
void output(const vector<int> &v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << endl;
    }
    cout << endl;
}
int binarySearch(const vector<int>& v, int val)
{
    if(v.size() == 0)
    {
        return -1;
    }
    int low = 0;
    int high = v.size() - 1;
    int middle = (low + high) / 2;
    int guess = v[middle];
    
    // Keep splitting the search space in half
    // Search only what you need to 
    while(guess != val && low <= high) 
    {
       // cout << "low: " << low << endl;
       // cout << "high: " << high << endl;
      //  cout << "middle: " << middle << endl << endl;;
        // Get the new middle
            if (guess < val) // guessed to low
            { 
               // cout << " too low" << endl;
                // Go search to the right of it
                low = middle + 1;
                middle = (low + high) / 2;
                guess = v[middle];
            }
            else// guess is too high
            {
               // cout << "too high" << endl;
                // move high val down
                high = middle - 1;
                middle = (low + high) / 2;
                guess = v[middle];
        }
     
    }
        if(low > high)
        {
            return -1;
        }
        else
        {
        return middle;
    }
}
void output(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}
void storeRandomValues(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        a[i] = rand() % 11; // 0 - 10
    } 
}
int* createArray(int size)
{
    int* dynamicArray = new int[size];
    
    return dynamicArray;
}
void addToSize(int* &array, int &size, int num)
{
    
     int *array2 = new int [size + 1];
   
     for(int i = 0; i < size;i++)
     {
         array2[i] = array[i];
     }
     
    array2[size] = num;
    delete[] array;
    array = array2;
    size++;

}