#include<iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
void storeRandomValues(int a[], int size);
int main(int argc, char** argv) 
{
    int size4 = 7;
    int staticArray[size4];
    
    storeRandomValues(staticArray, size4);
    int *pnt = staticArray;

    cout << "Printing static Array" << endl;
    for(int i : staticArray)
        cout << i << " ";
    
    cout << endl;
    cout << "Printing point targeting static Array" << endl;
    for(int i = 0; i < size4; i++)
        cout << pnt[i] << " ";
    
    return 0;
}

void storeRandomValues(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        a[i] = rand() % 11; // 0 - 10
    } 
}