#include<iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
void output(int[], int size);
void storeRandomValues(int a[], int size);
int* createArray(int size);
int main(int argc, char** argv) {

    int size3;
    cout << "PLease enter the size for the array: " << endl;
    cin >> size3;
    
    int* dynamicArray = createArray(size3);
    storeRandomValues(dynamicArray, size3);
    output(dynamicArray, size3);
    delete[] dynamicArray;
    
    
    return 0;
}

void output(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}
void storeRandomValues(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        a[i] = rand() % 11; // 0 - 10
    } 
}
int* createArray(int size)
{
    int* dynamicArray = new int[size];
    
    return dynamicArray;
}