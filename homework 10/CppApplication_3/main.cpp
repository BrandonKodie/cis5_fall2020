#include<iostream>
#include <cstdlib>
#include<vector>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int variable1 = 42;
    int variable2 = variable1;
    
    int *pnt1;
    int *pnt2;
    pnt1 = &variable1;
    pnt2 = &variable1;
    
    
    cout << "variable 1 = " <<  variable1 << endl << "variable 2 = "
            << variable2 << endl;
    variable1 = 12;
    cout << "variable 1 = " << variable1 << endl << "variable 2 = "
            << variable2 << endl;
     *pnt1 = 25; 
     cout << "variable 1 = " << variable1 << endl << "variable 2 = "
            << variable2 << endl;
     *pnt2 = 60;
      cout << "variable 1 = " << variable1 << endl << "variable 2 = "
            << variable2 << endl;
    return 0;
}

