#include<iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
void output(int[], int size);
void storeRandomValues(int a[], int size);
void add5(int* p, int size);
int* createRandomDynamicArray(int size);
int main(int argc, char** argv)
{
    srand(time(0));
//Static array 
    //Static - Not changing address or size and located on stack
    int size = 3;
    int staticArray[size];
    storeRandomValues(staticArray, size);
    cout << "Showing static array" << endl;
    output(staticArray, size);
    
    cout << "Showing static array" << endl;
    
    //Dynamic Array
    // Use new keyword
    int* dynamicArray = createRandomDynamicArray(size);
     output(dynamicArray, size);
     
     // use 
     add5(staticArray, size);
     add5(dynamicArray,size);
     
    output(staticArray, size);
    output(dynamicArray, size);
     // Static and dynamic arrays are both pointers
     // Static array variable points to the start of the array 
     // Example
     int a[10];
     int* p; // Creating a pointer
     //a = p; // Assigning the pointer address to array address -- ILLEGAL
     p = a; // Assigning array address to pointer address
    delete[] dynamicArray;
    return 0;
}

void output(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}
void storeRandomValues(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        a[i] = rand() % 11; // 0 - 10
    } 
}
void add5(int* p, int size)
{
    for(int i = 0; i < size; i++)
    {
        p[i] += 5;
    }
}
int* createRandomDynamicArray(int size)
{
    int* array = new int[size];
    storeRandomValues(array, size);
    
    return array;
}