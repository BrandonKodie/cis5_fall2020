#include<iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
void output(int[], int size);
void storeRandomValues(int a[], int size);
int main(int argc, char** argv) {

    int size;
    cout << "PLease enter the size for the array: " << endl;
    cin >> size;
    
    int* da = new int[size];
    storeRandomValues(da, size);
    output(da, size);
    delete[] da;
    
    
    return 0;
}

void output(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}
void storeRandomValues(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        a[i] = rand() % 11; // 0 - 10
    } 
}