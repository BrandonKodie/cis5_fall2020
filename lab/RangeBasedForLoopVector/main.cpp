#include<vector>
#include<iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
void vectorOutput(vector<int> v);
void vectorRandom(vector<int> &v);
const int SIZE = 5;
int main(int argc, char** argv)
{
    srand(time(0));
    vector<int> v(SIZE);
    for(int i = 0;i < v.size(); i++)
        v[i] = i + 1;
    vectorOutput(v);
    vectorRandom(v);
    vectorOutput(v);
    return 0;
}

void vectorOutput(vector<int> v)
{
    for(int x : v)
    {
        cout << x << endl;
    }         
}
void vectorRandom(vector<int> &v)
{
    int high = 20;
    int low = 1;

    for (int x : v)
    {   
        int randValue = rand() % (high - low + 1) + low;
        v[x - 1] = randValue;
    }
}
