#include<vector>
#include <cstdlib>
#include<iostream>

using namespace std;

void output(const vector<int> &v);
void output(int a[], int size);
void storeValuesFromUser(vector<int> &v);
void storeValuesFromUser(int a[], int size);
int getNumberOfOdds(const vector<int> &v);
int getNumberOfOdds(int a[], int size);
int main(int argc, char** argv)
{
    
    
    vector<int> values(5);
    
    // array size
    int size = 5;
    int transactions[size];
    output(values);
    
    //output contents of vector

    // output contents of array
    for(int i = 0; i < size; i++)
    {
        cout << transactions[i] << " ";
    }
    cout << endl;
    //function
    output(transactions, size * 2); // bounds error ..over array size
    
    //store user given values in vector
storeValuesFromUser(values);


output(values);
cout << "There are " << getNumberOfOdds(values) << " odd numbers in vector";
storeValuesFromUser(transactions, size);
cout << "There are " << getNumberOfOdds(transactions, size) << " odd numbers in array";

cout << "Outputting array..." << endl;
output(transactions, size);
for(int x : transactions)
{
    cout << x << endl;
}
    
    
    return 0;
}

void output(const vector<int> &v)
{
    for(int i : v)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}
void output(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}
void storeValuesFromUser(vector<int> &v)
{
      for (int i = 0; i < v.size(); i++)
    {
        cout << " Please enter a value: ";
        int value;
        cin >> value;
        
        v[i] = value;
    }  
}
void storeValuesFromUser(int a[], int size)
{
          for (int i = 0; i < size; i++)
    {
        cout << " Please enter a value: ";
        int value;
        cin >> value;
        
        a[i] = value;
    }  
}
int getNumberOfOdds(const vector<int> &v)
{
    // counter
    int numOdds = 0;
    
    for(int i = 0; i < v.size();i++)
    {
        if (v[i] % 2 == 1)
        {
            numOdds++;
        }
    }
    return numOdds;
}
int getNumberOfOdds(int a[], int size)
{
    //counter
    int numOdds = 0;
    for(int i = 0; i < size; i++)
    {
        if(a[i] % 2 == 1)
        {
            numOdds++;
        }
    }
    return numOdds;
}