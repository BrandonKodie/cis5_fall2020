#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

// Create a house class with 1 attribute
class House
{
public:
    //Constructor
    // Default Constructor
    House();
    
    //2nd constructor
    House(int sqFootage);
    
    //This is like vector
    //vector<int> v;     // defualt
    //vector<int> v1(10; // like 2nd construct
    
    // Get/Set square footage
    int getSqFootage();
    void setSqrFootage(int sqFootage); //Setter
private:
    int sqFootage;
    
};
// : -> inherits from
// Trailer inherits from house
class Trailer : public House
{
private:
    int numWheels;
};

void addValue(int* &p,int &size,int);
void output(int* p, int size);
void output(const vector<int> &v);

int main(int argc, char** argv)
{
    House house;
   
    //house.setSqrFootage(200);
    
    House bigHouse(2000);
   // bigHouse.setSqrFootage(2000);
    
    //very common mistake
    House badHouse(); // logic error! Function called bad house
    
    cout << "Sq Footage: " << house.getSqFootage() << endl;
    
    cout << "Sq Footage: " << bigHouse.getSqFootage() << endl;
    
    //REVIEW
    //Structures
    struct TinyHouse
    {
        int sqFootage;
    };
    // structures short term -> classes long term
    
    // Pointers
    // Store memory location
    int x = 12;
    int* p;
    
    //p can point to any integer
    p = &x;
    cout << "Value of p: " << p << endl;
    cout << "Address of p: " << &p << endl;
    cout << "Value at  p: " << *p << endl;
    
    // Allocate and return an address from the heap
    // using the new keyword
    p = new int;
    
    cout << "Value at heap: " << p << endl;
    cout << "Value at heap: " << *p << endl;
    // Remember to deallocate allocated data to avoid
    // memory leaks
    delete p;
    
    // Dangling pointer error! Avoid by making 
    // p = nullptr;
    
    *p = 10;
    cout << "AFter deallocation: " << *p << endl;
    p = nullptr;
    
    // Dynamic Arrays
    int size = 5;
    p = new int[size];
    
    
    delete[] p;
    // Arryas are pointers
    int a[10];
    p = a; // Legal because arrays and pointers are the same
    cout << "Array: "<< a << endl;
    cout << "Pointer:"<< p << endl;
    
    size = 0;
    p = new int[size];
    
    addValue(p, size, 10);
    output(p, size);
    addValue(p, size, 20);
    output(p, size);
    addValue(p, size, 30);
    output(p, size);
    addValue(p, size, 40);
    output(p, size);
    
    delete [] p;
    
    // Static Arrays 
    int staticArray[10];
    
    // Static arrays are on stack.
    // Dynamic array are on the heap.
    // Both  can not change size or location
    //"pseudo increase" for dynamic array
    
    // 2 ways of creating vectors
    vector<int> v;
    vector<int> v2(10);
    
    v.push_back(10);
    v.push_back(20);
    v.push_back(30);
    cout << "Outputting vector! " << endl;
    output(v);
    
    // Dynamic Arrays -> Good for dynamic size. Very good for memory
    // Vectors -> Everything is encapsulated. Not good if memory restrictive.
    
    // Sorting 
    // Bubble Sort - > "bubble" 2 values. Swap if needed. N-squared so
    // Insertion Sort - > Insert unsorted values into the sorted. N-squared
    // Selection -> Finds minimum in unsorted region. Places at end of 
    // Merger Sort -> "divide and conquer". N log(N) search time
    
    //Searching
    // Linear search - > N search time
    // Binary search -> log(N) search time. Container needs to be sorted
    
    // Static arrays -> need to know size before hand
    // Partially filled arrays -> userinput. Cap on memory 
    int capacity = 5;
    int currentSize = 0;
    int partiallyFilledArray[capacity];
    
    // File I/O
    // Basics 
    // Step 1) Open the file
    // Step 2) Acces/manipulate the file
    // Step 3) Close the file
    
    // Pre-Midterm
    x = 3;
    int y = 12;
    int z = 5;
    
    cout << "X: " << x << "Y: " << y << "Z: " << z << endl;
    
    cout << "X: " << x << "Y: " << y << "Z: " << z << endl;
    // Flowchart
    // Diamonds -> Decision making
    // Rctangles - > statements
    // Circles -> continuation
    // Ovals - > start/end
    // Parallelograms -> I/o
    
    // Pseudo-code
    // Writing an algorithm that can be interpreted in any language
    // x: =3
    // from i = 2 to 10
    
    // Strings -> character arrays
    // . substr(1, 2); 1 is the start location. 2 is the # charactes
    // .substr(2); // 2 is location. Reads until end
    // .size() / . length()
    // .find("c");
    
    // for loops - > known number of loops. Vectors
    // while loops - > unknown number of loops. User input
    // do-while loops -> unknown also, always loop once. Games
    
    // If statements
    // Just is statement is OK
    // if-else
    //if- elseif-else. Always need else on an else if
    
    // Compound booleans
    // if(x > 2 && x < 10)
    // demorgans law
    
    //Integer Division
    // 2 / 5 -> 0
    // 2 / 5. - > .4
    // 2 % 5 == 2
    // 5 % 2 == 1
    // 4 / 5 = 0
    
    //Scope
    //Scope -> From the definition to the end of the innermost block
    int loopCount = 10;
    for(int i = 0; i < loopCount; i++)
    {
        int loopCount = 5;
        for(int j = 0; j < loopCount; j++)
        {
            cout << i + j * loopCount << " ";
        }
        cout << endl;
    }
    cout << endl;
    
    return 0;
}

// Use resolution scope ::
int House::getSqFootage()
{
    return sqFootage;
}

void House::setSqrFootage(int sqF)
{
    sqFootage = sqF;
}
House::House()
{
    sqFootage = 150;
}
House::House(int sqF)
{
    sqFootage = sqF;
}
void output(int* p,int size)
{
    for(int i = 0; i < size;i++)
    {
        cout << p[i] << " ";
        
    }
    cout << endl;
}
void addValue(int* &p, int &size, int value)
{
    // Step 1 > create new array
    int* temp = new int[size + 1];
    
    // Step 2 -> copy vaules
    for(int i = 0; i < size;i++)
    {
        temp[i] = p[i];
    }
    
    //Step 3 -> assign new value to a new array
    temp[size] = value;
    
    //Step 4 -> Deallocate the old one
    delete [] p;
    
    // Step 5 -> Reassign to new dynamic array
    p = temp;
    
    // Step 6 - > Adjust 
}
void output(const vector<int> &v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}