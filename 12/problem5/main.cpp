#include<iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
void output2d(int** array, int row, int col);
void storeIncrease(int** array, int row, int col);
int main(int argc, char** argv) {
    int input;
    int colSize;
    string menuInput;
    int rowSize;
    cout << "Input col size" << endl;
    cin >> input;
    colSize = input;
    cout << "Input row size" << endl;
    cin >> input;
    rowSize = input;
    int multi[colSize][rowSize];
    
    int** pntRow = new int*[rowSize];
    
    for(int i = 0; i < rowSize;i++)
        {
        pntRow[i] = new int[colSize];
        }
    storeIncrease(pntRow,rowSize,colSize);
    output2d(pntRow,rowSize,colSize);
    
    for(int i = 0; i < rowSize; i++)
        {
        delete[] pntRow[i];
        }
    delete[] pntRow;
    
    cout << "Would you like to add another row? y/n" << endl;
        cin >> menuInput;
     
        do
    {
        rowSize++;
        int** pntRow = new int*[rowSize];
        for(int i = 0; i < rowSize;i++)
            {
            pntRow[i] = new int[colSize];
            }
        
        storeIncrease(pntRow,rowSize,colSize);
        output2d(pntRow,rowSize,colSize);
        for(int i = 0; i < rowSize; i++)
            {
            delete[] pntRow[i];
            }
        delete[] pntRow;
        cout << "Would you like to add another row? y/n" << endl;
        cin >> menuInput;
        
    }while (tolower(menuInput[0]) == 'y');

    for(int i = 0; i < rowSize; i++)
        {
        delete[] pntRow[i];
        }
    delete[] pntRow;
    return 0;
}

void output2d(int** array, int row, int col)
{
       for(int i = 0; i < row; i++)
    {
        for(int k = 0; k < col; k++)
        {
            cout << array[i][k] << " ";
        }
        cout << endl;
    }
}
void storeIncrease(int** array, int row, int col)
{
    int counter = 0;
       for(int i = 0; i < row; i++)
    {
        for(int k = 0; k < col; k++)
        {
            counter++;
            array[i][k] = counter;
        }
    }
}
void addToSize(int* &array, int &rowSize, int num)
{
    
     int *array2 = new int [rowSize + 1];
   
     for(int i = 0; i < rowSize;i++)
     {
         array2[i] = array[i];
     }
     
    array2[rowSize] = num;
    delete[] array;
    array = array2;
    rowSize++;

}