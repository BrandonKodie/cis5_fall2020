
#include <cstdlib>
#include<iostream>
#include<fstream>
using namespace std;
struct Time
{
    
    int seconds;
    int minutes;
    int hours;
};

class Classtime
{
public:
    Classtime()
    {
        seconds = 0;
        minutes = 0;
        seconds = 0;
    }
    Classtime(int h, int m, int s)
    {
        setHour(h);
        setMinutes(m);
        setSeconds(s);
    }
    void setHour(int h);
    int getHour();
    void setSeconds(int s);
    int getSeconds();
    void setMinutes(int m);
    int getMinutes();
    void output(Classtime p);
    int seconds;
    int minutes;
    int hours;
    
private:
    
   
};
// create child class date Problem 10
class Date : public Classtime 
{
public:
    Date()
    {
        month = 1;
        year = 1990;
    }
    Date(int m, int y)
    {
        month = m;
        year = y;
    }
    Date(int m, int y, int h, int b, int s)
    {
        minutes = b;
        hours = h;
        seconds = s;
        month = m;
        year = y;
    }
    void output(Date p);
    int month;
    int year;
private:
    
};
void output(const Date &p);
void output(const Time &p);
void output(const Classtime &p);
/*
 * 
 */
int main(int argc, char** argv) {

    int option;
    string menuInput;
    
    do
    {

        cout << "Homework 12 Menu" << endl;
        cout << "1.Problem 3" << endl;
        cout << "2.Problem 4" << endl;
        cout << "3.Problem 5" << endl;
        cout << "4.Problem 6" << endl;
        cout << "6.Problem 8" << endl;
        cout << "7.Problem 13" << endl;
       
        cout << endl << "Please select a problem to review" << endl;
        cin >> option;

        switch(option)
            {


            case 1:
                //problem 3
            {
    Time day;
    Classtime halfDay;
    output(day);
    output(halfDay);
            }
                break;

            case 2:
//problem 4{
            {   Time day; // Creating the struct instance
    
    output(day); //outputting struct before attributes have been added
    
    day.seconds = 12; // Adding 12 to struct day seconds variable;
    day.minutes = 33; //Adding 33 to struct day minutes variable;
    day.hours = 5; // Adding 5 to struct day hours variable;
    
    Classtime halfDay;
    
    output(halfDay); // outputting class before attributes have been added
    
    halfDay.seconds = 3; // Adding 3 to Classtime halfDay seconds variable;
    halfDay.minutes = 22; // Adding 22 to Classtime halfDay minutes variable;
    halfDay.hours = 9; // Adding 9 to Classtime halfDay hours variable;
    
    output(day); // outputting struct after attributes have been added
    
    output(halfDay);// outputting class after attributes have been added
            }
                break;

            case 3:
            {
                //problem 5
        //Can not access private variables
                
   // Time day; // Creating the struct instance
    
    //output(day); //outputting struct before attributes have been added
    
   // day.seconds = 12; // Adding 12 to struct day seconds variable;
   // day.minutes = 33; //Adding 33 to struct day minutes variable;
    //day.hours = 5; // Adding 5 to struct day hours variable;
    
    //Classtime halfDay;
    
    //output(halfDay); // outputting class before attributes have been added
    
   // halfDay.seconds = 3; // Adding 3 to Classtime halfDay seconds variable;
    //halfDay.minutes = 22; // Adding 22 to Classtime halfDay minutes variable;
    //halfDay.hours = 9; // Adding 9 to Classtime halfDay hours variable;
    
    //output(day); // outputting struct after attributes have been added
    
   // output(halfDay);// outputting class after attributes have been added
               
            }
                break;

            case 4:
            {
// problem 6
                    Time day; // Creating the struct instance
    
    output(day); //outputting struct before attributes have been added
    
    day.seconds = 12; // Adding 12 to struct day seconds variable;
    day.minutes = 33; //Adding 33 to struct day minutes variable;
    day.hours = 5; // Adding 5 to struct day hours variable;
    
    Classtime halfDay;
    
    output(halfDay); // outputting class before attributes have been added
    
    halfDay.seconds = 3; // Adding 3 to Classtime halfDay seconds variable;
    halfDay.minutes = 22; // Adding 22 to Classtime halfDay minutes variable;
    halfDay.hours = 9; // Adding 9 to Classtime halfDay hours variable;
    
    output(day); // outputting struct after attributes have been added
    
    output(halfDay);// outputting class after attributes have been added
            }
                break;

            case 5:
// Problem 8
            {
                Classtime one(1, 2,3);
                
                output(one);
        }
                break;

            case 6:
//Problem 9
            {     
                Classtime halfDay; // create halfDay from Classtime
                output(halfDay); // output default 
                Classtime bob(1,2,3); // create bob from Classtime using parameters 
    
                output(bob); // outputs bob
            }
                break;

            case 7:
//Problem 13    
            {Date date1;
                Date date2( 05,1990);
                Date date3(3,12,5,05,1990);
                output(date1);
                output(date2);
                output(date3);
        }
                break;

            default:

                cout << "Invalid input. Please enter 1-8" << endl;
                // displays if input is not 1-8
            }
        cout << "Return to main menu? y/n" << endl;
        cin >> menuInput;
    }while (tolower(menuInput[0]) == 'y'); 
    return 0;
}
void output(const Time &p)
{
    cout << "Struct Time" << endl;
    cout << "hours: " << p.hours << endl;
    cout << "minutes: " << p.minutes << endl;
    cout << "seconds: " << p.seconds << endl << endl;
   
}
void output(const Classtime &p)
{
    ofstream fout;
    fout.open("data.dat");
    
    cout << "Class Time" << endl;
    cout << "hours: " << p.hours << endl;
    fout << p.hours << endl;
    cout << "minutes: " << p.minutes << endl;
    fout << p.minutes << endl; 
    cout << "seconds: " << p.seconds << endl << endl;
    fout << p.seconds << endl;
    fout.close();
}
void output(const Date &p)                          //Problem 12
{
    ofstream fout;
    fout.open("data.dat");
            
    cout << "Class Time" << endl;
    cout << "hours: " << p.hours << endl;
    fout << p.hours << endl;
    cout << "minutes: " << p.minutes << endl;
    
    cout << "seconds: " << p.seconds <<  endl;
    cout << "month: " << p.month << endl;
    cout << "year: " << p.year << endl;
    fout.close();
}
 void Classtime::setHour(int h)
    {
        hours = h;
    }
 
 int Classtime::getHour()
    {
        return hours;
    }
    void Classtime::setSeconds(int s)
    {
        seconds = s;
    }
    
    int Classtime::getSeconds()
    {
        return seconds;
    }
    void Classtime::setMinutes(int m)
    {
        minutes = m;
    }
    
    int Classtime::getMinutes()
    {
        return minutes;
    }
    
