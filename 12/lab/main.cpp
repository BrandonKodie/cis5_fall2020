#include<iostream>
#include <cstdlib>

using namespace std;

// Create a house class with 1 attribute
class House
{
public:
    //Constructor
    // Default Constructor
    House();
    
    //2nd constructor
    House(int sqFootage);
    
    //This is like vector
    //vector<int> v;     // defualt
    //vector<int> v1(10; // like 2nd construct
    
    // Get/Set square footage
    int getSqFootage();
    void setSqrFootage(int sqFootage); //Setter
private:
    int sqFootage;
    
};
// : -> inherits from
// Trailer inherits from house
class Trailer : public House
{
private:
    int numWheels;
};



int main(int argc, char** argv)
{
    House house;
   
    //house.setSqrFootage(200);
    
    House bigHouse(2000);
   // bigHouse.setSqrFootage(2000);
    
    //very common mistake
    House badHouse(); // logic error! Function called bad house
    
    cout << "Sq Footage: " << house.getSqFootage() << endl;
    
    cout << "Sq Footage: " << bigHouse.getSqFootage() << endl;
    
    //REVIEW
    //Structures
    struct TinyHouse
    {
        int sqFootage;
    };
    // structures short term -> classes long term
    
    // Pointers
    // Store memory location
    int x = 12;
    int* p;
    
    //p can point to any integer
    p = &x;
    cout << "Value of p: " << p << endl;
    cout << "Address of p: " << &p << endl;
    cout << "Value at  p: " << *p << endl;
    
    // Allocate and return an address from the heap
    // using the new keyword
    p = new int;
    
    cout << "Value at heap: " << p << endl;
    cout << "Value at heap: " << *p << endl;
    // Remember to deallocate allocated data to avoid
    // memory leaks
    delete p;
    
    // Dangling pointer error! Avoid by making 
    // p = nullptr;
    
    *p = 10;
    cout << "AFter deallocation: " << *p << endl;
    p = nullptr;
    
    // Dynamic Arrays
    int size = 5;
    p = new int[size];
    
    
    delete[] p;
    // Arryas are pointers
    int a[10];
    p = a; // Legal because arrays and pointers are the same
    cout << "Array: "<< a << endl;
    cout << "Pointer:"<< p << endl;
    
    return 0;
}

// Use resolution scope ::
int House::getSqFootage()
{
    return sqFootage;
}

void House::setSqrFootage(int sqF)
{
    sqFootage = sqF;
}
House::House()
{
    sqFootage = 150;
}
House::House(int sqF)
{
    sqFootage = sqF;
}