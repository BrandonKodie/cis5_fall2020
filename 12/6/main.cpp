#include<iostream>
#include <cstdlib>

using namespace std;

/*
 * 
 */
struct Time
{
    
    int seconds;
    int minutes;
    int hours;
};
class Classtime
{
public:
    Classtime()
    {
        cout << "The time is: ";
    }
    Classtime(int h, int m, int s)
    {
        cin >> h;
        getHour(h);
        cin >> m;
        setMinutes(m);
        cin >> s;
        setSeconds(s);
    }
    void setHour(int h)
    {
        hours = h;
    }
    
    int getHour()
    {
        return hours;
    }
    void setSeconds(int s)
    {
        seconds = s;
    }
    
    int getSeconds()
    {
        return seconds;
    }
    void setMinutes(int m)
    {
        minutes = m;
    }
    
    int getMinutes()
    {
        return minutes;
    }
     int seconds;
    int minutes;
    int hours;
private:
   
    
};
void output(const Time &p);
void output(const Classtime &p);
int main(int argc, char** argv) {

    Time day; // Creating the struct instance
    
    output(day); //outputting struct before attributes have been added
    
    day.seconds = 12; // Adding 12 to struct day seconds variable;
    day.minutes = 33; //Adding 33 to struct day minutes variable;
    day.hours = 5; // Adding 5 to struct day hours variable;
    
    Classtime halfDay;
    
    output(halfDay); // outputting class before attributes have been added
    
    halfDay.seconds = 3; // Adding 3 to Classtime halfDay seconds variable;
    halfDay.minutes = 22; // Adding 22 to Classtime halfDay minutes variable;
    halfDay.hours = 9; // Adding 9 to Classtime halfDay hours variable;
    
    output(day); // outputting struct after attributes have been added
    
    output(halfDay);// outputting class after attributes have been added
    return 0;
}

void output(const Time &p)
{
    cout << "Struct Time" << endl;
    cout << "hours: " << p.hours << endl;
    cout << "minutes: " << p.minutes << endl;
    cout << "seconds: " << p.seconds << endl << endl;
   
}
void output(const Classtime p)
{
    cout << "Class Time" << endl;
    cout << "hours: " << p.getHour() << endl;
    cout << "minutes: " << p.getMinutes() << endl;
    cout << "seconds: " << p.getSeconds() << endl << endl;
   
}