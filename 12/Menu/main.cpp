#include<iostream>
#include <cstdlib>

using namespace std;

struct Animal 
{
    string name;// the animals name
    string type; // The type of animal
    int age;
   
};
void output2d(int** array, int row, int col);
void storeIncrease(int** array, int row, int col);
void output(Animal student);
const int row = 4;
const int col = 4;
void output2d(char** array, int row, int col);
void storeStars(char** c, int row, int col);
void lowerSize(string* &array, int &size, int loc);
void addToSize(string* &array, int &size, string num);
void output(string* names, int size);
int main(int argc, char** argv) {
    int option;
    int size = 5;// size of array
    int loc;// the location of the name to remove
    string nameList[size] = {"bob", "judy", "mark", "mike", "sally"}; //static array of strings
    int size2 = 5;
    char array[row][col];
    string input2, name2; // variables for user input of name and menu option
    Animal subject1; //create an instance to hold user input
    int age; // for subject.age
    string name;// will be used for subject1 name
    string type;// will be used for subject1 type
    int input;
    int colSize;
    string menuInput,menuInput2;
    int rowSize;
    
    do
    {
        cout << "Problem 1" << endl;
        cout << "Problem 2" << endl;
        cout << "Problem 3" << endl;
        cout << "Problem 4" << endl;
        cout << "Problem 5" << endl;
        cout << endl << "Select a problem to review" << endl;
        cin >> option;
        
        switch(option)
        {
            case 1:
            {
            string* names = new string[size]; // pointer array
            names[size];

            for(int i =0; i < size; i++)// loop size times 
                {
                names[i] = nameList[i];// Copy name
                }
            
            output(names,size);
            
            cout << endl << "Please enter a location to remove" << endl;
            cin >> loc;// user inputs which loc to remove
            
            while(loc >=5)//error check
                {
                cout << "Invalid input. Enter a location" << endl;
                cin >> loc;
                }
            
            lowerSize(names, size, loc);
            
            output(names,size);//output
            }
            break;            
            case 2:
            {
            string* names = new string[size2]; //created pointer for array
            names[size2];// pnt array

            for(int i =0; i < size2; i++) // loops through both array
                {
                names[i] = nameList[i];// copies names from static to dynamic
                }
            
            output(names, size2);
            
            cout << endl << "Would you like to add a name? y/n" << endl;
            cin >> input2;
            
            while(tolower(input2[0]) != 'y' && tolower(input2[0]) != 'n' )
                {
                cout << "Invalid input. Try again" << endl;// error check
                cin >> input2;
                }
            
            while (input2[0] != 'n')// loop while uses inputs y
                {
                cout << "Enter name" << endl;
                cin >> name2;//user inputs name
                addToSize (names, size2, name2);// array is increased, size increases, and user input is applied
                output(names, size2);// outputs updated array to screen
                cout << endl <<  "Would you like to add another name y/n" << endl;
                cin >> input2;// user inputs y to loop again or n to stop
                }
            
            output(names, size2);// final output
            }
            break;
            case 3:
            {
            char** pntRow = new char*[row];
           //
            for(int i = 0; i < row; i++)
                {
                pntRow[i] = new char[col];
                }

            storeStars(pntRow,row,col);

            output2d(pntRow,row, col);
            
            for(int i = 0; i < row; i++)
                {
                delete[] pntRow[i];// Deallocate 
                }
            
            delete[] pntRow;// Deallocate 
            }
            break;
            case 4:
            {
            cout << "Please enter the animals name:" << endl;
            cin >> name;

            subject1.name = name;// Applies user input to the name variable of subject 1

            cout << "Please enter the type of animal:" << endl;
            cin >> type;// User input for type

            subject1.type = type; // Input applied to subject1.type

            cout << "How old is " << name << "?" << endl;
            cin >> age; // user input int value for age

            subject1.age = age; //user input is stored as subject1.age
            
            output(subject1);  
            }
            break;
            case 5:
            {
            cout << "Input col size" << endl;
            cin >> input;
            
            colSize = input;
            
            cout << "Input row size" << endl;
            cin >> input;
            
            rowSize = input;
            
            int multi[colSize][rowSize];

            int** pntRow = new int*[rowSize];

            for(int i = 0; i < rowSize;i++)
                {
                pntRow[i] = new int[colSize];
                }
            
            storeIncrease(pntRow,rowSize,colSize);
            
            output2d(pntRow,rowSize,colSize);

            for(int i = 0; i < rowSize; i++)
                {
                delete[] pntRow[i];
                }
            
            delete[] pntRow;

            cout << "Would you like to add another row? y/n" << endl;
                cin >> menuInput;

                do
                {
                rowSize++;
                int** pntRow = new int*[rowSize];
                
                for(int i = 0; i < rowSize;i++)
                    {
                    pntRow[i] = new int[colSize];
                    }

                storeIncrease(pntRow,rowSize,colSize);
                
                output2d(pntRow,rowSize,colSize);
                
                for(int i = 0; i < rowSize; i++)
                    {
                    delete[] pntRow[i];
                    }
                
                delete[] pntRow;
                
                cout << "Would you like to add another row? y/n" << endl;
                cin >> menuInput;

                }while (tolower(menuInput[0]) == 'y');

            for(int i = 0; i < rowSize; i++)
                {
                delete[] pntRow[i];
                }
                
            delete[] pntRow;
            }
            break;
            default:
                cout <<"Invalid";
           
        }
        cout << endl << "Return to menu? y/n" << endl;
        cin >> menuInput;
        
    }while(tolower(menuInput[0]));
    
    
    
    
    
    
    
    return 0;
}

void lowerSize(string* &array, int &size, int loc)
{
    
    string *array2 = new string [size - 1];//Creates an array one size lower
        
    int counter = 0;
    int k = 0;
     for(int i = 0; i < size ;i++)
     {
         
         if(i == loc)
         {
             counter++;
             cout << endl;
         }
         else if(counter >= 1)
         {
             array2[k] = array[i];
             k++;  
         }
         else
         {
         array2[k]= array[i];
         k++;
         }
        
     }
     
    delete[] array;
    array = array2;
    size--;

}
/**
 * prints pointer array to screen
 * @param names
 * @param size
 */

void output(string* names, int size)
{
          for(int i =0; i < size; i++)
    {
        cout << names[i] << " ";
    } 
}
/**
 * Creates a new dynamic array one size larger, copy original over
 * then stores num as the last element 
 * @param array the array
 * @param size the size passed in to be used and increased
 * @param name the name to add
 */
void addToSize(string* &array, int &size, string name)
{
    
     string *array2 = new string [size + 1];
   
     for(int i = 0; i < size;i++)
     {
         
         array2[i] = array[i]; // copies old array to new 
     }
     array2[size] = name; // stores name as last element 
   
    delete[] array; // deallocate 
    array = array2; // assign the new array 
    size++; // increases size used for array

}
void output2d(char** array, int row, int col)
{
       for(int i = 0; i < row; i++)
    {
        for(int k = 0; k < col; k++)
        {
            cout << array[i][k];
        }
        cout << endl;
    }
}
void storeStars(char** array, int row, int col)
{
       for(int i = 0; i < row; i++)
    {
        for(int k = 0; k < col; k++)
        {
            if(i == 0 || i == 3)
            {
            array[i][k] = '*';
            }
            else if(k == 0 || k == 3)
            {
                array[i][k] = '*';
            }
            else
            {
                array[i][k] = ' ';
            }
        }
    }
}
void output(Animal student)
{
    cout << "Name: " << student.name << endl;
    cout << "Type of Animal: " << student.type << endl;
    cout << "Age " << student.age << endl;
   
}
void output2d(int** array, int row, int col)
{
       for(int i = 0; i < row; i++)
    {
        for(int k = 0; k < col; k++)
        {
            cout << array[i][k] << " ";
        }
        cout << endl;
    }
}
void storeIncrease(int** array, int row, int col)
{
    int counter = 0;
       for(int i = 0; i < row; i++)
    {
        for(int k = 0; k < col; k++)
        {
            counter++;
            array[i][k] = counter;
        }
    }
}