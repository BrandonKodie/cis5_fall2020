#include<iostream>
#include <cstdlib>
#include<vector>
#include<fstream>
using namespace std;

/*
 * 
 */
void highest(vector<int> v1);
void lowest(vector<int> v1);
int main(int argc, char** argv)
{
    
    int input;
    vector<int> v1;
    ifstream fin;            
    cout << endl << "Please enter a file name:" << endl;
    string filename;
    cin >> filename;
   
    fin.open(filename);

        if (fin.fail())
        {
            cout << "The file does not exist!" << endl;
        }
        else 
        {    
            cout << "The numbers contained in " << filename << " are :" << endl;     
            while (fin >> input)
            {
                v1.push_back(input);
                cout << input << endl;
            }
        }
    cout << endl;        
    fin.close();
    highest(v1);
    cout << endl;
    lowest(v1);
    cout << endl;
    for (int x : v1)
        if(x % 2 == 0)
        {
            count++;
        }
    cout << "This vector contains " << count << " even numbers." << endl;
    return 0;
}

void highest(vector<int> v1)
{
    int highest = v1[0];
        for (int i = 0; i < v1.size(); i++)
        {
            if (v1[i] > highest)
            {
            highest = v1[i];
            }
        
        }
     cout << highest << " is the highest." << endl; 
}
void lowest(vector<int> v1)
{
    int lowest = v1[0];
        for (int i = 0; i < v1.size(); i++)
        {
            if (v1[i] < lowest)
            {
            lowest = v1[i];
            }
        
        }
     cout << lowest << " is the lowest." << endl; 
}