#include<vector>
#include <cstdlib>
#include<iostream>

using namespace std;

/*
 * 
 */
void AddRandomTenToVector(vector<int>& size);
int main(int argc, char** argv)
{
    int count = 0;
    srand(time(0));
    cout << "Create an empty vector " << endl;
    vector<int> v1;
    cout << "Invoke AddRandomTenToVector function" << endl;
    AddRandomTenToVector(v1);
    cout << "Print Vector" << endl;
    for(int x : v1)
        cout << x << endl;
    for(int x : v1)
        if (x % 2 != 0)
        {
            count++;
        }
    cout << "You have " << count << " odd integers inside the vector" << endl;
    
    
    return 0;
}

void AddRandomTenToVector(vector<int>& size)
{

    int high = 100;
    int low = 1;
    
    for (int i = 0; i < 10; i++)
    { 
        int randValue = rand() % (high - low + 1) + low;
        size.push_back(randValue);
    }
    
}