#include<iostream>
#include <cstdlib>
#include<fstream>
#include<vector>
#include <algorithm>
#include <ctime>
using namespace std;

/*
 * 
 */
void highest(vector<int> v1);
void lowest(vector<int> v1);
void findMedian(vector<int> v1);
void AddRandomTenToVector(vector<int>& size);
int main(int argc, char** argv)
{
    string menuInput;
    do
    {
    string filename;;
    int input, option, count = 0;
    vector<int> v1;
    ifstream fin;
    srand(time(0));
    
    cout << "HomeWork 7" << endl;
    for( int i = 0; i < 6; i++)
        cout << i + 1 << ". Problem " << i + 1 << endl;
    cout << endl << "Please select a problem to review" << endl;
    cin >> option;
    switch(option)
    {
        case 1:
    cout << "Search file data.dat and write the highest and lowest :" << endl;
    fin.open("data.dat");
    while (fin >> input)
    {
        v1.push_back(input);
        cout << input << endl;
    }
    fin.close();
    highest(v1);  
    lowest(v1);    
    cout << endl << v1[0] << endl;
            break;   
        case 2:
    cout << "Search file received from user then display highest and lowest" << endl;       
    cout << "Please enter a file" << endl;
    cin >> filename;
    fin.open(filename);
        if (fin.fail())
        {
            cout << "The file does not exist!" << endl;
        }
        else 
        {                
            while (fin >> input)
            {
                v1.push_back(input);
                cout << input << endl;
            }
        }        
    fin.close();
    highest(v1);  
    lowest(v1);       
            break;
        case 3:
    cout << "Displays the amount of even numbers contained in a file given by the user"
            << endl;
    cout << endl << "Please enter a file name:" << endl;
    cin >> filename; 
    fin.open(filename);
        if (fin.fail())
        {
            cout << "The file does not exist!" << endl;
        }
        else 
        {    
            cout << "The numbers contained in " << filename << " are :" << endl;     
            while (fin >> input)
            {
                v1.push_back(input);
                cout << input << endl;
            }
        }
    cout << endl;        
    fin.close();
    highest(v1);
    cout << endl;
    lowest(v1);
    cout << endl;
    for (int x : v1)
        if(x % 2 == 0)
        {
            count++;
        }
    cout << "This file contains " << count << " even numbers." << endl;
            break;
        case 4:
    cout << "Compute the median of a data file containing an odd or even amount of numbers" 
            << endl;
    cout << endl << "Please enter a file" << endl;
    cin >> filename;
    fin.open(filename);
        if (fin.fail())
        {
            cout << "The file does not exist!" << endl;
        }
        else 
        {                
            while (fin >> input)
            {
                v1.push_back(input);
                cout << input << endl;
            }
        }      
    fin.close();
    cout << endl << "Sorting " << endl;
    sort(v1.begin(), v1.end());
    for(int x : v1)
    cout << x << endl;
    cout << "The median is ";
    findMedian(v1);
    cout << endl;
            
            break;
        case 5:
    cout << "insert 10 random integers into a vector" << endl;
    cout << "Invoke AddRandomTenToVector function" << endl;
    AddRandomTenToVector(v1);
    cout << "Print Vector" << endl;
    for(int x : v1)
        cout << x << endl;
            break;
        case 6:
    cout << "Insert 10 random integers into a vector then display the number of odd integers"
            << endl;
    cout << "Invoke AddRandomTenToVector function" << endl;
    AddRandomTenToVector(v1);
    cout << "Print Vector" << endl;
    for(int x : v1)
        cout << x << endl;
    cout << endl << "Count number of odd integers" << endl;
    for(int x : v1)
        if (x % 2 != 0)
        {
            count++;
        }
    cout << "You have " << count << " odd integers inside the vector" << endl;
            break;
        default:
            cout << "Input not valid" << endl;
    }
    cout << "Return to main menu? y/n" << endl;
    cin >> menuInput;
    }while (tolower(menuInput[0]) == 'y');         
    return 0;
}

void highest(vector<int> v1)
{
    int highest = v1[0];
    /*for (int i = 0; i < v1.size(); i++)
    {
        if (v1[i] > highest)
        {
        highest = v1[i];
        }
    */
    for(int i : v1)
    {
        if (i > highest)
        {
            highest = i;
        }
    }
     cout << highest << " is the highest." << endl; 
}
void lowest(vector<int> v1)
{
    int lowest = v1[0];
    /*for (int i = 0; i < v1.size(); i++)
    {
        if (v1[i] < lowest)
        {
        lowest = v1[i];
        }
    */
    for (int i : v1)
    {
        if (i < lowest)
        {
        lowest = i;
        }
    }
     cout << lowest << " is the lowest." << endl; 
}
void findMedian(vector<int> v1)
{
    if(v1.size() % 2 == 0)
    {
       double median = static_cast<double>(v1[v1.size() / 2] + v1[(v1.size() / 2) - 1]) / 2;
       cout << median << endl;
    }
    else
    {
       cout << v1[v1.size() / 2];
    }    
}
void AddRandomTenToVector(vector<int>& size)
{

    int high = 100;
    int low = 1;
    for (int i = 0; i < 10; i++)
    { 
        int randValue = rand() % (high - low + 1) + low;
        size.push_back(randValue);
    }
    
}