#include<iostream>
#include <cstdlib>

using namespace std;


struct Animal 
{
    string name;// the animals name
    string type; // The type of animal
    int age;
   
};

void output(Animal student);

int main(int argc, char** argv)
{
    
    Animal subject1; //create an instance to hold user input
    int age; // for subject.age
    string name;// will be used for subject1 name
    string type;// will be used for subject1 type
    cout << "Please enter the animals name:" << endl;
    cin >> name;
    
    subject1.name = name;// Applies user input to the name variable of subject 1
    
    cout << "Please enter the type of animal:" << endl;
    cin >> type;// User input for type
    
    subject1.type = type; // Input applied to subject1.type
    
    cout << "How old is " << name << "?" << endl;
    cin >> age; // user input int value for age
    
    subject1.age = age; //user input is stored as subject1.age
    output(subject1);

    return 0;
}
void output(Animal student)
{
    cout << "Name: " << student.name << endl;
    cout << "Type of Animal: " << student.type << endl;
    cout << "Age " << student.age << endl;
   
}
