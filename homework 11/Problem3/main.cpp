#include<iostream>

#include <cstdlib>

using namespace std;

/*
 * 
 */
const int row = 4;
const int col = 4;
void output2d(char** array, int row, int col);
void storeStars(char** c, int row, int col);
int main(int argc, char** argv) {
    
   
    char array[row][col];
    
    //
    char** pntRow = new char*[row];
   //
    for(int i = 0; i < row; i++)
    {
        pntRow[i] = new char[col];
    }
    
    storeStars(pntRow,row,col);
  
    output2d(pntRow,row, col);
    // Deallocate 
    for(int i = 0; i < row; i++)
    {
        delete[] pntRow[i];
    }
    delete[] pntRow;
    return 0;
}


void output2d(char** array, int row, int col)
{
       for(int i = 0; i < row; i++)
    {
        for(int k = 0; k < col; k++)
        {
            cout << array[i][k];
        }
        cout << endl;
    }
}
void storeStars(char** array, int row, int col)
{
       for(int i = 0; i < row; i++)
    {
        for(int k = 0; k < col; k++)
        {
            if(i == 0 || i == 3)
            {
            array[i][k] = '*';
            }
            else if(k == 0 || k == 3)
            {
                array[i][k] = '*';
            }
            else
            {
                array[i][k] = ' ';
            }
        }
    }
}
