#include<iostream>
#include <cstdlib>
#include<vector>
using namespace std;

// SYNTAX

//struct Name
//{
//  attributes/ properties 
//};
struct Person
{
    int age;
    double height;
    string name;
    double weight;
};

// PROTOTYPE
void output(const Person &p);
Person generatePerson();
int main(int argc, char** argv)
{
    // create an instance of a person
    Person matt;
    
    // modify the persons attributes
    matt.age = 50;
    matt.height = 200;
    matt.name = "Matthew";
    matt.weight = 200;
    //matt.age = matt.age - 22;
    cout << " Printing matt" << endl;
    output(matt);
    Person dave = generatePerson();
   
    cout <<"Printing dave" << endl;
    output(dave);
    
    // Create a vector of people
    vector<Person> people;
    people.push_back(generatePerson());
    people.push_back(generatePerson());
    
    // Output all the people
    for(int i = 0;i < people.size(); i++)
    {
        output(people[i]);
    }
    
    return 0;
}
/**
 * output a person struct to consol
 * @param p
 */
void output(const Person &p)
{
    cout << "Age: " << p.age << endl;
    cout << "Height: " << p.height << endl;
    cout << "Name: " << p.name << endl;
    cout << "Weight:" << p.weight << endl;;
}
/**
 * Ask the user for input
 * Create and return a person from user input
 * @return 
 */
Person generatePerson()
{
    Person temp;
    cout << " Enter an age: ";
    // TODO: should probably error check
  
    cin >> temp.age;
    
   
    cout << " Enter an height: ";
    cin >> temp.height;
    cout << " Enter an name: ";
    cin >> temp.name;
    cout << " Enter an weight: ";
    cin >> temp.weight;
    
    return temp;
}