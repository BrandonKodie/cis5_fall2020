#include<iostream>
#include <cstdlib>
#include <string>

using namespace std;

/*
 * 
 */
void addToSize(string* &array, int &size, string num);
void output(string* names, int size);
int main(int argc, char** argv) {
    int size2 = 5;
    string nameList[size2] = {"bob", "judy", "mark", "mike", "sally"}; // static array holding names
    string input2, name2; // variables for user input of name and menu option
    string* names = new string[size2]; //created pointer for array
    names[size2];// pnt array
    
        for(int i =0; i < size2; i++) // loops through both array
    {
        names[i] = nameList[i];// copies names from static to dynamic
    }
   output(names, size2);
    cout << endl << "Would you like to add a name? y/n" << endl;
    cin >> input2;
    while(tolower(input2[0]) != 'y' && tolower(input2[0]) != 'n' )
    {
        cout << "Invalid input. Try again" << endl;// error check
        cin >> input2;
    }
    while (input2[0] != 'n')// loop while uses inputs y
    {
        cout << "Enter name" << endl;
        cin >> name2;//user inputs name
        addToSize (names, size2, name2);// array is increased, size increases, and user input is applied
        output(names, size2);// outputs updated array to screen
        cout << endl <<  "Would you like to add another name y/n" << endl;
        cin >> input2;// user inputs y to loop again or n to stop
    }
    output(names, size2);// final output
    return 0;
}
/**
 * prints pointer array to screen
 * @param names
 * @param size
 */

void output(string* names, int size)
{
          for(int i =0; i < size; i++)
    {
        cout << names[i] << " ";
    } 
}
/**
 * Creates a new dynamic array one size larger, copy original over
 * then stores num as the last element 
 * @param array the array
 * @param size the size passed in to be used and increased
 * @param name the name to add
 */
void addToSize(string* &array, int &size, string name)
{
    
     string *array2 = new string [size + 1];
   
     for(int i = 0; i < size;i++)
     {
         
         array2[i] = array[i]; // copies old array to new 
     }
     array2[size] = name; // stores name as last element 
   
    delete[] array; // deallocate 
    array = array2; // assign the new array 
    size++; // increases size used for array

}