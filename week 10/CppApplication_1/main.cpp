#include<vector>
#include <cstdlib>
#include<iostream>

using namespace std;

/*
 * 
 */
void output(const vector<string> &names, const vector<int> &ids);
void deleteName(vector<string> &, vector<int> &);
void addName(vector<string> &, vector<int> &);
int findLocationOfValue(const vector<int> &, int);
void orderedDelete(vector<string>& names, int loc);
void orderedDelete(vector<int>& ids, int loc);
int main(int argc, char** argv)
{
    vector<string> names;
    vector<int> ids;
    
    const int EXIT_INPUT = 3;
    int input;
    
    do
    {
        cout << "Delete or add name?" << endl;
        cout << "1 Delete" << endl;
        cout << "2 Add name" << endl;
        cout << "3 Exit!" << endl;
        
        cin >> input;
        switch(input)
        {
            case 1: // Deleting a name
                deleteName(names, ids);
                break;
            case 2: // Adding a name
                addName(names, ids);
                break;
            case 3:
                cout << "Exiting the vectors.." << endl;
                break;
            default:
                cout << "Please enter a valid input!" << endl;
            
        }
        // Output the contents of a vector
        output(names, ids);
    }while(input != EXIT_INPUT);
    

    return 0;
}
/**
 * Outputs the parallel vector
 * @param names
 * @param ids
 */
void output(const vector<string> &names, const vector<int> &ids)
{
    for(int i = 0; i < names.size(); i++)
    {
        cout << ids[i] << ": " << names[i] << endl;
    }  
    cout << endl;
}
void deleteName(vector<string> &names, vector<int> &ids)
{
    cout << "Please enter an id to delete:";
    int idToDelete;
    cin >> idToDelete;
    
    //Find the id location of the given user input
    int loc = findLocationOfValue(ids, idToDelete);
    
    if(loc == -1)
    {
        cout << "Id is not valid. No names deleted" << endl;
    }
    else
    {
        orderedDelete(names, loc);
        orderedDelete(ids, loc);
    }
}
void addName(vector<string> &names, vector<int> &ids)
{
    cout << "Please enter a name..";
    string input;
    cin >> input;
    
    names.push_back(input);
    
    //Check if ids has ids already
    if(ids.size() > 0)
    {
        // Grab last value, and add 1 to it
        ids.push_back(ids[ids.size() - 1] + 1);
    }
    else
    {
        ids.push_back(1);
    }
}
int findLocationOfValue(const vector<int> &ids, int id)
{
    for(int i = 0; i < ids.size(); i++)
    {
        if(id == ids[i])
        {
            return i;
        }
    }       
}
void orderedDelete(vector<string>& names, int loc)
{
   if(loc >= 0 && loc < names.size())
    {
         for(int i = loc; i < names.size() - 1; i++)
         {
             names[i] = names[i + 1];
         }
         names.pop_back();
    }
    else
    {
        cout << "Invalid location provided" << endl;
    }
}
void orderedDelete(vector<int>& ids, int loc)
{
    if(loc >= 0 && loc < ids.size())
    {
         for(int i = loc; i < ids.size() - 1; i++)
         {
             ids[i] = ids[i + 1];
         }
         ids.pop_back();
    }
    else
    {
        cout << "Invalid location provided" << endl;
    }
}