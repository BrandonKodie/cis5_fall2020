#include<iostream>
#include<iomanip>
#include <cstdlib>
#include<vector>
#include<fstream>
using namespace std;
struct Party
{
    int health;
    int attack;
    string name;
    int level;
    
};
struct Inventory
{
   int potion; 
   int key;
   int floorKey;
};
struct Mob
{
    int health;
    int attack;
    int level;
    string name;
    
};
vector<int> resetVector(vector<int>& a);
void output(vector<int> v);
bool find(vector<int> v, int a);
int binarySearch(const int v[], int size, int val);
void output(Party a, Inventory b);
void output(Mob a);
void encounter(Party &hero, Mob m, Inventory &a,int& levelClear);
void roomChange(int a[],int size);
void output(int a[],int size);
void selectionSort(int v[],int size);
bool arrayCheck(int a[],int size, int number);
int main(int argc, char** argv) {
    int keyRoom1 = 0;
    int keyRoom2 = 0;
    int roomSize = 9;
    int rooms[roomSize];
    string menuInput, bossInput;
    int playerLocation = 20;
    int location;
    vector<int> explored;
    srand(time(0));
    string dungeonInput;
    string direction;
    int rowSize = 5;
    int dungeonSize = 10;
    int map[dungeonSize][dungeonSize];
    int dungeonCounter = 0;
    int potionRoom = 1;
    int keyRoom = 1;
    int levelClear = 0;
    
    Inventory bag;
    bag.potion = 1;
    bag.key = 0;
   
    Party hero1;
    hero1.health = 100;
    hero1.level = 1;
    hero1.attack = 3;
   
    Mob slime;
    slime.name = "Slime";
    slime.level = 1;
    slime.health = 10;
    slime.attack = 1;
    
    Mob skeleton;
    skeleton.name = "Skeleton";
    skeleton.level = 1;
    skeleton.health = 15;
    skeleton.attack = 3;
        
    Mob dragon;
    dragon.health = 50;
    dragon.level = 10;
    dragon.name = "Dragon";
    dragon.attack = 15;
    
    
    do
    {
    levelClear = 0;
    roomChange(rooms,roomSize);


    
    
    if(hero1.level == 1)
    {
    cout << "Enter a name for Hero 1: " << endl;
    cin >> dungeonInput;  
     hero1.name = dungeonInput;   
    }
    
    
    
    

    dungeonCounter = 0;
    
    for(int i = 0; i < rowSize; i++)\
    {
        for(int k = 0; k < dungeonSize; k++)
        {
            map[i][k] = dungeonCounter;
            dungeonCounter ++;
        }
    }
    
    cout << "W) Up A) left D) Right S) Down... type p to exit" << endl;
        do
        {
   
        
        output(hero1, bag);
        cout << endl;
        for(int i = 0; i < rowSize; i++)
            {
                for(int k = 0; k < dungeonSize; k++)
                {
                    if (map[i][k] == playerLocation)
                    {
                        cout << setw(5) << "H";
                    }
                    else if(find(explored,map[i][k]))
                    {
                        cout << setw(5) << "O";
                    }
                    else
                    cout << setw(5) << "X";
                }
                cout << endl;
            }
        int move = 0;
        
        while(move == 0)
        {
        
        cin >> direction;
            if (tolower(direction[0]) == 'w' && playerLocation - 10 >= 0)//..[;.[.[.[.
            {
                explored.push_back(playerLocation);
                playerLocation = playerLocation - 10; 
                move++;

            }
            else if(tolower(direction[0]) == 'a' && playerLocation - 1 >= 0 && playerLocation != 10 && playerLocation != 20 && playerLocation != 30 && playerLocation != 40)
            {
                explored.push_back(playerLocation);
                playerLocation = playerLocation - 1; 
                move++;
            }
            else if(tolower(direction[0]) == 'd' && playerLocation + 1 <= 49 && playerLocation != 19 && playerLocation != 29 && playerLocation != 39 && playerLocation != 9)
            {
                explored.push_back(playerLocation);
                playerLocation = playerLocation + 1; 
                move++;
                
            }
            else if(tolower(direction[0]) == 's' && playerLocation + 10 <= 49)
            {
                explored.push_back(playerLocation);
                playerLocation = playerLocation + 10;
                move++;
                
            }
            else if(tolower(direction[0]) == 'p')
            {
                cout <<"Goodbye" << endl;
                move++;       
            }
            else
            {
                cout << "Invalid Input" << endl;
            }
        }
        if(playerLocation == rooms[0])
        {
            if (keyRoom1 == 0)
            {
            cout << "You found a key!" << endl;
            bag.key++;
            keyRoom1++;
            }
            else
            {
                cout << "The key room is empty" << endl;
            }
        }
        else if(playerLocation == rooms[1])
        {
            levelClear = 0;
            cout << "The sliding sounds of wet movement become louder.."  << endl;
            encounter(hero1,slime,bag, levelClear);
            hero1.attack++;
            hero1.level++;
            slime.attack++;
            slime.level++;
            slime.health = slime.health + 10;
        }
        
        else if(playerLocation == rooms[2])
        {
            levelClear = 0;
            cout << "The sliding sounds of wet movement become louder.." << endl;
            encounter(hero1,slime,bag, levelClear);
            slime.attack++;
            slime.level++;
            slime.health = slime.health + 10;
            hero1.attack++;
            hero1.level++;
        }
        else if(playerLocation == rooms[3])
        {
            cout << "You find a fountain with restorative properties" << endl <<
                    " +2 potions" << endl;
            bag.potion = bag.potion + 2;
        }
        else if (playerLocation == rooms[4])
        {
            levelClear = 0;
            cout << "An enormous obsidian door with a lock on each side blocks the path " << endl;
            if(bag.key >= 2)
            {
                cout << "Insert both keys? y/n" << endl;
                cin >> bossInput;
                if(tolower(bossInput[0]) == 'y')
                {    
                 
                encounter(hero1,dragon,bag, levelClear);
                        if ( levelClear > 0)
                    {
                        cout << "The dragon breaks through the ceiling to escape , you follow after." << endl;
                    dragon.attack = dragon.attack + 15;
                    dragon.level = dragon.level + 10; 
                    dragon.health = dragon.health + 30;
                    resetVector(explored);
                    roomChange(rooms,roomSize);
                    keyRoom1--;
                    keyRoom2--;
                    bag.key = bag.key - 2;   
                    }
                         else
                    {
                        cout << "You could not defeat the dragon" << endl;
                    }
                }
             }       
        }
        else if(playerLocation == rooms[5])
        {
            levelClear = 0;
            cout << "The sounds of shuffling and clanking grow closer" << endl;
            encounter(hero1,skeleton,bag, levelClear);
            skeleton.attack++;
            skeleton.level++;
            skeleton.health = skeleton.health + 10;
            hero1.attack++;
            hero1.level++;
        }
        else if(playerLocation == rooms[6])
        {
            levelClear = 0;
            cout << "The sounds of shuffling and clanking grow closer" << endl;
            encounter(hero1,skeleton,bag, levelClear);
            skeleton.attack++;
            skeleton.level++;
            skeleton.health = skeleton.health + 10;
            hero1.attack++;
            hero1.level++;
        }
        else if(playerLocation == rooms[7])
        {
            if (keyRoom2 == 0)
            {
            cout << "You found a key!" << endl;
            bag.key++;
            keyRoom2++;
            }
            else
            {
                cout << "The key room is empty" << endl;
            }
        }
    }while (hero1.health > 0 && tolower(direction[0]) != 'p');
    
        if(hero1.health <= 0){
        cout << "You have died." << endl;}
    cout << "Create a new character? y/n" << endl;
    cin >> menuInput;
    }while(tolower(menuInput[0] == 'y'));
    return 0;
}
void output(Mob a)
{
    cout << "Lvl." << a.level << " " << a.name << " " << "Health: " << a.health
    << endl;
}
void output(Party a, Inventory b)
{
    cout << "Lvl." << a.level << " " << a.name << " " << "Health: " << a.health
    << " Potion(s):" << b.potion << " Key(s): " << b.key <<endl;
}
bool find(vector<int> v, int a)
{
    for (int i = 0; i < v.size(); i++)
    {
        if (v[i] == a )
        {
            return 1;
        }  
    }
    return 0;
}
void output(vector<int> v)
{
    for(int i = 0; i < v.size();i++)
    {
        cout << v[i] << " ";
    }
}
void encounter(Party& hero, Mob m, Inventory& a, int& levelClear)
{
    
    int action;
    int run = 0;
    Mob temp;
    temp.name = m.name;
    temp.attack = m.attack;
    temp.level = m.level;
    temp.health = m.health;
    while(hero.health > 0 && temp.health > 0 && run == 0)
    {
        output(temp);
        cout << endl << "1. Attack" << endl << "2. Run" << endl << "3. potion" << endl;
        output(hero, a);
        
        while (!(cin >> action))
        {

            cout << "ERROR: a number must be entered: ";

            cin.clear();

            cin.ignore(123, '\n');
        }
        if(action == 1)
        {
            temp.health = temp.health - hero.attack;
            if(temp.health <= 0)
            {
            levelClear++;
            }
            else
            {
            cout << temp.name << "Strikes back!" << endl;
            hero.health = hero.health - temp.attack;
            }
        }
        else if(action == 2) 
        {
            int flee = rand() % 10;
            if (flee % 2 == 0)
            {
               cout << "You Escaped!" << endl;
               run++;
            }
            else
            {
                cout << m.name << "has stopped you from fleeing" << endl;
                cout << m.name << "Strikes!" << endl;
                hero.health = hero.health - temp.attack;
            }
        }
        else if( action == 3)
        {
            if (a.potion > 0)
            {
                hero.health = hero.health + 30;
                a.potion--;
            }
        }
        else
        {
            cout << "Invalid input" << endl;
        }
    }
    
}
vector<int> resetVector(vector<int>& a)
{
    vector<int> v2;
    a = v2;
    return a;
}
void output(int a[],int size)
{
    for(int i = 0; i < size;i++)
    {
        cout << a[i] << " ";
    }
}
void roomChange(int a[], int size)
{
    int low = 1;
    int high = 49;
    int location;
    int random = rand() % (high - low) - low;
    for (int i = 0; i < size; i++)
    {
        while(arrayCheck(a,size,random) != true)
        {
            random = rand() % high;        
        }
        a[i] = random;
   }
    
}
bool arrayCheck(int a[],int size, int number)
{
    for(int i = 0; i < size; i++)
    {
        while(a[i] == number)
        {
            return false;
        }
    }
    return true;
}