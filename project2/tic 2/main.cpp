#include<iostream>
#include <cstdlib>
#include<vector>
#include<iomanip>
using namespace std;

/*
 * 
 */
void checkForWin(int a[], int size, int& player1,int& player2, int& gameWin);
void output(int a[], int size);
bool arrayCheck(int a[],int size, int number);
bool vectorCheck(vector<int> a, int number);
void aiPlayer(int game[], int& input);
int main(int argc, char** argv) {
    srand(time(0));
    int player1 = 0;
    int player2 = 0;
    vector<int> numbers;
    string menu;
    do 
    {
    int ticSize = 9;
    int input;
    int gameWin = 0;
    int counter = 0;
    int picks = 0;
    int game[ticSize];

    for(int i = 0; i < ticSize;i++)
    {
    counter++;
    game[i] = counter;
     }
    
    cout << "Player 1: " << player1 << " Player 2: " << player2;
    
    output(game,ticSize);
    
    while(gameWin == 0 && picks < ticSize)
    {
        
        
        if(gameWin == 0 && picks < ticSize)
        {
            cout << endl << "Pick a space for X 1-9" << endl;
            cin >> input;
        
            while(cin.fail() || game[input - 1] != input)
            {
            cin.clear();    
            cin.ignore();
            cout << "Invalid input";
            cin >> input;
            input = input;
            }
       
            for(int i = 0; i < ticSize;i++)
            {
                if (input == game[i])
                {
                    game[i] = 20;
                    numbers.push_back(i);  
                }
               
            }
            
        output(game,ticSize);
        checkForWin(game,ticSize,player1,player2,gameWin);   
        }
        picks++;
    
        
        if(gameWin == 0 && picks < ticSize)
        {
        cout << endl << "Pick a space for O 1-9" << endl;
        aiPlayer(game,input);
        
         
        picks++;
        output(game,ticSize);
        checkForWin(game,ticSize,player1,player2,gameWin);  
        }
        if(picks == ticSize)
        {
            gameWin++;
            cout << endl << "Tie";
        }
    }
    cout << endl << "Would you like to play again? y/n";
    cin >> menu;
    }while(menu[0] == 'y');
    return 0;
}

void output(int a[], int size)
{
    int counter = 0;
    for(int i = 0; i < size;i++)
    {
        
        if(counter % 3 == 0)
        {
            cout << endl << "-------------" <<endl;  
        }
        if (i == 0 || i == 2 || i == 3 || i == 5 || i == 6 || i == 8 )
        { 
            if (a[i] == 20)
            {
                cout <<"| X |" ;
                counter++;
            }
            else if (a[i] == 100)
            {
                cout << "| O |";
                counter++;
            }
            else
            {
            cout << "| "<< a[i] << " |" ;
            counter++;}
        }
        else if (a[i] == 20)
            {
                cout <<" X ";
                counter++;
            }
            else if (a[i] == 100)
            {
                cout << " O ";
                counter++;
            }
            else
            
        {
            cout << " "<< a[i] << " " ;
            counter++;
        }

    
    }
    cout << endl << "-------------" <<endl;
}
void checkForWin(int game[], int size,int& player1, int& player2, int& gameWin)
{
    int row1 = game[0] + game[1] + game[2];
    int row2 = game[3] + game[4] + game[5];
    int row3 = game[6] + game[7] + game[8];
    int row4 = game[0] + game[4] + game[8];
    int row5 = game[6] + game[4] + game[2];
    int row6 = game[0] + game[3] + game[6];
    int row7 = game[1] + game[4] + game[7];
    int row8 = game[2] + game[5] + game[8];
    if (row1 == 60 || row2 == 60 || row3 == 60 || row4 == 60 || row5 == 60 || row6 == 60
            || row7 == 60 || row8 == 60)
    {
        cout << endl << "X is the winner";
        player1++;
        gameWin++;
    }
    else if(row1 == 300 || row2 == 300 || row3 == 300 || row4 == 300 || row5 == 300 || row6 == 300
            || row7 == 300 || row8 == 300)
    {
        cout << endl << "O is the winner";
                player2++;
                gameWin++;
    }
    else
    {
        
    }
}
void aiPlayer(int game[], int& input)
{
    int found = 0;
    int size = 9;
    int pick = rand() % size;
    int rowSize = 8;
    int row[rowSize];
     row[0] = game[0] + game[1] + game[2];
     row[1] = game[3] + game[4] + game[5];
     row[2] = game[6] + game[7] + game[8];
     row[3] = game[0] + game[4] + game[8];
     row[4] = game[6] + game[4] + game[2];
     row[5] = game[0] + game[3] + game[6];
     row[6] = game[1] + game[4] + game[7];
     row[7] = game[2] + game[5] + game[8];
     int arrayTotal = 0;
     for(int i = 0; i < size;i ++)
     {
         arrayTotal = arrayTotal + game[i];
     }
        cout << "Array Total " << arrayTotal; 
        
          if(arrayTotal <= 55 && game[0] == 20)
            {
            game[4] = 100;
            input = 4;   
            cout << endl << " spot 1";
             }
     
            else if(arrayTotal <= 55 && game[2] == 20)
            {
            game[4] = 100;
            input = 4;
             cout << endl << " spot 2";
            }
            else if(arrayTotal <= 55 && game[6] == 20)
            {
            game[4] = 100;
            input = 4;     
            }
            else if(arrayTotal <= 55 && game[8] == 20)
            {
            game[4] = 100;
            input = 4;  
            }  
        else if(200 <= row[0] && row[0] != 220)// check to see if row contains a game winning choice possible
        {                                 // ruling a line already blocked by X(20) 220
            if(game[0] < 100)
            {
                game[0] = 100;
               input = 0;
            }
            else if(game[1] < 100)
            {
                game[1] = 100;
                input = 1;
            }
            else
            {
                game[2] = 100;
                input = 2;
            }
        }
        else if(200 <= row[1] && row[1] != 220)
        {
            if(game[3] < 20)
            {
                game[3] = 100;
               input = 3;
            }
            else if(game[4] < 20)
            {
                game[4] = 100;
               input = 4;
            }
            else
            {
                game[5] = 100;
               input = 5;
            }
        }
        else if(200 <= row[2] && row[2] != 220)
        {
            if(game[6] < 20)
            {
               game[6] = 100;
                 input = 6;
            }
            else if(game[7] < 20)
            {
                game[7] = 100;
               input = 7;
            }
            else
            {
               game[8] = 100;
               input = 8;
            }
        }
        else if(200 <= row[3] && row[3] != 220)
        {
            if(game[0] < 20)
            {
                game[0] = 100;
               input = 0;
            }
            else if(game[4] < 20)
            {
                game[4] = 100;
               input = 4;
            }
            else
            {
                game[8] = 100;
                input = 8;
            }
        }
        else if(200 <= row[4] && row[4] != 220)
        {
           if(game[6] < 20)
            {
                game[6] = 100;
                input =  6;
            }
            else if(game[4] < 20)
            {
                game[4] = 100;
               input =  4;
            }
            else
            {
                game[2] = 100;
                input = 2;
            }
        }
        else if(200 <= row[5] && row[5] != 220)
        {
             if(game[0] < 20)
            {
                game[0] = 100;
                input = 0;
            }
            else if(game[3] < 20)
            {
                game[3] = 100;
               input = 3;
                
            }
            else
            {
                game[6] = 100;
               input = 6;
            }  
        }
        else if(200 <= row[6] && row[6] != 220)
        {
               if(game[1] < 20)
            {
                game[1] = 100;
                 input = 1;
            }
            else if(game[4] < 20)
            {
                game[4] = 100;
               input = 4;
            }
            else
            {
                game[7] = 100;
               input = 7;
            }
        }
        else if(200 <= row[7] && row[7] != 220)
        {
            if(game[2] < 20)
            {
                game[2] = 100;
               input = 2;
            }
            else if(game[5] < 20)
            {
                game[5] = 100;
               input = 5;
            }
            else
            {
                game[8] = 100;
                input = 8;
            }
           
        }
        
        else if(100 > row[0] && row[0] >= 40)
        {
            if(game[0] < 20)
            {
                game[0] = 100;
               input = 0;
            }
            else if(game[1] < 20)
            {
                game[1] = 100;
                input = 1;
            }
            else
            {
                game[2] = 100;
                input = 2;
            }
        }
        else if(100 > row[1] && row[1] >= 40)
        {
            if(game[3] < 20)
            {
                game[3] = 100;
               input = 3;
            }
            else if(game[4] < 20)
            {
                game[4] = 100;
               input = 4;
            }
            else
            {
                game[5] = 100;
               input = 5;
            }
        }
        else if(100 > row[2] && row[2] >= 40)
        {
            if(game[6] < 20)
            {
               game[6] = 100;
                 input = 6;
            }
            else if(game[7] < 20)
            {
                game[7] = 100;
               input = 7;
            }
            else
            {
               game[8] = 100;
               input = 8;
            }
        }
        else if(100 > row[3] && row[3] >= 40)
        {
            if(game[0] < 20)
            {
                game[0] = 100;
               input = 0;
            }
            else if(game[4] < 20)
            {
                game[4] = 100;
               input = 4;
            }
            else
            {
                game[8] = 100;
                input = 8;
            }
        }
        else if(100 > row[4] && row[4] >= 40)
        {
           if(game[6] < 20)
            {
                game[6] = 100;
                input =  6;
            }
            else if(game[4] < 20)
            {
                game[4] = 100;
               input =  4;
            }
            else
            {
                game[2] = 100;
                input = 2;
            }
        }
        else if(100 > row[5] && row[5] >= 40)
        {
             if(game[0] < 20)
            {
                game[0] = 100;
                input = 0;
            }
            else if(game[3] < 20)
            {
                game[3] = 100;
               input = 3;
                
            }
            else
            {
                game[6] = 100;
               input = 6;
            }  
        }
        else if(100 > row[6] && row[6] >= 40)
        {
               if(game[1] < 20)
            {
                game[1] = 100;
                 input = 1;
            }
            else if(game[4] < 20)
            {
                game[4] = 100;
               input = 4;
            }
            else
            {
                game[7] = 100;
               input = 7;
            }
        }
        else if(100 > row[7] && row[7] >= 40)
        {
            if(game[2] < 20)
            {
                game[2] = 100;
               input = 2;
            }
            else if(game[5] < 20)
            {
                game[5] = 100;
               input = 5;
            }
            else
            {
                game[8] = 100;
                input = 8;
            }
           
        }
        else if(game[4] == 5)
        {
            game[4] = 100;
            input = 4;
        }
        else if(game[0] == game[8] || game[2] == game[6] )
        {
            if (row[0] < 120)
            {
                game[1] = 100;
                input = 1;
            }
            else if (row[2] < 120)
            {
                game[7] = 100;
                input = 7;
            }
            else if (row[5] < 120)
            {
                game[3] = 100;
                input = 3;
            }
                    else
            {
                game[5] = 100;
                input = 5;
            }
        }
        else if(arrayTotal > 270)
        {
            cout << "Hello 1" << endl;
            while(found == 0)
            {
                
                pick = rand() % size;
                input =  pick;  
                
                for(int i = 0;i < size;i++)
                {
                    if(game[i] == input)
                    {
                        found++;
                        game[i] = 100;
                    }
                }
            }
        }
        else
        {
    cout << "Hello 2" << endl;
            while(found == 0)
            {
                
                pick = rand() % size;
                while(pick == 2 || pick == 4 || pick == 6 || pick == 8)
                {
                    pick = rand() % size;
                  
                   
                }
                input =  pick;  
                
                for(int i = 0;i < size;i++)
                {
                    if(game[i] == input)
                    {
                        found++;
                        game[i] = 100;
                    }
                }
            }
        }
       
}
bool arrayCheck(int a[],int size, int number)
{
    for(int i = 0; i < size; i++)
    {
        while(a[i] == number)
        {
            return false;
        }
    }
    return true;
}
bool vectorCheck(vector<int> a, int number)
{
    for(int i = 0; i < a.size(); i++)
    {
        while(a[i] == number)
        {
            return false;
        }
    }
    return true;
}

 