#include<iostream>
#include <cstdlib>
#include <iomanip>
#include<vector>
#include<fstream>
using namespace std;

struct Party
{
    int health;
    int attack;
    string name;
    int level;
    
};
struct Inventory
{
   int potion; 
   int key;
   int floorKey;
};
struct Mob
{
    int health;
    int attack;
    int level;
    string name;
    
};
void checkForWin(int a[], int size, int& player1,int& player2, int& gameWin);
void dumbPlayer(int game[], int& input);
void outputArray(int a[], int size);
bool arrayCheck(int a[],int size, int number);
bool vectorCheck(vector<int> a, int number);
void aiPlayer(int game[], int& input);
vector<int> resetVector(vector<int>& a);
void output(vector<int> v);
bool find(vector<int> v, int a);
int binarySearch(const int v[], int size, int val);
void output(Party a, Inventory b);
void output(Mob a);
void encounter(Party &hero, Mob m, Inventory &a,int& levelClear);
void roomChange(int a[],int size);
void output(int a[],int size);
void selectionSort(int v[],int size);
bool arrayCheck(int a[],int size, int number);

int main(int argc, char** argv) {
    
    Inventory bag;
    bag.potion = 1;
    bag.key = 0;
   
    Party hero1;
    hero1.health = 100;
    hero1.level = 1;
    hero1.attack = 3;
   
    Mob slime;
    slime.name = "Slime";
    slime.level = 1;
    slime.health = 10;
    slime.attack = 1;
    
    Mob skeleton;
    skeleton.name = "Skeleton";
    skeleton.level = 1;
    skeleton.health = 15;
    skeleton.attack = 3;
        
    Mob dragon;
    dragon.health = 50;
    dragon.level = 10;
    dragon.name = "Dragon";
    dragon.attack = 15;
    string readFlag = "TICTACTOE";
    
    int option;
    string menuInput;
    srand(time(0));
    int player1 = 0;
    int player1VsDumb = 0;
    int dumbVsPlayer = 0;
    int player2 = 0;
    vector<int> numbers;
    string menu;
    int dumbWin1 = 0;
    int dumbWin2 = 0;
    int smartWin = 0;
    int smartLoss = 0;
    int ticSize = 9;
    int input;
    int gameWin = 0;
    int vectorTemp; // temp to hold file read vector input
    int picks = 0;
    int game[ticSize];
    int potionLoot = rand() % 3; // potion drop rate
    int keyLoot = rand() % 5;// key drop rate
    int size = 9;
    int found = 0;
    
    int counter = 0;
    string temp;
    int keyRoom1 = 0;
    int keyRoom2 = 0;
    int roomSize = 9;
    int rooms[roomSize];
    string bossInput;
    int playerLocation = 20;
    int location;
    vector<int> explored;
    srand(time(0));
    string dungeonInput;
    string direction;
    int rowSize = 5;
    int dungeonSize = 10;
    int map[dungeonSize][dungeonSize];
    int dungeonCounter = 0;
    int potionRoom = 0;
    int keyRoom = 1;
    int levelClear = 0;
    
    do
    {
    
    
        cout << "Project 2 " << endl << endl;
        cout << "1. Tic tac toe" << endl;
        cout << "2. Dungeon Crawler text" << endl;
        cout << "3. Load" << endl;
        cout << "4. Save" << endl;
        cout << "Pick a game" << endl << endl;
        cin >> option;

        switch(option)
        {
            case 1:
            {
        cout << "1. Player vs. Player" << endl;
        cout << "2. Player vs. Smart AI" << endl;
        cout << "3. Player vs. Dumb AI" << endl;
        cout << "4. Dumb AI vs. Dumb AI" << endl << endl; 
        cout << "Select an option" << endl << endl;
        cin >> option;

              
            switch(option)
            {
        case 1:
        {
    do 
    {

        picks = 0;
        counter = 0;
        gameWin = 0;

            for(int i = 0; i < size;i++)
            {
            counter++;
            game[i] = counter;
             }
    
        cout << "Player 1: " << player1 << " Player 2: " << player2;
    
        output(game,size);
    
    while(gameWin == 0)
    {
        
        
        if(gameWin == 0)
        {
            cout << endl << "Pick a space for X 1-9" << endl;
            cin >> input;
            
            
            while(game[input - 1] != input)
            {
            cout << "Invalid input";
            cin >> input;
            }
         picks++;

            for(int i = 0; i < size;i ++)
            {
                if (input == game[i])
                {
                    game[i] = 20;
                }
            }
        output(game,size);
        checkForWin(game,size,player1,player2,gameWin);
            
        if(gameWin == 0)
        {
            if(picks == ticSize)
            {
                gameWin++;
                cout << endl << "Tie";
            }
        }
        
        
        }
        
    
        
        if(gameWin == 0)
        {
        cout << endl << "Pick a space for O 1-9" << endl;
        cin >> input;
            while(game[input - 1] != input)
            {
            cout << "Invalid input";
            cin >> input;
            }
        
        
        for(int i = 0; i < size;i ++)
        {
            if (input == game[i])
            {
                game[i] = 100;
            }
        }
        picks++;
        output(game,size);
        checkForWin(game,size,player1,player2,gameWin); 
        if(picks == ticSize)
            {
                gameWin++;
                cout << endl << "Tie";
            }
        }
     
    }
    cout << endl << "Would you like to play again? y/n";
    cin >> menu;
    while(cin.fail())
            {
            cin.clear();    
            cin.ignore();
            cout << "Invalid input";
            cin >> menu;
            menu = menu;
            }
     
    }while(menu[0] == 'y');
                    
                }
                break;
                case 2:
                    
                {

        do 
    {
        picks = 0;
        counter = 0;
        gameWin = 0;

        for(int i = 0; i < ticSize;i++)
        {
        counter++;
        game[i] = counter;
         }
    
        cout << "Player 1: " << player1 << " Player 2: " << player2;

        output(game,ticSize);

        while(gameWin == 0 && picks < ticSize)
        {


            if(gameWin == 0 && picks < ticSize)
            {
                cout << endl << "Pick a space for X 1-9" << endl;
                cin >> input;

                while(cin.fail() || game[input - 1] != input)
                {
                cin.clear();    
                cin.ignore();
                cout << "Invalid input";
                cin >> input;
                input = input;
                }
                for(int i = 0; i < size;i ++)
        {
            if (input == game[i])
            {
                game[i] = 20;
            }
        }


            output(game,ticSize);
            checkForWin(game,ticSize,player1,player2,gameWin);   
            }
            picks++;


            if(gameWin == 0 && picks < ticSize)
            {
            cout << endl << "Pick a space for O 1-9" << endl;
            aiPlayer(game,input);


            picks++;
            output(game,ticSize);
            checkForWin(game,ticSize,player1,player2,gameWin);  
            }
            if(picks == ticSize)
            {
                gameWin++;
                cout << endl << "Tie";
            }
        }
        cout << endl << "Would you like to play again? y/n";
        cin >> menu;
        while(cin.fail())
            {
            cin.clear();    
            cin.ignore();
            cout << "Invalid input";
            cin >> menu;
            menu = menu;
            }
        }while(tolower(menu[0] == 'y'));

                    }
                break;
                case 3:
                {
       do 
    {
        picks = 0;
        counter = 0;
        gameWin = 0;

    for(int i = 0; i < ticSize;i++)
    {
    counter++;
    game[i] = counter;
     }
    
    cout << "Player 1: " << player1VsDumb << " Player 2: " << dumbVsPlayer;
    
    output(game,ticSize);
    
    while(gameWin == 0 && picks < ticSize)
    {
        
        
        if(gameWin == 0 && picks < ticSize)
        {
            cout << endl << "Pick a space for X 1-9" << endl;
            cin >> input;
        
            while(cin.fail() || game[input - 1] != input)
            {
            cin.clear();    
            cin.ignore();
            cout << "Invalid input";
            cin >> input;
            input = input;
            }
       
            for(int i = 0; i < ticSize;i++)
            {
                if (input == game[i])
                {
                    game[i] = 20;
                    numbers.push_back(i);  
                }
               output(game,ticSize);
        checkForWin(game,ticSize,player1VsDumb,dumbVsPlayer,gameWin); 
            }
            
       
        }
        picks++;
    
        
        if(gameWin == 0 && picks < ticSize)
        {
        cout << endl << "Pick a space for O 1-9" << endl;
        dumbPlayer(game,input);
        output(game,ticSize);
         checkForWin(game,ticSize,player1VsDumb,dumbVsPlayer,gameWin);
        picks++;
        
          
        }
        if(picks == ticSize)
        {
            gameWin++;
            cout << endl << "Tie";
        }
        checkForWin(game,ticSize,player1VsDumb,dumbVsPlayer,gameWin);  
    }
    cout << endl << "Would you like to play again? y/n";
    cin >> menu;
    while(cin.fail())
            {
            cin.clear();    
            cin.ignore();
            cout << "Invalid input";
            cin >> menu;
            menu = menu;
            }
    }while(tolower(menu[0] != 'n'));
                }
                break;
                case 4:
                {
  do 
    {
        picks = 0;
        counter = 0;
        gameWin = 0;
        for(int i = 0; i < size;i++)
        {
        counter++;
        game[i] = counter;
         }
    
    cout << "Dumb AI 1: " << dumbWin1 << " Dumb AI 2: " << dumbWin2;
    
    output(game,size);
    
    while(gameWin == 0 && picks < size)
    {    
        if(gameWin == 0)
        {
        cout << endl << "Player 1 pick a space for X 1-9" << endl;
        input = rand() % 9;
        while(game[input - 1] != input)
        {
            input = rand() % 9 + 1;
        }

        for(int i = 0; i < size;i ++)
        {
            if (input == game[i])
            {
                game[i] = 20;
            }
        }
        output(game,size);
        checkForWin(game,size,dumbWin1,dumbWin2,gameWin); 
        picks++;
            if(gameWin == 0)
            {
                 if(picks == ticSize)
            {
            gameWin++;
            cout << endl << "Tie";
            }
            }
        }
            if(gameWin == 0)
            {
            cout << endl << "Player 2 pick a space for O 1-9" << endl;
            input = rand() % 9 + 1;
            while(game[input - 1] != input)
            {
                input = rand() % 9 + 1;
            }
        
        
        for(int i = 0; i < size;i ++)
        {
            if (input == game[i])
            {
                game[i] = 100;
            }
        }
        output(game,size);
        checkForWin(game,size,dumbWin1,dumbWin2,gameWin);  
        }
        picks++;
             if(picks == ticSize)
            {
            gameWin++;
            cout << endl << "Tie";
            }
        
    }
    cout << endl << "Would you like to play again? y/n";
    cin >> menu;
      while(cin.fail())
            {
            cin.clear();    
            cin.ignore();
            cout << "Invalid input";
            cin >> menu;
            menu = menu;
            }
    }while(tolower(menu[0] == 'y'));
                }
                
        }   
            }break;
   //      --------------------------------------------------------
            case 2:
            {

                  do
    {
    levelClear = 0;
    roomChange(rooms,roomSize);


    
    
    if(hero1.level == 1 || hero1.health < 0)
    {
    cout << "Enter a name for Hero 1: " << endl;
    cin >> dungeonInput;  
    hero1.name = dungeonInput;
    hero1.health = 100;
    hero1.level = 1;
    hero1.attack = 3;

    bag.potion = 1;
    bag.key = 0;

    slime.name = "Slime";
    slime.level = 1;
    slime.health = 10;
    slime.attack = 1;

    skeleton.name = "Skeleton";
    skeleton.level = 1;
    skeleton.health = 15;
    skeleton.attack = 3;

    dragon.health = 50;
    dragon.level = 10;
    dragon.name = "Dragon";
    dragon.attack = 15;
    roomChange(rooms,roomSize);
    playerLocation = 20;
    resetVector(explored);
    }
    
    dungeonCounter = 0;
    
    for(int i = 0; i < rowSize; i++)\
    {
        for(int k = 0; k < dungeonSize; k++)
        {
            map[i][k] = dungeonCounter;
            dungeonCounter ++;
        }
    }
    
    cout << "W) Up A) left D) Right S) Down... type p to exit" << endl;
        do
        {
   
        
        output(hero1, bag);
        cout << endl;
        for(int i = 0; i < rowSize; i++)
            {
                for(int k = 0; k < dungeonSize; k++)
                {
                    if (map[i][k] == playerLocation)
                    {
                        cout << setw(5) << "H";
                    }
                    else if(find(explored,map[i][k]))
                    {
                        cout << setw(5) << "O";
                    }
                    else
                    cout << setw(5) << "X";
                }
                cout << endl;
            }
        int move = 0;
        
        while(move == 0)
        {
            potionLoot = rand() % 3;
            keyLoot = rand() % 5;
        cin >> direction;
            if (tolower(direction[0]) == 'w' && playerLocation - 10 >= 0)//..[;.[.[.[.
            {
                explored.push_back(playerLocation);
                playerLocation = playerLocation - 10; 
                move++;

            }
            else if(tolower(direction[0]) == 'a' && playerLocation - 1 >= 0 && playerLocation != 10 && playerLocation != 20 && playerLocation != 30 && playerLocation != 40)
            {
                explored.push_back(playerLocation);
                playerLocation = playerLocation - 1; 
                move++;
            }
            else if(tolower(direction[0]) == 'd' && playerLocation + 1 <= 49 && playerLocation != 19 && playerLocation != 29 && playerLocation != 39 && playerLocation != 9)
            {
                explored.push_back(playerLocation);
                playerLocation = playerLocation + 1; 
                move++;
                
            }
            else if(tolower(direction[0]) == 's' && playerLocation + 10 <= 49)
            {
                explored.push_back(playerLocation);
                playerLocation = playerLocation + 10;
                move++;
                
            }
            else if(tolower(direction[0]) == 'p')
            {
                cout <<"Goodbye" << endl;
                move++;       
            }
            else
            {
                cout << "Invalid Input" << endl;
            }
        }
        if(playerLocation == rooms[0])
        {
            if (keyRoom1 == 0)
            {
            cout << "You found a key!" << endl;
            bag.key++;
            keyRoom1++;
            }
            else
            {
                cout << "The key room is empty" << endl;
            }
        }
        else if(playerLocation == rooms[1])
        {
            levelClear = 0;
            cout << "The sliding sounds of wet movement become louder.."  << endl;
            encounter(hero1,slime,bag, levelClear);
            if(levelClear > 0)
            {
            hero1.attack++;
            hero1.level++;
            slime.attack++;
            slime.level++;
            slime.health = slime.health + 10;
            if(potionLoot == 2)
            {
                cout << slime.name << " dropped a potion" << endl;
                bag.potion++;
            }
            if(keyLoot == 2)
            {
                cout << slime.name << " dropped a key" << endl;
                bag.key++;
            }
            }
        }
        
        else if(playerLocation == rooms[2])
        {
            levelClear = 0;
            cout << "The sliding sounds of wet movement become louder.." << endl;
            encounter(hero1,slime,bag, levelClear);
            if(levelClear > 0)
            {
            slime.attack++;
            slime.level++;
            slime.health = slime.health + 10;
            hero1.attack++;
            hero1.level++;
            }
            if(potionLoot == 2)
            {
                cout << slime.name << " dropped a potion" << endl;
                bag.potion++;
            }
            if(keyLoot == 2)
            {
                cout << slime.name << " dropped a key" << endl;
                bag.key++;
            }
        }
        else if(playerLocation == rooms[3])
        {
            if(potionRoom == 0)
            {
            cout << "You find a fountain with restorative properties" << endl <<
                    " +2 potions" << endl;
            bag.potion = bag.potion + 2;
            potionRoom++;
            }
            else
            {
                cout << "The fountain has been emptied" << endl;
            }
        }
        else if (playerLocation == rooms[4])
        {
            levelClear = 0;
            cout << "An enormous obsidian door with a lock on each side blocks the path " << endl;
            if(bag.key >= 2)
            {
                cout << "Insert both keys? y/n" << endl;
                cin >> bossInput;
                while(cin.fail())
                {
                cin.clear();    
                cin.ignore();
                cout << "Invalid input";
                cin >> bossInput;
                bossInput = bossInput;
                }
                if(tolower(bossInput[0]) == 'y')
                {    
                 
                encounter(hero1,dragon,bag, levelClear);
                        if ( levelClear > 0)
                    {
                        cout << "The dragon breaks through the ceiling to escape , you follow after." << endl;
                    dragon.attack = dragon.attack + 15;
                    dragon.level = dragon.level + 10; 
                    dragon.health = dragon.health + 30;
                    resetVector(explored);
                    roomChange(rooms,roomSize);
                    keyRoom1--;
                    keyRoom2--;
                    bag.key = bag.key - 2; 
                    bag.potion++;
                    potionRoom = 0;
                    }
                         else
                    {
                        cout << "You could not defeat the dragon" << endl;
                    }
                }
             }       
        }
        else if(playerLocation == rooms[5])
        {
            levelClear = 0;
            cout << "The sounds of shuffling and clanking grow closer" << endl;
            encounter(hero1,skeleton,bag, levelClear);
            if(levelClear > 0)
            {
            skeleton.attack++;
            skeleton.level++;
            skeleton.health = skeleton.health + 10;
            hero1.attack++;
            hero1.level++;
            if(potionLoot == 2)
            {
                cout << skeleton.name << " dropped a potion" << endl;
                bag.potion++;
            }
            if(keyLoot == 2)
            {
                cout << skeleton.name << " dropped a key" << endl;
                bag.key++;
            }
            }
        }
        else if(playerLocation == rooms[6])
        {
            levelClear = 0;
            cout << "The sounds of shuffling and clanking grow closer" << endl;
            encounter(hero1,skeleton,bag, levelClear);
            if(levelClear > 0)
            {
            skeleton.attack++;
            skeleton.level++;
            skeleton.health = skeleton.health + 10;
            hero1.attack++;
            hero1.level++;
            if(potionLoot == 2)
            {
                cout << skeleton.name << " dropped a potion" << endl;
                bag.potion++;
            }
            if(keyLoot == 2)
            {
                cout << skeleton.name << " dropped a key" << endl;
                bag.key++;
            }
            }
        }
        else if(playerLocation == rooms[7])
        {
            if (keyRoom2 == 0)
            {
            cout << "You found a key!" << endl;
            bag.key++;
            keyRoom2++;
            }
            else
            {
                cout << "The key room is empty" << endl;
            }
        }
    }while (hero1.health > 0 && tolower(direction[0]) != 'p');
    
        if(hero1.health <= 0){
        cout << "You have died." << endl;}
    cout << "Create a new character? y/n" << endl;
    cin >> menuInput;
    while(cin.fail())
            {
            cin.clear();    
            cin.ignore();
            cout << "Invalid input";
            cin >> menuInput;
            menuInput = menuInput;
            }
    }while(tolower(menuInput[0] == 'y'));  
    
            }break;
            case 3: 
            {
                cout << "Loading File." << endl;
                ifstream fin;
                fin.open("save.dat");
                fin >> temp;
                
                if (temp == readFlag)
                {
                     fin >> player2;
    fin >> player1;
                    
    fin >> dumbWin1;
    fin >> dumbWin2;
    fin >> smartWin;
    fin >> smartLoss;      
                    
    fin >> hero1.name;
    fin >> hero1.health;
    fin >> hero1.level;
    fin >> hero1.attack;

    fin >> bag.potion;
    fin >> bag.key;

    fin >> slime.name;
    fin >> slime.level;
    fin >> slime.health;
    fin >> slime.attack;

    fin >> skeleton.name;
    fin >> skeleton.level;
    fin >> skeleton.health;
    fin >> skeleton.attack;

    fin >> dragon.health;
    fin >> dragon.level;
    fin >> dragon.name;
    fin >> dragon.attack;

    fin >> playerLocation;
     
    fin >> keyRoom1;
    fin >> keyRoom2;
    fin >> playerLocation;
    fin >> potionRoom;
    fin >> keyRoom;
    while(!fin.eof())
    {
         
        fin >> vectorTemp;
        explored.push_back(vectorTemp);
    }
    cout << "File loaded" << endl;
                }
                else
                {
                    cout << "Read error" << endl;
                }
                
                fin.close();
                
            } break;
            case 4: 
            {
                cout << "Saving File" << endl;
                ofstream fout;
                fout.open("save.dat");
                    fout << "TICTACTOE" << endl;
    fout << player2 << endl;
    fout << player1 << endl;
                    
    fout << dumbWin1  << endl;
    fout << dumbWin2<< endl;
    fout << smartWin << endl;
    fout << smartLoss << endl;      
                    
    fout << hero1.name << endl;
    fout << hero1.health << endl;
    fout << hero1.level << endl;
    fout << hero1.attack << endl;

    fout << bag.potion << endl;
    fout << bag.key << endl;

    fout << slime.name << endl;
    fout << slime.level << endl;
    fout << slime.health << endl;
    fout << slime.attack << endl;

    fout << skeleton.name << endl;
    fout << skeleton.level << endl;
    fout << skeleton.health << endl;
    fout << skeleton.attack << endl;

    fout << dragon.health << endl;
    fout << dragon.level << endl;
    fout << dragon.name << endl;
    fout << dragon.attack << endl;

    fout << playerLocation << endl;
     
    fout << keyRoom1 << endl;
    fout << keyRoom2 << endl;
    fout << playerLocation << endl;
    fout << potionRoom << endl;
    fout << keyRoom << endl;
    for(int i = 0; i < explored.size();i++)
    {
        fout << explored[i] << endl;
    }
                fout.close();
                cout << "File saved" << endl;
            } break;
        }
        cout << "Would you like to return to the menu? y/n" << endl;
        cin >> menuInput;
         while(cin.fail())
            {
            cin.clear();    
            cin.ignore();
            cout << "Invalid input";
            cin >> menuInput;
            menuInput = menuInput;
            }
    }while(tolower(menuInput[0]) != 'n');

    return 0;
}
//-------------------------------------------------------------------------
void output(int a[], int size)
{
    int counter = 0;
    for(int i = 0; i < size;i++)
    {
        
        if(counter % 3 == 0)
        {
            cout << endl << "-------------" <<endl;  
        }
        if (i == 0 || i == 2 || i == 3 || i == 5 || i == 6 || i == 8 )
        { 
            if (a[i] == 20)
            {
                cout <<"| X |" ;
                counter++;
            }
            else if (a[i] == 100)
            {
                cout << "| O |";
                counter++;
            }
            else
            {
            cout << "| "<< a[i] << " |" ;
            counter++;}
        }
        else if (a[i] == 20)
            {
                cout <<" X ";
                counter++;
            }
            else if (a[i] == 100)
            {
                cout << " O ";
                counter++;
            }
            else      
        {
            cout << " "<< a[i] << " " ;
            counter++;
        }

    }
    cout << endl << "-------------" <<endl;
}
void checkForWin(int game[], int size,int& player1, int& player2, int& gameWin)
{
    int row1 = game[0] + game[1] + game[2];
    int row2 = game[3] + game[4] + game[5];
    int row3 = game[6] + game[7] + game[8];
    int row4 = game[0] + game[4] + game[8];
    int row5 = game[6] + game[4] + game[2];
    int row6 = game[0] + game[3] + game[6];
    int row7 = game[1] + game[4] + game[7];
    int row8 = game[2] + game[5] + game[8];
    if (row1 == 60 || row2 == 60 || row3 == 60 || row4 == 60 || row5 == 60 || row6 == 60
            || row7 == 60 || row8 == 60)
    {
        cout << endl << "X is the winner";
        player1++;
        gameWin++;
    }
    else if(row1 == 300 || row2 == 300 || row3 == 300 || row4 == 300 || row5 == 300 || row6 == 300
            || row7 == 300 || row8 == 300)
    {
        cout << endl << "O is the winner";
                player2++;
                gameWin++;
    }
    else
    {
        
    }
}
void aiPlayer(int game[], int& input)
{
    int found = 0;
    int size = 9;
    int pick = rand() % size;
    int rowSize = 8;
    int row[rowSize];
     row[0] = game[0] + game[1] + game[2];
     row[1] = game[3] + game[4] + game[5];
     row[2] = game[6] + game[7] + game[8];
     row[3] = game[0] + game[4] + game[8];
     row[4] = game[6] + game[4] + game[2];
     row[5] = game[0] + game[3] + game[6];
     row[6] = game[1] + game[4] + game[7];
     row[7] = game[2] + game[5] + game[8];
     int arrayTotal = 0;
     for(int i = 0; i < size;i ++)
     {
         arrayTotal = arrayTotal + game[i];
     }
        cout << "Array Total " << arrayTotal; 
        
          if(arrayTotal <= 55 && game[0] == 20)
            {
            game[4] = 100;
            input = 4;   
            cout << endl << " spot 1";
             }
     
            else if(arrayTotal <= 55 && game[2] == 20)
            {
            game[4] = 100;
            input = 4;
             cout << endl << " spot 2";
            }
            else if(arrayTotal <= 55 && game[6] == 20)
            {
            game[4] = 100;
            input = 4;     
            }
            else if(arrayTotal <= 55 && game[8] == 20)
            {
            game[4] = 100;
            input = 4;  
            }  
        else if(200 <= row[0] && row[0] != 220)// check to see if row contains a game winning choice possible
        {                                 // ruling a line already blocked by X(20) 220
            if(game[0] < 100)
            {
                game[0] = 100;
               input = 0;
            }
            else if(game[1] < 100)
            {
                game[1] = 100;
                input = 1;
            }
            else
            {
                game[2] = 100;
                input = 2;
            }
        }
        else if(200 <= row[1] && row[1] != 220)
        {
            if(game[3] < 20)
            {
                game[3] = 100;
               input = 3;
            }
            else if(game[4] < 20)
            {
                game[4] = 100;
               input = 4;
            }
            else
            {
                game[5] = 100;
               input = 5;
            }
        }
        else if(200 <= row[2] && row[2] != 220)
        {
            if(game[6] < 20)
            {
               game[6] = 100;
                 input = 6;
            }
            else if(game[7] < 20)
            {
                game[7] = 100;
               input = 7;
            }
            else
            {
               game[8] = 100;
               input = 8;
            }
        }
        else if(200 <= row[3] && row[3] != 220)
        {
            if(game[0] < 20)
            {
                game[0] = 100;
               input = 0;
            }
            else if(game[4] < 20)
            {
                game[4] = 100;
               input = 4;
            }
            else
            {
                game[8] = 100;
                input = 8;
            }
        }
        else if(200 <= row[4] && row[4] != 220)
        {
           if(game[6] < 20)
            {
                game[6] = 100;
                input =  6;
            }
            else if(game[4] < 20)
            {
                game[4] = 100;
               input =  4;
            }
            else
            {
                game[2] = 100;
                input = 2;
            }
        }
        else if(200 <= row[5] && row[5] != 220)
        {
             if(game[0] < 20)
            {
                game[0] = 100;
                input = 0;
            }
            else if(game[3] < 20)
            {
                game[3] = 100;
               input = 3;
                
            }
            else
            {
                game[6] = 100;
               input = 6;
            }  
        }
        else if(200 <= row[6] && row[6] != 220)
        {
               if(game[1] < 20)
            {
                game[1] = 100;
                 input = 1;
            }
            else if(game[4] < 20)
            {
                game[4] = 100;
               input = 4;
            }
            else
            {
                game[7] = 100;
               input = 7;
            }
        }
        else if(200 <= row[7] && row[7] != 220)
        {
            if(game[2] < 20)
            {
                game[2] = 100;
               input = 2;
            }
            else if(game[5] < 20)
            {
                game[5] = 100;
               input = 5;
            }
            else
            {
                game[8] = 100;
                input = 8;
            }
           
        }
        
        else if(100 > row[0] && row[0] >= 40)
        {
            if(game[0] < 20)
            {
                game[0] = 100;
               input = 0;
            }
            else if(game[1] < 20)
            {
                game[1] = 100;
                input = 1;
            }
            else
            {
                game[2] = 100;
                input = 2;
            }
        }
        else if(100 > row[1] && row[1] >= 40)
        {
            if(game[3] < 20)
            {
                game[3] = 100;
               input = 3;
            }
            else if(game[4] < 20)
            {
                game[4] = 100;
               input = 4;
            }
            else
            {
                game[5] = 100;
               input = 5;
            }
        }
        else if(100 > row[2] && row[2] >= 40)
        {
            if(game[6] < 20)
            {
               game[6] = 100;
                 input = 6;
            }
            else if(game[7] < 20)
            {
                game[7] = 100;
               input = 7;
            }
            else
            {
               game[8] = 100;
               input = 8;
            }
        }
        else if(100 > row[3] && row[3] >= 40)
        {
            if(game[0] < 20)
            {
                game[0] = 100;
               input = 0;
            }
            else if(game[4] < 20)
            {
                game[4] = 100;
               input = 4;
            }
            else
            {
                game[8] = 100;
                input = 8;
            }
        }
        else if(100 > row[4] && row[4] >= 40)
        {
           if(game[6] < 20)
            {
                game[6] = 100;
                input =  6;
            }
            else if(game[4] < 20)
            {
                game[4] = 100;
               input =  4;
            }
            else
            {
                game[2] = 100;
                input = 2;
            }
        }
        else if(100 > row[5] && row[5] >= 40)
        {
             if(game[0] < 20)
            {
                game[0] = 100;
                input = 0;
            }
            else if(game[3] < 20)
            {
                game[3] = 100;
               input = 3;
                
            }
            else
            {
                game[6] = 100;
               input = 6;
            }  
        }
        else if(100 > row[6] && row[6] >= 40)
        {
               if(game[1] < 20)
            {
                game[1] = 100;
                 input = 1;
            }
            else if(game[4] < 20)
            {
                game[4] = 100;
               input = 4;
            }
            else
            {
                game[7] = 100;
               input = 7;
            }
        }
        else if(100 > row[7] && row[7] >= 40)
        {
            if(game[2] < 20)
            {
                game[2] = 100;
               input = 2;
            }
            else if(game[5] < 20)
            {
                game[5] = 100;
               input = 5;
            }
            else
            {
                game[8] = 100;
                input = 8;
            }
           
        }
        else if(game[4] == 5)
        {
            game[4] = 100;
            input = 4;
        }
        else if(game[0] == game[8] || game[2] == game[6] )
        {
            if (row[0] < 120)
            {
                game[1] = 100;
                input = 1;
            }
            else if (row[2] < 120)
            {
                game[7] = 100;
                input = 7;
            }
            else if (row[5] < 120)
            {
                game[3] = 100;
                input = 3;
            }
                    else
            {
                game[5] = 100;
                input = 5;
            }
        }
        else if(arrayTotal > 270)
        {
           
            while(found == 0)
            {
                
                pick = rand() % size;
                input =  pick;  
                
                for(int i = 0;i < size;i++)
                {
                    if(game[i] == input)
                    {
                        found++;
                        game[i] = 100;
                    }
                }
            }
        }
        else
        {
  
            while(found == 0)
            {
                
                pick = rand() % size;
                while(pick == 2 || pick == 4 || pick == 6 || pick == 8)
                {
                    pick = rand() % size;
                  
                   
                }
                input =  pick;  
                
                for(int i = 0;i < size;i++)
                {
                    if(game[i] == input)
                    {
                        found++;
                        game[i] = 100;
                    }
                }
            }
        }
       
}
bool arrayCheck(int a[],int size, int number)
{
    for(int i = 0; i < size; i++)
    {
        while(a[i] == number)
        {
            return false;
        }
    }
    return true;
}
bool vectorCheck(vector<int> a, int number)
{
    for(int i = 0; i < a.size(); i++)
    {
        while(a[i] == number)
        {
            return false;
        }
    }
    return true;
}
void output(Mob a)
{
    cout << "Lvl." << a.level << " " << a.name << " " << "Health: " << a.health
    << endl;
}
void output(Party a, Inventory b)
{
    cout << "Lvl." << a.level << " " << a.name << " " << "Health: " << a.health
    << " Potion(s):" << b.potion << " Key(s): " << b.key <<endl;
}
bool find(vector<int> v, int a)
{
    for (int i = 0; i < v.size(); i++)
    {
        if (v[i] == a )
        {
            return 1;
        }  
    }
    return 0;
}
void output(vector<int> v)
{
    for(int i = 0; i < v.size();i++)
    {
        cout << v[i] << " ";
    }
}
void encounter(Party& hero, Mob m, Inventory& a, int& levelClear)
{
    
    int action;
    int run = 0;
    Mob temp;
    temp.name = m.name;
    temp.attack = m.attack;
    temp.level = m.level;
    temp.health = m.health;
    while(hero.health > 0 && temp.health > 0 && run == 0)
    {
        output(temp);
        cout << endl << "1. Attack" << endl << "2. Run" << endl << "3. potion" << endl;
        output(hero, a);
        
        while (!(cin >> action))
        {
            cout << "Invalid Input ";
            cin.clear();
            cin.ignore(123, '\n');
        }
        if(action == 1)
        {
            temp.health = temp.health - hero.attack;
            if(temp.health <= 0)
            {
            levelClear++;
            }
            else
            {
            cout << temp.name << "Strikes back!" << endl;
            hero.health = hero.health - temp.attack;
            }
        }
        else if(action == 2) 
        {
            int flee = rand() % 10;
            if (flee % 2 == 0)
            {
               cout << "You Escaped!" << endl;
               run++;
            }
            else
            {
                cout << m.name << "has stopped you from fleeing" << endl;
                cout << m.name << "Strikes!" << endl;
                hero.health = hero.health - temp.attack;
            }
        }
        else if( action == 3)
        {
            if (a.potion > 0)
            {
                hero.health = hero.health + 30;
                a.potion--;
            }
        }
        else
        {
            cout << "Invalid input" << endl;
        }
        
    }
  
}
vector<int> resetVector(vector<int>& a)
{
    vector<int> v2;
    a = v2;
    return a;
}
void outputArray(int a[],int size)
{
    for(int i = 0; i < size;i++)
    {
        cout << a[i] << " ";
    }
}
void roomChange(int a[], int size)
{
    int low = 1;
    int high = 48;
    int location;
    int random = rand() % (high - low) + low;
    for (int i = 0; i < size; i++)
    {
        while(arrayCheck(a,size,random) != true)
        {
            random = rand() % (high - low) + low;        
        }
        a[i] = random;
   }
    
}
void dumbPlayer(int game[], int& input)
{
    int found = 0;
    int size = 9;
    int pick = rand() % size;

 

    while(found == 0)
    {

        pick = rand() % size;
        
        input =  pick;  

        for(int i = 0;i < size;i++)
        {
            if(game[i] == input)
            {
                found++;
                game[i] = 100;
            }
        }
    }
      
}